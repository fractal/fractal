/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.factory

import org.objectweb.fractal.api.Type
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.`type`.BasicComponentType
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import scala.collection.mutable.HashMap
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.Fractal
import org.objectweb.fractal.api.factory.GenericFactory
import org.objectweb.fractal.julia.control.attribute.UseCloneableAttributeControllerTrait
import org.objectweb.fractal.api.control.AttributeController
import org.objectweb.fractal.julia.control.name.UseNameControllerTrait
import org.objectweb.fractal.api.control.NameController

/**
 * Trait providing a name copy function to a {@link Template} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait NameTemplateTrait extends BasicControllerTrait
with UseNameControllerTrait with TemplateTrait {

    override def newFcControllerInstance : Component = {
        val comp = super.newFcControllerInstance
        if( weaveableNC != null ) {
	        // copies the template's attributes into the component, if applicable
	        try {
	            val name = weaveableNC.getFcName
        		comp.getFcInterface("name-controller").
        		asInstanceOf[NameController].setFcName(name)
	      }
	      catch {
	        case ignored: NoSuchInterfaceException => // Indeed nothing
	      }
        }
        return comp
    }
}
