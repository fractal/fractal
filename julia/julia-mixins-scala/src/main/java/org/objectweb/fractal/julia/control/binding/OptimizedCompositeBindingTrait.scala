/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.control.content.UseContentControllerTrait
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Trait providing optimized {@link ComponentInterface} management to a {@link
 * BindingController}. This mixin uses shortcut links between component
 * interfaces where possible, i.e. when some interfaces do not have associated
 * interceptors.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait OptimizedCompositeBindingTrait extends AbstractBindingControllerTrait
with UseComponentTrait with UseContentControllerTrait {

    /**
     * Sets the shortcuts for the given binding and then calls the overriden
     * method.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @param serverItf a server interface.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be created.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface,
     *      but it is not in an appropriate state to perform this operation.
     */
	override def bindFc(
	    clientItfType: InterfaceType, clientItfName: String, serverItf: Object ) = {
		
	    super.bindFc(clientItfType,clientItfName,serverItf)
	    setFcShortcuts(clientItfType,clientItfName,serverItf)
    }

    /**
     * Updates the shortcuts for the given binding and then calls the overriden
     * method.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface,
     *      but it is not in an appropriate state to perform this operation.
     */
	override def unbindFc( clientItfType: InterfaceType, clientItfName: String ) = {
		super.unbindFc(clientItfType,clientItfName)
		setFcShortcuts(clientItfType,clientItfName,null)
    }

	
    // -------------------------------------------------------------------------
    // Utility methods: algorithm to compute of shortcut links
    // -------------------------------------------------------------------------

    /**
     * Creates the given binding and updates the shortcuts links accordingly.
     *
     * @param clientItfType the type of the 'clientItfName' interface. 
     * @param clientItfName a client interface name.
     * @param serverItf the server interface to which the client interface must
     *      be bound, or <tt>null</tt> if it must be unbound.
     * @throws NoSuchInterfaceException if there is no client interface whose
     *      name is equal to 'clientItfName'.
     * @throws IllegalBindingException if a problem occurs.
     */
	def setFcShortcuts(
	    clientItfType: InterfaceType, clientItfName: String, serverItf: Object ) = {
	  
	    var ocbh = new OptimizedCompositeBindingHelper(weaveableC,weaveableCC)
	    ocbh.setFcShortcuts(clientItfType,clientItfName,serverItf)
	}
}
