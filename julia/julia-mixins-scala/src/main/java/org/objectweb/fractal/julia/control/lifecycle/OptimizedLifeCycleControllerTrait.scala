/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

import scala.collection.mutable.HashSet
import org.objectweb.fractal.api.control.LifeCycleController
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.control.binding.Util
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.NoSuchInterfaceException
import scala.collection.mutable.LinkedList

/**
 * Trait providing an optimized implementation of the {@link
 * org.objectweb.fractal.api.control.LifeCycleController} interface. This
 * implementation does not need interceptors. This life cycle controller
 * works by stopping all the direct and indirect primitive sub components that
 * have a {@link LifeCycleCoordinator} interface, and the primitive client
 * components of these components (these clients <i>must</i> also have a {@link
 * LifeCycleCoordinator} interface). These components are stopped
 * simultaneously, by using the coordinator interface provided by this
 * component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait OptimizedLifeCycleControllerTrait extends LifeCycleCoordinatorTrait
with UseComponentTrait {

    // Indicates if this component is started or not
	private var fcStarted: Boolean = false

    // -------------------------------------------------------------------------
    // Implementation of the LifeCycleController interface
    // -------------------------------------------------------------------------

	override def getFcState: String = {
	    return if(fcStarted)
	    	LifeCycleController.STARTED else
	    	LifeCycleController.STOPPED
	}
	
	override def startFc = { 
	    val id = weaveableC.getFcInterface("component").asInstanceOf[Component]
	    val clccs = getFcLifeCycleControllers(id)
	    clccs.foreach( clcc => {
	        clcc.setFcStarted
	    })
	    super.setFcState(true)
	}

	override def stopFc = { 
	    val id = weaveableC.getFcInterface("component").asInstanceOf[Component]
	    val clccs = getFcLifeCycleControllers(id)
	    super.stopFc(clccs)
	    super.setFcState(false)
	}

    // -------------------------------------------------------------------------
    // Implementation of the CoordinatorLifeCycleController interface
    // -------------------------------------------------------------------------

	override def setFcStarted: Boolean = {
	    if( ! fcStarted ) {
	        fcStarted = true
	        return true
	    }
	    return false
	}
	
	override def setFcStopping( coordinator: LifeCycleCoordinator ) = {
	    // this method should never be called, 
	    // as this component is stopped by stopping its sub components
	    throw new Error("Internal error")
	}

	override def setFcStopped: Boolean = {
	    if( fcStarted ) {
	        fcStarted = false
	        return true
	    }
	    return false
	}
	
    // -------------------------------------------------------------------------
    // Utility methods
    // -------------------------------------------------------------------------

    /**
     * Returns the components that must be stopped in order to stop the given
     * component. These components are the direct or indirect primitive sub
     * components of this component that provide a {@link LifeCycleCoordinator}
     * interface, as well as the primitive client components of these components.
     *
     * @param id a composite component.
     * @return the components that must be stopped in order to stop the given
     *      component.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
	private def getFcLifeCycleControllers( id: Component ):
		Array[LifeCycleCoordinator] = {
	    
	    val clccList = getFcInternalLifeCycleControllers
	    val sItfs = id.getFcInterfaces
	    val visited = new HashSet[Interface]
	    sItfs.foreach( o => {
	        val sItf = o.asInstanceOf[Interface]
	        if( ! sItf.getFcItfType.asInstanceOf[InterfaceType].isFcClientItf ) {
	        	getSExtLifeCycleControllers( sItf, clccList, visited )
	        }
	    })
	    return clccList.toArray
    }

    /**
     * Finds the primitive client components that are bound to the given server
     * interface.
     *
     * @param serverItf a server interface.
     * @param clccList where to put the {@link LifeCycleCoordinator}
     *      interfaces of the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
	private def getSExtLifeCycleControllers(
	    serverItf: Interface, clccList: List[LifeCycleCoordinator],
	    visited: HashSet[Interface] ) : Boolean = {
	  
	    val comps = Util.getFcPotentialClientsOf(serverItf).toArray
	    comps.foreach( o => {
	        val comp = o.asInstanceOf[Component]
	        val l = Util.getFcClientItfsBoundTo(comp,serverItf)
	        val clientItfs = l.toArray
	        clientItfs.foreach( p => {
	            val clientItf = p.asInstanceOf[Interface]
	            getCExtLifeCycleControllers( clientItf, clccList, visited )
	        })
	    })
	    
	    return true  // recursive methods need return type in Scala
    }

    /**
     * Finds the primitive client components that are bound to the given client
     * interface.
     *
     * @param clientItf a client interface.
     * @param clccList where to put the {@link LifeCycleCoordinator} interfaces
     *      of the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
	private def getCExtLifeCycleControllers(
	    clientItf: Interface, clccList: List[LifeCycleCoordinator],
	    visited: HashSet[Interface] ) = {
	  
	    val component = clientItf.getFcItfOwner
	    var cc: ContentController = null
	    try {
	        cc = component.getFcInterface("content-controller").
	        		asInstanceOf[ContentController]
	    }
	    catch {
	      case e: NoSuchInterfaceException => // Indeed nothing
	    }
	    
	    if( cc != null ) {
	    	val name = clientItf.getFcItfName
	    	val itf =
	    	  ( if( ! clientItf.isFcInternalItf ) cc.getFcInternalInterface(name)
	    	  else component.getFcInterface(name) ).asInstanceOf[Interface]
	    	  if( ! visited.contains(itf) ) {
	    	      visited += itf
	    	      getSExtLifeCycleControllers( itf, clccList, visited )
	    	  }
	    }
	    else if( ! visited.contains(clientItf) ) {
	        visited += clientItf
	        val c = clientItf.getFcItfOwner
	        var lcc: LifeCycleCoordinator = null
	        try {
	            lcc = c.getFcInterface("lifecycle-controller").
	            		asInstanceOf[LifeCycleCoordinator]
	        }
	        catch {
	          case e:Exception =>
	            try {
	            	lcc = c.getFcInterface("/lifecycle-controller").
	            		asInstanceOf[LifeCycleCoordinator]
	            }
		        catch {
		          case nsie:NoSuchInterfaceException =>
			        throw new ChainedIllegalLifeCycleException( nsie, component,
			            "Primitive client without a LifeCycleCoordinator" )
		        }
	        }
	        if( ! clccList.contains(lcc) ) {
	        	clccList :+ lcc
	        }
        }
    }

	private def getFcInternalLifeCycleControllers: List[LifeCycleCoordinator] = {
    	// finds all the direct and indirect sub components of this component
	    val thisComponent =
	    	weaveableC.getFcInterface("component").asInstanceOf[Component]
	    val allSubComponents =
	      	org.objectweb.fractal.julia.control.content.Util.
	      	getAllSubComponents(thisComponent)
	    var result: List[LifeCycleCoordinator] = Nil
	    for( i <- 0 until allSubComponents.size ) {
	        val c = allSubComponents.get(i).asInstanceOf[Component]
	        try {
	            c.getFcInterface("content-controller")
	        }
	        catch {
	          case e:NoSuchInterfaceException =>
	            try {
	            	/*
	            	 * The Java version had this comment:
	            	 * 		"do not remove the cast!"
	            	 * Is it to enable catching ClassClastException?
	            	 */
	                result = result :+
	                		c.getFcInterface("lifecycle-controller").
	                		asInstanceOf[LifeCycleCoordinator]
	            }
	            catch {
	              case f:Exception =>
	                try {
	                	result = result :+
                			c.getFcInterface("/lifecycle-controller").
                			asInstanceOf[LifeCycleCoordinator]
	                }
	                catch {
	                  case ignored:NoSuchInterfaceException => // Indeed nothing
	                }
	            }
	        }
	    }

	    return result
    }
}
