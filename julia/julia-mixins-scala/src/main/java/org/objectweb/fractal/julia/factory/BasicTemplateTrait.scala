/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.factory

import org.objectweb.fractal.api.Type
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.`type`.BasicComponentType
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import scala.collection.mutable.HashMap
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.Fractal
import org.objectweb.fractal.api.factory.GenericFactory

/**
 * Trait providing a basic implementation of the {@link Template} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicTemplateTrait extends BasicControllerTrait with UseComponentTrait
with TemplateTrait {

    /**
     * The functional type of the components instantiated by this template.
     */
    private var fcInstanceType : Type = null
    
    /**
     * The controller and content descriptors of the components instantiated by
     * this template.
     */
    private var fcContent : Array[Object] = null
    
    override def initFcController( ic: InitializationContext ) {
      
	    // initializes the fcContent field
	    fcContent = ic.content.asInstanceOf[Array[Object]]
	
	    // initializes the fcInstanceType field
	    // this type is computed from the type of this component by removing the
	    // factory interface type, and all the control interface types
	    val tmplType = ic.`type`.asInstanceOf[ComponentType]
	    var itfTypes = tmplType.getFcInterfaceTypes
	    var itfList : List[InterfaceType] = Nil
	    itfTypes.foreach( itfType => {
	        val n = itfType.getFcItfName
	        if( !n.equals("factory") && !n.equals("/template") ) {
	        	if( n.equals("attribute-controller") ) {
	        		itfList = itfList :+ itfType
	        	}
	        	else if( !(n.equals("component") || n.endsWith("-controller"))) {
	        		itfList = itfList :+ itfType
	        	}
	        }
	    })
	    itfTypes = itfList.toArray
	    fcInstanceType = new BasicComponentType(itfTypes)
	
	    // calls the overriden method
	    super.initFcController(ic)
    }
    
    override def getFcInstanceType : Type = {
        return fcInstanceType
    }
    
    override def getFcControllerDesc : Object = {
        return fcContent(0)
    }
    
    override def getFcContentDesc : Object = {
        return fcContent(1)
    }
    
    override def newFcInstance : Component = {
        // finds all the direct and indirect sub templates of this template
        // finds all the direct and indirect sub components of this component
        val thisComponent =
        	weaveableC.getFcInterface("component").asInstanceOf[Component]
        val allSubTemplates = Util.getAllSubComponents(thisComponent)

        // instantiates all the templates
        val instances = new HashMap[Component,Component]
        for( i <- 0 until allSubTemplates.size ) {
            val tmpl = allSubTemplates.get(i).asInstanceOf[Component]
            var t : Template = null
            try {
                t = tmpl.getFcInterface("factory").asInstanceOf[Template]
            }
            catch {
              case e: Exception =>
                try {
                	t = tmpl.getFcInterface("factory").asInstanceOf[Template]
                }
                catch {
                  case f: NoSuchInterfaceException =>
		            throw new ChainedInstantiationException(
	            		f, weaveableC,
		            	"All the (sub) templates must provide the Template interface")
                }
            }
            val instance = t.newFcControllerInstance
            instances += tmpl -> instance
        }

        // adds the instances into each other
        for( i <- 0 until allSubTemplates.size ) {
            val tmpl = allSubTemplates.get(i).asInstanceOf[Component]
            val instance = instances(tmpl)
            
            var continue = false
            var tmplCC : ContentController = null
            try {
                tmplCC = tmpl.getFcInterface("content-controller").
                			asInstanceOf[ContentController]
            }
            catch {
              case e: NoSuchInterfaceException =>
                continue = true
            }
            
            if( ! continue ) {
                val instanceCC = instance.getFcInterface("content-controller").
                		asInstanceOf[ContentController]
                val subTemplates = tmplCC.getFcSubComponents
                val subInstances = instanceCC.getFcSubComponents
                for( j <- 0 until subTemplates.length ) {
                    val subInstance =
                    		instances(subTemplates(j)).asInstanceOf[Component]
                    var add = true
                    for( k <- 0 until subInstances.length ) {
			            if( subInstances(k).equals(subInstance) ) {
				            // if sunInstance is already a sub component of
			                // instance, do not add it again (this can happen
			                // with singleton templates)
				            add = false
			          }
                    }
                    if( add ) {
                        instanceCC.addFcSubComponent(subInstance)
                    }
                }
            }
        }

        // binds the instances to each other, as the templates are bound
        for( i <- 0 until allSubTemplates.size ) {
            val tmpl = allSubTemplates.get(i).asInstanceOf[Component]
            val instance = instances(tmpl).asInstanceOf[Component]
            
            var continue = false
            var tmplBC : BindingController = null
            try {
                tmplBC = tmpl.getFcInterface("binding-controller").
                			asInstanceOf[BindingController]
            }
            catch {
              case e: NoSuchInterfaceException =>
                continue = true
            }
            
            if( ! continue ) {
                val instanceBC = instance.getFcInterface("binding-controller").
                					asInstanceOf[BindingController]
                val itfNames = tmplBC.listFc
                for( j <- 0 until itfNames.length ) {
                    val itfName = itfNames(j)
                    val serverItf =
                        tmplBC.lookupFc(itfName).asInstanceOf[Interface]
                    if( serverItf != null ) {
                        val serverTmpl = serverItf.getFcItfOwner
                        val serverInstance = instances(serverTmpl)
                        if( serverInstance != null ) {
                        	// if serverInstance is null, this meands that
                            // 'tmpl' is bound to a component that does not
                            // belong to the root instantiated template: do not
                            // create this binding for 'instance'
                            var itfValue : Object = null
                            if( serverItf.isFcInternalItf ) {
                                val cc = serverInstance.
                                	getFcInterface("content-controller").
                                	asInstanceOf[ContentController]
                                itfValue = cc.getFcInternalInterface(
                                    serverItf.getFcItfName)
                            }
                            else {
                                itfValue = serverInstance.getFcInterface(
                                    serverItf.getFcItfName)
                            }
                            
                            try {
                                if( instanceBC.lookupFc(itfName).equals(itfValue) ) {
                                    continue = true
                                }
                            }
                            catch {
                              case e: Exception => // Indeed nothing
                            }
                            if( ! continue ) {
                              instanceBC.bindFc(itfName,itfValue)
                            }
                        }
                    }
                }
            }
        }

        val x = allSubTemplates.get(0).asInstanceOf[Component]
        return instances(x).asInstanceOf[Component]
    }
    
    override def newFcControllerInstance : Component = {
        if( fcContent(1).isInstanceOf[Component] ) {
            return fcContent(1).asInstanceOf[Component]
        }
        val factory =
        	Fractal.getBootstrapComponent.getFcInterface("generic-factory").
        	asInstanceOf[GenericFactory]
        return factory.newFcInstance(fcInstanceType,fcContent(0),fcContent(1))
    }
}
