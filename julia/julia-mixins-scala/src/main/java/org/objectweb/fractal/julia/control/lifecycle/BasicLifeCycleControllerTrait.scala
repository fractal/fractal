/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.control.content.Util
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.LifeCycleController
import scala.collection.mutable.LinkedList
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext

/**
 * Trait providing an abstract implementation of the {@link
 * LifeCycleController} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicLifeCycleControllerTrait extends BasicControllerTrait
with UseComponentTrait with LifeCycleCoordinatorTrait {

    /**
     * The life cycle state of this component. Zero means stopped, one means
     * stopping and two means started.
     */
    private var fcState: Integer = 0
    
    /**
     * The number of currently executing method calls in this component.
     */
    private var fcInvocationCounter: Integer = 0
    
    /**
     * The coordinator used to stop this component with other components
     * simultaneously.
     */
    private var fcCoordinator: LifeCycleCoordinator = null
    
    
    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------

    override def initFcController( ic: InitializationContext ) = {
      
        try {
            if( ! (ic.getInterface("lifecycle-controller").isInstanceOf[LifeCycleCoordinator]) ) {
                throw new Exception
            }
        }
        catch {
          case e:Exception =>
            ic.getInterface("/lifecycle-coordinator")
        }
        super.initFcController(ic)
    }

    // -------------------------------------------------------------------------
    // Implementation of the LifeCycleController interface
    // -------------------------------------------------------------------------

    override def getFcState: String = {
        return if(fcState==0) LifeCycleController.STOPPED else LifeCycleController.STARTED
    }
    
    override def startFc = {
        if( fcState != 2 ) {
            // _this_ was called in the Java version. Is this a potential issue?
            super.setFcState(true)
        }
    }
    
    override def stopFc = {
        if( fcState == 2 ) {
            val a = new Array[LifeCycleCoordinator](1)
            a(0) = getFcCoordinator
            
            /*
             * _this_ was called in both cases in the Java version. Is this a
             * potential issue?
             */
	        super.stopFc(a)
	        super.setFcState(false)
        }
    }

    // -------------------------------------------------------------------------
    // Implementation of the LifeCycleCoordinator interface
    // -------------------------------------------------------------------------
    
    override def setFcStarted: Boolean = synchronized {
        if( fcState == 2 ) {
        	return false
        }
        fcState = 2;
        /**
         * The following line unblock the threads that may be blocked in the
         * component, in the incrementFcInvocationCounter method.
         */
        notifyAll
        return true
    }

    override def setFcStopping( coordinator: LifeCycleCoordinator ) = synchronized {
        fcState = 1
        fcCoordinator = coordinator
        if( fcInvocationCounter == 0 ) {
        	fcCoordinator.fcInactivated(getFcCoordinator)
        }
    }

    override def setFcStopped: Boolean = synchronized {
        if( fcState == 0 ) {
        	return false
        }
        fcState = 0
        fcCoordinator = null
        return true
    }

    // -------------------------------------------------------------------------
    // Methods called by the associated interceptor
    // -------------------------------------------------------------------------

    /**
     * Increments the number of currently executing method calls in this
     * component. If the component is started this method just increments this
     * counter. If the component is stopped, it waits until the component is
     * restarted, and then increments the counter. Finally, if the component is
     * stopping, there are two cases. If the counter is not null, it is just
     * incremented. If it is null, this method asks the coordinator if the method
     * call can be executed, or if it should be delayed until the component is
     * restarted. The method then blocks or not, depending on the coordinator's
     * response, before incrementing the counter.
     */
    def incrementFcInvocationCounter = {
        var ok: Boolean = false
        do {
	        if (fcState == 0) {
	            ok = false
	        }
	        else if (fcState == 1) {
	            if (fcInvocationCounter == 0) {
	            	ok = fcCoordinator.fcActivated(getFcCoordinator)
	            }
	            else {
	            	ok = true
	            }
	        }
	        else {
	          ok = true
	        }
	        if (!ok) {
	          try { wait() }
	          catch {
	            case e:InterruptedException => // Indeed nothing
	          }
	        }
        } while(!ok)
        fcInvocationCounter += 1
    }

    /**
     * Decrements the number of currently executing method calls in this
     * component. If the component is stopping, and if the counter of currently
     * executing method calls becomes null after being decremented, this method
     * notifies the coordinator that the component has become inactive.
     */
    def decrementFcInvocationCounter = {
        fcInvocationCounter -= 1
        if( fcState != 2 ) {
            /*
             * This test (fcState!=2) has been added since
             * decrementFcInvocationCounter is invoked in all cases with life
             * cycle interceptor. In the Julia version, the method, if
             * fcState==2 the method is not called and the public field
             * fcInvocationCounter is directly decremented. See the Javadoc in
             * LifeCycleSourceCodeGenerator for the reason why we can no longer
             * rely on public fields in the Scala version.
             */
	        if( fcInvocationCounter == 0 ) {
	        	fcCoordinator.fcInactivated(getFcCoordinator)
	        }
        }
    }

    // -------------------------------------------------------------------------
    // Utility methods
    // -------------------------------------------------------------------------

    /**
     * Returns the {@link LifeCycleCoordinator} interface of this component.
     *
     * @return the {@link LifeCycleCoordinator} interface of the component to
     *      which this controller object belongs.
     */
    private def getFcCoordinator : LifeCycleCoordinator = {
        try {
            return weaveableC.getFcInterface("lifecycle-controller").
            		asInstanceOf[LifeCycleCoordinator]
        }
        catch {
          case e:Exception =>
            return weaveableC.getFcInterface("/lifecycle-coordinator").
            		asInstanceOf[LifeCycleCoordinator]
        }
    }
}
