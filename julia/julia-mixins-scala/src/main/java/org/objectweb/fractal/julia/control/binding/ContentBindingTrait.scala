/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import java.lang.Object
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.control.content.UseSuperControllerTrait
import org.objectweb.fractal.api.control.ContentController
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.SuperController

/**
 * Component hierarchy related checks for the {@link BindingController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait ContentBindingTrait extends AbstractBindingControllerTrait
with UseComponentTrait with UseSuperControllerTrait {

    /**
     * Calls the {@link #checkFcLocalBinding checkFcLocalBinding} method and
     * then calls the overriden method.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @param serverItf a server interface.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be created.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      org.objectweb.fractal.api.control.LifeCycleController} interface,
     *      but it is not in an appropriate state to perform this operation.
     */
	override def bindFc(
	    clientItfType: InterfaceType, clientItfName: String, serverItf: Object ) = {
	    
		checkFcLocalBinding( clientItfType, clientItfName, serverItf )
	    super.bindFc( clientItfType, clientItfName, serverItf )
	}

    /**
     * Checks that the given binding is a local binding.
     *
     * @param clientItfType the type of the <tt>clientItfName</tt> interface.
     * @param clientItfName the name of a client interface.
     * @param serverItf a server interface.
     * @throws IllegalBindingException if the given binding is not a local
     *      binding.
     */
	def checkFcLocalBinding(
	    clientItfType: InterfaceType, clientItfName: String, serverItf: Object )
		: Boolean = {  // dirty trick since return with no value cannot be used
	  
	    var sItf: Interface = null
	    var sComp: Component = null
	    try {
	        sItf = serverItf.asInstanceOf[Interface]
	        sComp = sItf.getFcItfOwner
	    }
	    catch {
	      case e:ClassCastException =>
		    // if the server interface does not provide interface introspection
		    // functions, the checks below cannot be performed
	        return true
	    }
	  
    	val cParents = weaveableSC.getFcSuperComponents
    	var msg: String = null
    	if( ! clientItfType.isFcClientItf ) {
    		// internal client interface
    	    var cc: ContentController = null
    	    try {
    	        cc = weaveableC.getFcInterface("content-controller").
    	        		asInstanceOf[ContentController]
    	    }
    	    catch {
    	      case e:NoSuchInterfaceException => return true
    	    }
		    // yes, check export binding:
		    // server component must be a sub component of client component...
	        val cSubComps = cc.getFcSubComponents
	        cSubComps.foreach( cSubComp => {
		        if( cSubComp.equals(sComp) ) {
		          return true
		        }
	        })
	        // ...or equal to client component
	        var thisComp: Component = null
	        try {
	            thisComp = weaveableC.getFcInterface("component").asInstanceOf[Component]
	        }
	        catch {
	          case e:NoSuchInterfaceException =>
		        throw new ChainedIllegalBindingException(
	        		e, weaveableC, sItf.getFcItfOwner, clientItfName,
	        		sItf.getFcItfName,
		        	"Cannot get the Component interface of the client component")
	        }
	        if( sComp.equals(thisComp) && sItf.isFcInternalItf ) {
	        	return true
	        }
	        msg = "Invalid export binding"
    	}
    	else if( sItf.isFcInternalItf ) {
    		// checks that the server component is a parent of the client component
    	    cParents.foreach( cParent => {
    	    	if( sComp.equals(cParent) ) {
	        		return true
	        	}
    	    })
	        msg = "Invalid import binding"
    	}
    	else {
    	    var sCompSC: SuperController = null
    	    try {
    	        sCompSC = sComp.getFcInterface("super-controller").
    	        			asInstanceOf[SuperController]
    	    }
    	    catch {
    	      case e:NoSuchInterfaceException =>
		        // if the server component does not have a SuperController
		        // interface, the checks below cannot be performed
		        return true
    	    }
    	    val sParents = sCompSC.getFcSuperComponents
    	    // checks that the client and server components have a common parent
    	    cParents.foreach( cParent => {
    	        sParents.foreach( sParent => {
    	            if( cParent.equals(sParent) ) {
    	                return true
    	            }
    	        })
    	    })
    		msg = "Not a local binding"
    	}
    	
        throw new ChainedIllegalBindingException(
    		null, weaveableC, sItf.getFcItfOwner, clientItfName,
    		sItf.getFcItfName, msg)
    }
}
