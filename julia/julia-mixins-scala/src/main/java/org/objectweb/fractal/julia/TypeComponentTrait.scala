/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.Type
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType

/**
 * Trait implementing a basic type system related checks to a {@link Component}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait TypeComponentTrait extends BasicControllerTrait with BasicComponentTrait {
  
    /**
     * Checks the interface name against the component's type and then calls the
     * overriden method. This method also creates the collection interfaces
     * when needed, and puts them in the {@link #_this_fcInterfaces} map.
     * 
     * @param interfaceName
     *          the name of the external interface that must be returned
     * @return  the external interface of the component to which this interface
     *          belongs, whose name is equal to the given name
     * @throws NoSuchInterfaceException  if there is no such interface
     */
    override def getFcInterface( interfaceName: String ) : Object = {
	    
        if( interfaceName.indexOf('/') == 0 ) {
	      return super.getFcInterface(interfaceName)
	    }
	    
	    val compType = getFcType.asInstanceOf[ComponentType]
	    var itfType = compType.getFcInterfaceType(interfaceName)
	    
	    var result : Object = null
	    try {
	        result = super.getFcInterface(interfaceName)
	    }
	    catch {
	        case e: NoSuchInterfaceException => {
		        if ( itfType.isFcCollectionItf ) {
			        result = super.getFcInterface("/collection/" + itfType.getFcItfName)
			        /*
			         * result = result.asInstanceOf[ComponentInterface].clone
			         * 
			         * generates the following error with Scala 2.8.1:
			         * method clone cannot be accessed in org.objectweb.fractal.julia.ComponentInterface
			         * 
			         * A quick search in Google returns mixed opinions with
			         * issues existing with the clone() method, some being
			         * apparently fixed. To avoid the issue, 2 options seem to
			         * exist:
			         * (1) using dynamic call (such as below),
			         * (2) subclassing BasicComponentInterface with a myclone()
			         * method which calls super.clone(), and invoking this
			         * myclone() method here (yet, the condition is to use this
			         * new subclass for all interfaces - see the interface class
			         * generator.)
			         */
			        val m = result.getClass.getMethod("clone")
			        result = m.invoke(result)
			        result.asInstanceOf[ComponentInterface].setFcItfName(interfaceName)
			        fcInterfaces.put(interfaceName,result)
		        }
		        else {
		        	throw e
		        }
	        }
	    }
	    
	    return result
    }
}
