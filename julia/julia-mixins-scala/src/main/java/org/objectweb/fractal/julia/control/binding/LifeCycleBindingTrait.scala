/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import org.objectweb.fractal.api.control.LifeCycleController
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerTrait
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Life cycle related checks for the {@link BindingController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait LifeCycleBindingTrait extends AbstractBindingControllerTrait
with UseComponentTrait with UseLifeCycleControllerTrait {

    /**
     * Checks that the component is stopped and then calls the overriden method.
     *
     * @param clientItfName the name of a client interface of the component to
     *      which this interface belongs.
     * @throws NoSuchInterfaceException if there is no such client interface.
     * @throws IllegalBindingException if the binding cannot be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      LifeCycleController} interface, but it is not in an appropriate
     *      state to perform this operation.
     */
	override def unbindFc( clientItfName: String ) = {
	    if( weaveableLC != null ) {
	        val state = weaveableLC.getFcState
	        if( ! LifeCycleController.STOPPED.equals(state) ) {
	        	throw new ChainedIllegalLifeCycleException(
        			null, weaveableC, "The component is not stopped")
	      }
	    }
	    super.unbindFc(clientItfName)
	}
}
