/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.control.content.Util
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.LifeCycleController
import scala.collection.mutable.LinkedList

/**
 * Trait providing an abstract implementation of the {@link
 * LifeCycleCoordinator} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicLifeCycleCoordinatorTrait extends LifeCycleCoordinatorTrait
with UseComponentTrait {

    /**
     * The components that are currently active.
     * 
     * Use java.util.List here instead of immutable List. Since this method
     * uses synchronized on fcActive and that fcActive may change due to adds
     * and removes, I guess this wouldn't work correctly if immutable objects
     * were used. I though it would work with immutable LinkedList but it
     * happens that I may not have fully understood what it means for Scala to
     * have an mutable collection: the documentation says that the head and the
     * tail are mutable, but whenever a new element is appended with :+ a new
     * collection is returned. This seems to be safer to revert to
     * java.util.List.
     */
    private var fcActive: java.util.List[LifeCycleCoordinator] = null

    /**
     * Sets the lifecycle state of this component and of all its direct and
     * indirect sub components that have a {@link LifeCycleCoordinator} interface.
     *
     * @param started <tt>true</tt> to set the lifecycle state of the components
     *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
     *      {@link #STOPPED STOPPED}.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    override def setFcState( started: Boolean ) = {
      
         // finds all the direct and indirect sub components of this component
        val thisComponent = weaveableC.getFcInterface("component").asInstanceOf[Component]
        val allSubComponents =
          Util.getAllSubComponents(thisComponent).
          asInstanceOf[java.util.List[Component]]

        // sets the state of these components
        for( i <- 0 until allSubComponents.size ) {
        	var continue: Boolean = false
        	var lc: LifeCycleCoordinator = null
            val c = allSubComponents.get(i)
            try {
                lc = c.getFcInterface("lifecycle-controller").asInstanceOf[LifeCycleCoordinator]
            }
            catch {
              case e:Exception =>
                try {
                	lc = c.getFcInterface("lifecycle-controller").asInstanceOf[LifeCycleCoordinator]
                }
                catch {
                  case f:NoSuchInterfaceException => continue=true
                }
            }
            
            if( ! continue ) {
                if(started) {
                    lc.setFcStarted
                }
                else {
                    lc.setFcStopped
                }
            }
        }
    }
    
    override def fcActivated( component: LifeCycleCoordinator) : Boolean = {
        fcActive.synchronized( {
        	// a component can become active iff another component is already active
	        if( fcActive.size > 0) {
	        	if( ! fcActive.contains(component) ) {
	        		fcActive.add(component)
	        	}
	        	return true
	        }
	        return false
        })
    }

    override def fcInactivated( component: LifeCycleCoordinator) = {
        fcActive.synchronized( {
	        fcActive.remove(component)
	        // notifies the thread that may be blocked in stopFc
	        fcActive.notifyAll
        })
    }
    
    /**
     * Stops the given components simultaneously. This method sets the state of
     * the components to "<tt>STOPPING</tt>", waits until all the components
     * are simultaneoulsy inactive (their state is known thanks to the {@link
     * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated} callback
     * methods), and then sets the state of the components to {@link #STOPPED
     * STOPPED}.
     *
     * @param components the {@link LifeCycleCoordinator} interface of the
     *      components to be stopped.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    override def stopFc( components: Array[LifeCycleCoordinator] ) = {
      
        // initializes the fcActive list
        fcActive = new java.util.ArrayList[LifeCycleCoordinator]
        components.foreach( component => {
            if( component.getFcState.equals(LifeCycleController.STARTED) ) {
                fcActive.add(component)
            }
        })

        // sets the state of the components to STOPPING
    	var c: LifeCycleCoordinator = null
        try {
            c = weaveableC.getFcInterface("lifecycle-controller").
            		asInstanceOf[LifeCycleCoordinator]
        }
        catch {
          case e:Exception =>
            try {
            	c = weaveableC.getFcInterface("lifecycle-controller").
            			asInstanceOf[LifeCycleCoordinator]
            }
            catch {
              case f:NoSuchInterfaceException =>
              	throw new ChainedIllegalLifeCycleException(
              		f, weaveableC, "Cannot stop components" )
            }
        }

        components.foreach( component => {
            if( component.getFcState.equals(LifeCycleController.STARTED) ) {
                component.setFcStopping(c)
            }
        })

        // waits until all the components are simultaneously inactive
        fcActive.synchronized( {
        	while( fcActive.size > 0 ) {
        	    fcActive.wait
        	}
        })
        
        // sets the state of the components to STOPPED
        components.foreach( component => {
            if( component.getFcState.equals(LifeCycleController.STARTED) ) {
                component.setFcStopped
            }
        })

        fcActive = null
    }
}
