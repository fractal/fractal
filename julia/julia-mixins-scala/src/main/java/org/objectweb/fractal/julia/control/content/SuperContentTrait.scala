/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Provides {@link SuperControllerNotifier notification} functions to a {@link
 * ContentController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait SuperContentTrait extends AbstractContentControllerTrait
with UseComponentTrait {

  /**
   * Calls the overriden method and then notifies the given component it has
   * been added in this component. This method does nothing if the given sub
   * component does not provide the {@link SuperControllerNotifier} interface.
   *
   * @param subComponent the component to be added inside this component.
   * @throws IllegalContentException if the given component cannot be added
   *      inside this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */
  override def addFcSubComponent( subComponent: Component ) = {
    super.addFcSubComponent(subComponent)
    val scn = getFcSuperControllerNotifier(subComponent)
    if (scn != null) {
      try {
        val c = weaveableC.getFcInterface("component").asInstanceOf[Component]
        scn.addedToFc(c)
      }
      catch {
        case ignored: NoSuchInterfaceException =>
      }
    }
  }

  /**
   * Calls the overriden method and then notifies the given component it has
   * been removed from in this component. This method does nothing if the given
   * sub component does not provide the {@link SuperControllerNotifier}
   * interface.
   *
   * @param subComponent the component to be removed from this component.
   * @throws IllegalContentException if the given component cannot be removed
   *      from this component.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but
   *      it is not in an appropriate state to perform this operation.
   */
  override def removeFcSubComponent( subComponent: Component ) = {
    super.removeFcSubComponent(subComponent)
    val scn = getFcSuperControllerNotifier(subComponent)
    if (scn != null) {
      try {
        val c = weaveableC.getFcInterface("component").asInstanceOf[Component]
        scn.removedFromFc(c)
      }
      catch {
        case ignored: NoSuchInterfaceException =>
      }
    }
  }

  /**
   * Returns the {@link SuperControllerNotifier} interface of the given 
   * component.
   * 
   * @param c a component.
   * @return the {@link SuperControllerNotifier} interface of the given 
   *      component, or <tt>null</tt>.
   */
  private def getFcSuperControllerNotifier( c: Component ) :
    SuperControllerNotifier = {
    
    try {
      return c.getFcInterface("super-controller").
    		   asInstanceOf[SuperControllerNotifier]
    }
    catch {
      case e: Exception =>
      try {
        return c.getFcInterface("/super-controller-notifier").
        	     asInstanceOf[SuperControllerNotifier]
      }
      catch {
        case ignored: NoSuchInterfaceException => return null
      }
    }
  }
}
