/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import java.lang.Object

import scala.collection.mutable.HashMap

/**
 * Trait implementing the functionalities of the {@link BindingController}. This
 * trait uses a map to store bindings.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait BasicBindingControllerTrait extends AbstractBindingControllerTrait {

    /**
     * The bindings of the component to which this controller object belongs.
     * This map associates to each client interface name the server interface to
     * which it is bound. If a client interface is not bound, its name is not
     * associated to <tt>null</tt>, but to the {@link #fcBindings} map itself.
     * This way the {@link #listFc listFc} method returns the names of all the
     * client interfaces of the component, and not only the names of the
     * interfaces that are bound.
     */
    protected var fcBindings: HashMap[String,Object] = null

    
    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------

    override def listFc : Array[String] = {
		if( fcBindings == null ) {
		    return new Array[String](0)
		}
        
        return fcBindings.keySet.toArray
    }
    
    override def lookupFc( clientItfName: String ) : Object = {
		if( fcBindings == null ) {
		    return null
		}
		val serverItf =
		  if(fcBindings.contains(clientItfName)) fcBindings(clientItfName)
		  else null
		  val ret = if(serverItf==fcBindings) null else serverItf
        return ret
    }
    
    override def bindFc( clientItfName: String, serverItf: Object ) = {
    	if( fcBindings == null ) {
    	    fcBindings = new HashMap[String,Object]
    	}
    	fcBindings += clientItfName -> serverItf
    }
    
    override def unbindFc( clientItfName: String ) = {
        if( fcBindings != null ) {
            fcBindings += clientItfName -> fcBindings
        }
    }
}
