/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.binding

import java.lang.Object

import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor
import org.objectweb.fractal.julia.UseComponentTrait

/**
 * Trait providing output interceptors management to a {@link
 * BindingController}. This trait is only useful for the {@link
 * ContainerBindingControllerMixin}, since the {@link CompositeBindingMixin} and
 * the {@link OptimizedCompositeBindingMixin} already manage interceptors.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait InterceptorBindingTrait extends AbstractBindingControllerTrait
with UseComponentTrait {

    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------

    override def lookupFc( clientItfName: String ) : Object = {
        var o = super.lookupFc(clientItfName)
        if( o.isInstanceOf[Interceptor] ) {
		    // skips the output interceptor, if there is one
		    o = o.asInstanceOf[Interceptor].getFcItfDelegate
        }
        return o
    }
    
    override def bindFc( clientItfName: String, serverItf: Object ) = {
	    var o = weaveableC.getFcInterface(clientItfName)
	    if( o.isInstanceOf[ComponentInterface] ) {
	        val next = o.asInstanceOf[ComponentInterface].getFcItfImpl
	        if( next.isInstanceOf[Interceptor] ) {
	        	next.asInstanceOf[Interceptor].setFcItfDelegate(serverItf)
	        	o = next
	        }
	        else {
	        	o.asInstanceOf[ComponentInterface].setFcItfImpl(serverItf)
	        	o = serverItf
	        }
	    }
	    else {
	        o = serverItf
	    }
	    super.bindFc(clientItfName,o)
    }
    
    override def unbindFc( clientItfName: String ) = {
	    var o = weaveableC.getFcInterface(clientItfName)
	    if( o.isInstanceOf[ComponentInterface] ) {
	        val next = o.asInstanceOf[ComponentInterface].getFcItfImpl
	        if( next.isInstanceOf[Interceptor] ) {
	        	next.asInstanceOf[Interceptor].setFcItfDelegate(null)
	        }
	        else {
	        	o.asInstanceOf[ComponentInterface].setFcItfImpl(null)
	        }
	    }
	    super.unbindFc(clientItfName)
    }
}
