/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

import org.objectweb.fractal.api.Component
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.control.LifeCycleController

/**
 * Trait providing an abstract implementation of the {@link
 * LifeCycleCoordinator} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait ContainerLifeCycleTrait extends LifeCycleCoordinatorTrait
with UseComponentTrait {

    override def setFcStarted: Boolean = {
        this.synchronized({
            if( super.setFcStarted ) {
                setFcContentState(true)
                return true
            }
        })
        return false
    }
    
    override def setFcStopped: Boolean = {
        this.synchronized({
            if( super.setFcStopped ) {
                setFcContentState(false)
                return true
            }
        })
        return false
    }

    /**
     * Calls the {@link LifeCycleController#startFc startFc} or {@link
     * LifeCycleController#stopFc stopFc} method on the encapsulated component.
     * This method does nothing if there is no encapsulated component, or if it
     * does not implement the {@link LifeCycleController} interface.
     *
     * @param started <tt>true</tt> to call the {@link LifeCycleController#startFc
     *      startFc}, or <tt>false</tt> to call the {@link
     *      LifeCycleController#stopFc stopFc} method.
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    private def setFcContentState( started: Boolean ) : Boolean = {
        // Do nothing Boolean return type; just to be able to invoke return
      
        var content: Object = null
        try {
            content = weaveableC.getFcInterface("/content")
        }
        catch {
          case e: NullPointerException => return true
          case e: NoSuchInterfaceException => return true
        }
        
        if( content == this ) {
	        // case of merge...AndContent options
	        if( this.isInstanceOf[ContentLifeCycleController] ) {
		        if(started) {
		            this.asInstanceOf[ContentLifeCycleController].startFcContent
		        }
		        else {
		            this.asInstanceOf[ContentLifeCycleController].stopFcContent
		        }
	        }
        }
        else {
            if( content.isInstanceOf[LifeCycleController] ) {
                if(started) {
                	content.asInstanceOf[LifeCycleController].startFc
                }
                else {
                	content.asInstanceOf[LifeCycleController].stopFc
                }
            }
        }
        
        return true
    }
}
