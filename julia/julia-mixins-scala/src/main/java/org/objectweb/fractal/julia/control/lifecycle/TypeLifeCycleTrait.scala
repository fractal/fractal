/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException

/**
 * Trait providing basic type system related checks to a {@link
 * LifeCycleController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait TypeLifeCycleControllerTrait extends LifeCycleCoordinatorTrait
with UseComponentTrait {

    /**
     * Checks the mandatory client interfaces of the component (and of all its
     * sub components) and then calls the overriden method.
     * 
     * @throws IllegalLifeCycleException if a problem occurs.
     */
    override def startFc = {

        // finds all the direct and indirect sub components of this component
        val thisComponent = weaveableC.getFcInterface("component").asInstanceOf[Component]
        val allSubComponents = Util.getAllSubComponents(thisComponent)

        // checks that the mandatory client interfaces of these components are bound
        for( i <- 0 until allSubComponents.size ) {
            val subComponent = allSubComponents.get(i).asInstanceOf[Component]
            try {
            	checkFcMandatoryInterfaces(subComponent)
            }
            catch {
              case cibe: ChainedIllegalBindingException =>
                throw new ChainedIllegalLifeCycleException(
                		cibe,weaveableC,"Cannot start the component")
            }
        }

    	// calls the overriden method
    	super.startFc
    }

    /**
     * Checks that all the mandatory client interface of the given component are
     * bound.
     *
     * @param c a component.
     * @throws IllegalBindingException if a mandatory client interface of the
     *      given component is not bound.
     */
    private def checkFcMandatoryInterfaces( c: Component ) : Boolean = {
      
        var bc: BindingController = null
        try {
            bc = c.getFcInterface("binding-controller").asInstanceOf[BindingController]
        }
        catch {
          case e:NoSuchInterfaceException => return true
        }
      
        val compType = c.getFcType.asInstanceOf[ComponentType]
        val names = bc.listFc
        names.foreach( name => {
            var continue: Boolean = false
            var itfType: InterfaceType = null
            try {
                itfType = compType.getFcInterfaceType(name)
            }
            catch {
              case e:NoSuchInterfaceException => continue=true
            }
            if( ! continue ) {
                var continue2: Boolean = false
        	    if( itfType.isFcClientItf && !itfType.isFcOptionalItf ) {
        	        var sItf: Object = null
        	        try {
        	            sItf = bc.lookupFc(name)
        	        }
        	        catch {
        	          case e:NoSuchInterfaceException => continue2=true
        	        }
        	        if( ! continue2 ) {
        	    	    if (sItf == null) {
        	    		    throw new ChainedIllegalBindingException(
        	    				  null, c, null, name, null,
        	    				  "Mandatory client interface unbound")
			          }
        	      }
        	  }
          }
      })
      
      return true
    }
}
