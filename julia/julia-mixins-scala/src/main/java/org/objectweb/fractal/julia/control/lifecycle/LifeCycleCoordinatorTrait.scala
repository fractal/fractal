/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.lifecycle

/**
 * Abstract implementation of the {@link LifeCycleCoordinator} interface to
 * enable calling super.aMethod in traits which use this trait.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait LifeCycleCoordinatorTrait extends LifeCycleCoordinator {
	override def startFc = {}
    override def setFcStarted: Boolean = return false
    override def setFcStopping( coordinator: LifeCycleCoordinator ) = {}
    override def setFcStopped: Boolean = return false
    override def fcActivated( component: LifeCycleCoordinator ) : Boolean = return false
    override def fcInactivated( component: LifeCycleCoordinator ) = {}
    def setFcState( started: Boolean ) = {}
    def stopFc( clccs: Array[LifeCycleCoordinator] ) = {}
}
