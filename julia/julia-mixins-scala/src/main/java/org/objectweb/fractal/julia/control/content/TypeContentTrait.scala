/***
 * Julia
 * Copyright (C) 2011-2016 Inria, Universite Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.control.content

import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.julia.ComponentInterface

/**
 * Basic type system checks for the {@link ContentController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 2.5.2
 */
trait TypeContentTrait extends AbstractContentControllerTrait
with UseComponentTrait with BasicContentControllerTrait {

  /**
   * Checks the interface name against the component's type and then calls the
   * overriden method. This method also creates the collection interfaces
   * when needed, and puts them in the {@link #_this_fcInternalInterfaces} map.
   *
   * @param interfaceName the name of the internal interface that must be
   *      returned.
   * @return the internal interface of the component to which this interface
   *      belongs, whose name is equal to the given name.
   * @throws NoSuchInterfaceException if there is no such interface.
   */
  override def getFcInternalInterface( interfaceName: String ) : Object = {

    val compType = weaveableC.getFcType.asInstanceOf[ComponentType]
    val itfType = compType.getFcInterfaceType(interfaceName)
    try {
      val result = super.getFcInternalInterface(interfaceName)
      return result
    }
    catch {
      case nsie: NoSuchInterfaceException =>
        if( itfType.isFcCollectionItf ) {
          val collectionName = "/collection/" + itfType.getFcItfName
          var result = super.getFcInternalInterface(collectionName)
          /*
           * result = result.asInstanceOf[ComponentInterface].clone
           * 
           * generates the following error with Scala 2.9.1:
           * method clone cannot be accessed in org.objectweb.fractal.julia.ComponentInterface
           * 
           * A quick search in Google returns mixed opinions with issues
           * existing with the clone() method, some being apparently fixed. To
           * avoid the issue, 2 options seem to exist:
           * (1) using dynamic call (such as below),
           * (2) subclassing BasicComponentInterface with a myclone() method
           * which calls super.clone(), and invoking this myclone() method here
           * (yet, the condition is to use this new subclass for all interfaces
           * - see the interface class generator.)
           */
	      val m = result.getClass.getMethod("clone")
	      result = m.invoke(result)
          result.asInstanceOf[ComponentInterface].setFcItfName(interfaceName)
          fcInternalInterfaces.put(interfaceName,result)
          return result
        }
        else {
          throw nsie
        }
    }
  }
}
