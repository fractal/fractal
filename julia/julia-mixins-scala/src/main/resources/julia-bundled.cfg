###############################################################################
# STANDARD JULIA CONFIGURATION FILE - DO NOT EDIT
#
# PUT NEW OR OVERRIDEN DEFINITIONS AT THE END OF THE FILE, OR IN OTHER FILES
###############################################################################

# -----------------------------------------------------------------------------
# INTERFACE CLASS GENERATORS
# -----------------------------------------------------------------------------

# default class generator, generates sub classes of BasicComponentInterface

(interface-class-generator
  (org.objectweb.fractal.julia.asm.InterfaceClassGenerator
    org.objectweb.fractal.julia.BasicComponentInterface
  )
)

# -----------------------------------------------------------------------------
# CONTROLLER INTERFACES
#
# each definition must be of the form (interface-name interface-signature)
# -----------------------------------------------------------------------------

# Component interface

(component-itf
  (component org.objectweb.fractal.api.Component)
)

# TypeFactory interface

(type-factory-itf
  (type-factory org.objectweb.fractal.api.type.TypeFactory)
)

# GenericFactory interface

(generic-factory-itf
  (generic-factory org.objectweb.fractal.api.factory.GenericFactory)
)

# Factory interface

(factory-itf
  # choose one of the following definitions:
  # the first one provides only the Fractal Factory interface
  # the second one provides a Julia extension of the Factory interface
  # (factory org.objectweb.fractal.api.factory.Factory)
  (factory org.objectweb.fractal.julia.factory.Template)
)

(julia-factory-itf
  (/template org.objectweb.fractal.julia.factory.Template)
)

# AttributeController interface

(attribute-controller-itf
  (attribute org.objectweb.fractal.api.control.AttributeController)
)

(julia-attribute-controller-itf
  (/cloneable-attribute-controller org.objectweb.fractal.julia.control.attribute.CloneableAttributeController)
)

# BindingController interface

(binding-controller-itf
  (binding-controller org.objectweb.fractal.api.control.BindingController)
)

# ContentController interface

(content-controller-itf
  (content-controller org.objectweb.fractal.api.control.ContentController)
)

# SuperController interface

(super-controller-itf
  # choose one of the following definitions:
  # the first one provides only the Fractal SuperController interface
  # the second one provides a Julia extension of the SuperController interface
  # (super-controller org.objectweb.fractal.api.control.SuperController)
  (super-controller org.objectweb.fractal.julia.control.content.SuperControllerNotifier)
)

(julia-super-controller-itf
  (/super-controller-notifier org.objectweb.fractal.julia.control.content.SuperControllerNotifier)
)

# LifeCycleController interface

(lifecycle-controller-itf
  # choose one of the following definitions:
  # the first one provides only the Fractal LifeCycleController interface
  # the second one provides a Julia extension of the LifeCycleController interface
  # (lifecycle-controller org.objectweb.fractal.api.control.LifeCycleController)
  (lifecycle-controller org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator)
)

(julia-lifecycle-controller-itf
  (/lifecycle-coordinator org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator)
)

# NameController interface

(name-controller-itf
  (name-controller org.objectweb.fractal.api.control.NameController)
)

# -----------------------------------------------------------------------------
# CONTROLLER OBJECTS
#
# each definition must be an object descriptor
# -----------------------------------------------------------------------------

# Component implementation

(component-impl
  (org.objectweb.fractal.julia.ComponentControllerImpl)
)

# TypeFactory implementation

(type-factory-impl
  (org.objectweb.fractal.julia.type.TypeFactoryImpl)
)

# GenericFactory implementation

(generic-factory-impl
  (org.objectweb.fractal.julia.factory.GenericFactoryImpl)
)

# Factory implementation (for template components)

(factory-impl
  (org.objectweb.fractal.julia.factory.FactoryImpl)
)

# Factory implementation (for singleton template components)

(singleton-factory-impl
  (org.objectweb.fractal.julia.factory.SingletonFactoryImpl)
)

# BindingController implementation (for primitive components without content)

(primitive-binding-controller-impl
  (org.objectweb.fractal.julia.control.binding.PrimitiveBindingControllerImpl)
)

# BindingController implementation (for primitive components with content)

(container-binding-controller-impl
  (org.objectweb.fractal.julia.control.binding.ContainerBindingControllerImpl)
)

# BindingController implementation (for composite components)

(composite-binding-controller-impl
  (org.objectweb.fractal.julia.control.binding.CompositeBindingControllerImpl)
)

# ContentController implementation

(content-controller-impl
  (org.objectweb.fractal.julia.control.content.ContentControllerImpl)
)

# SuperController implementation

(super-controller-impl
  (org.objectweb.fractal.julia.control.content.SuperControllerImpl)
)

# LifeCycleController implementation (for primitive or composite components)

(lifecycle-controller-impl
  (org.objectweb.fractal.julia.control.lifecycle.LifeCycleControllerImpl)
)

# LifeCycleController implementation (for composite components only)

(composite-lifecycle-controller-impl
  (org.objectweb.fractal.julia.control.lifecycle.CompositeLifeCycleControllerImpl)
)

# NameController implementation

(name-controller-impl
  (org.objectweb.fractal.julia.control.name.NameControllerImpl)
)

# -----------------------------------------------------------------------------
# CONTROLLER DESCRIPTORS
# -----------------------------------------------------------------------------

(optimizationLevel
  # choose one of the following optimization options:
  none
  # mergeControllers
  # mergeControllersAndInterceptors
  # mergeControllersAndContent
  # mergeControllersInterceptorsAndContent
)

(bootstrap
  (
    'interface-class-generator
    (
      'component-itf
      'type-factory-itf
      'generic-factory-itf
      (loader org.objectweb.fractal.julia.loader.Loader)
    )
    (
      'component-impl
      'type-factory-impl
      'generic-factory-impl
      # choose one of the following classes:
      # the first one loads all classes from the classpath
      # the second one can generate missing classes on the fly, dynamically
      # org.objectweb.fractal.julia.loader.BasicLoader
      org.objectweb.fractal.julia.loader.DynamicLoader
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    none
  )
)

(primitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'container-binding-controller-impl
      'super-controller-impl
      'lifecycle-controller-impl
      'name-controller-impl
    )
    (
      (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
        org.objectweb.fractal.julia.control.lifecycle.ScalaLifeCycleCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(parametricPrimitive
  'primitive
)

(composite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'composite-lifecycle-controller-impl
      'name-controller-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(parametricComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'composite-lifecycle-controller-impl
      'name-controller-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(primitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      'primitive-binding-controller-impl
      'super-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(parametricPrimitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'julia-attribute-controller-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'primitive-binding-controller-impl
      'super-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(compositeTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(parametricCompositeTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'julia-attribute-controller-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

###############################################################################
# CUSTOM CONFIGURATION INFORMATION
###############################################################################

# -----------------------------------------------------------------------------
# Definitions for a "flat" component model, without composite components
# ContentController, SuperController and all Content related mixins are not used
# -----------------------------------------------------------------------------

(flat-primitive-binding-controller-impl
  (org.objectweb.fractal.julia.control.binding.FlatPrimitiveBindingControllerImpl)
)

(flat-container-binding-controller-impl
  (org.objectweb.fractal.julia.control.binding.FlatContainerBindingControllerImpl)
)

(flatPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'flat-container-binding-controller-impl
      'lifecycle-controller-impl
      'name-controller-impl
    )
    (
      (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
        org.objectweb.fractal.julia.control.lifecycle.ScalaLifeCycleCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(flatParametricPrimitive
  'flatPrimitive
)

(flatPrimitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      'flat-primitive-binding-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(flatParametricPrimitiveTemplate
  (
    'interface-class-generator
    (
      'component-itf
      'julia-attribute-controller-itf
      'binding-controller-itf
      'name-controller-itf
      'factory-itf
      # only if factory-itf does not designate the Julia interface:
      # 'julia-factory-itf
    )
    (
      'component-impl
      ((org.objectweb.fractal.julia.asm.AttributeControllerClassGenerator
        ('attributeControllerInterface)
      ))
      'flat-primitive-binding-controller-impl
      'name-controller-impl
      'factory-impl
    )
    (
      # no interceptors
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)


# -----------------------------------------------------------------------------
# Definitions for the interceptor tests
# -----------------------------------------------------------------------------

(stat-controller-itf
  (stat-controller org.objectweb.fractal.julia.conform.controllers.StatController)
)

(statPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'stat-controller-itf
    )
    (
      'component-impl
      'container-binding-controller-impl
      'super-controller-impl
      'lifecycle-controller-impl
      'name-controller-impl
      org.objectweb.fractal.julia.conform.controllers.BasicStatController
    )
    (
      (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
        org.objectweb.fractal.julia.control.lifecycle.ScalaLifeCycleCodeGenerator
        org.objectweb.fractal.julia.conform.controllers.StatCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(statComposite
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'content-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
      'stat-controller-itf
    )
    (
      'component-impl
      'composite-binding-controller-impl
      'content-controller-impl
      'super-controller-impl
      'composite-lifecycle-controller-impl
      'name-controller-impl
      org.objectweb.fractal.julia.conform.controllers.BasicStatController
    )
    (
      (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
        org.objectweb.fractal.julia.conform.controllers.StatCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

# -----------------------------------------------------------------------------
# Bad definitions to test errors detection
# -----------------------------------------------------------------------------

(badPrimitive
  (
    'interface-class-generator
    (
      'component-itf
      'binding-controller-itf
      'super-controller-itf
      # only if super-controller-itf does not designate the Julia interface:
      # 'julia-super-controller-itf
      'lifecycle-controller-itf
      # only if lifecycle-controller-itf does not designate the Julia interface:
      # 'julia-lifecycle-controller-itf
      'name-controller-itf
    )
    (
      'component-impl
      'container-binding-controller-impl
      'super-controller-impl
      # implementation missing for the LifeCycleController interface
      # 'lifecycle-controller-impl
      'name-controller-impl
    )
    (
      (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
        org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator
      )
    )
    org.objectweb.fractal.julia.asm.MergeClassGenerator
    'optimizationLevel
  )
)

(badParametricPrimitive
  'badPrimitive
)
