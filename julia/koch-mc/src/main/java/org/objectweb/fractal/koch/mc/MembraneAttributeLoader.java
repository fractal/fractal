/***
 * Koch Membrane Compiler
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.mc;

import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Definition;
import org.objectweb.fractal.adl.attributes.AttributeLoader;
import org.objectweb.fractal.adl.attributes.AttributesContainer;
import org.objectweb.fractal.adl.components.Component;
import org.objectweb.fractal.adl.components.ComponentContainer;
import org.objectweb.fractal.adl.components.ComponentDefinition;

/**
 * This class replaces the default attribute loader defined by Fractal ADL which
 * uses Java reflection to check that the signature of an attribute control
 * interface defines the setter and the getter associated to the defined
 * attribute. As the interface associated to the signature may not be available
 * in the classpath, we do not want Fractal ADL to return an exception when
 * the setter and the getter methods are not found. This class does not perform
 * this check.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneAttributeLoader extends AttributeLoader {
  
  // --------------------------------------------------------------------------
  // Implementation of the Loader interface
  // --------------------------------------------------------------------------
  
  public Definition load (final String name, final Map context)
    throws ADLException
  {
    Definition d = clientLoader.load(name, context);
    boolean extend = false;
    if (d instanceof ComponentDefinition) {
      extend = ((ComponentDefinition)d).getExtends() != null;
    }
    checkNode(d, extend, context);
    return d;
  }

  // --------------------------------------------------------------------------
  // Checking methods
  // --------------------------------------------------------------------------
  
  private void checkNode (
    final Object node, 
    final boolean extend, 
    final Map context) throws ADLException 
  {
    if (node instanceof AttributesContainer) {
      // checkAttributesContainer((AttributesContainer)node, extend, context);
    }
    if (node instanceof ComponentContainer) {
      Component[] comps = ((ComponentContainer)node).getComponents();
      for (int i = 0; i < comps.length; i++) {
        checkNode(comps[i], extend, context);
      }
    }
  }

}
