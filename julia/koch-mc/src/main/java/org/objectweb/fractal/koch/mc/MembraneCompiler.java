/***
 * Koch Membrane Compiler
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.mc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;

/**
 * A Java source code generator for Fractal control components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneCompiler {
    
    public static void main(String[] args) throws Exception {
        
        if( args.length < 3 || ! args[0].equals("-d") ) {
            usage();
            return;
        }
        
        File genDir = new File(args[1]);
        MembraneCompiler mc = new MembraneCompiler(genDir);
        
        for (int i = 2; i < args.length; i++) {
            mc.generate(args[i]);
        }
    }
    
    private static void usage() {
        System.out.println("java "+MembraneCompiler.class.getName()+" -d outputdir files");
    }
    
    public MembraneCompiler( File genDir ) {
        this.genDir = genDir;
    }
    private File genDir;

    /**
     * Generate the source code of a class which implements the {@link
     * org.objectweb.fractal.api.factory.Factory} interface. This
     * class instantiates components described by the specified ADL.
     * 
     * @param adl  the fully-qualified name of the ADL descriptor
     */
    public void generate( String adl ) throws ADLException, IOException {
        System.out.println(adl);
        
        /*
         * Instantiates YesClassLoader with the parent classloader of this
         * class. If this is not done (e.g. just like instantiating
         * YesClassLoader without any parent), a default class loader is used.
         * When run under maven, this default class loader can not load
         * resource streams such as the .fractal file which are bundled in
         * dependent .jar files (such as juliak.jar).
         * The trick is not needed under eclipse, but the code is kept as this
         * to preserve compatibility with maven.
         */
        ClassLoader cl = new YesClassLoader(getClass().getClassLoader());
        
        String backend = MembraneBackendDef.STATIC_BACKEND;
        Map context = new HashMap();
        context.put("classloader",cl);
        generate(adl,backend,context);
    }
    
    /**
     * Fractal ADL checks that referenced Java types (e.g. interface signatures,
     * content classes) exist. This is safe but prevent ADLs from being parsed
     * if some referenced types are missing. If fact, in all cases except for
     * attribute controller signatures, Fractal ADL simply checks that loading
     * the class does not throw {@link ClassNotFoundException}.
     * 
     * This class loader returns its own class (any other class could also being
     * returned). By this way, Fractal ADL is made to believe that the class
     * exists.
     * 
     * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
     */
    private static class YesClassLoader extends ClassLoader {
        public YesClassLoader( ClassLoader parent ) {
           super(parent);
        }
        public Class loadClass( String name ) throws ClassNotFoundException {
            return YesClassLoader.class;
        }
    }

    /**
     * Generate the source code of a class which implements the {@link
     * org.objectweb.fractal.api.factory.Factory} interface. This class
     * instantiates components described by the specified ADL.
     * 
     * @param adl      the fully-qualified name of the membrane ADL descriptor
     * @param backend  the Fractal ADL backend
     * @param context  the Fractal ADL contextual information
     */
    private void generate( String adl, String backend, Map context )
    throws ADLException, IOException {
        
        int lastdot = adl.lastIndexOf('.');
        String packagename = adl.substring(0,lastdot);
        String classname = adl.substring(lastdot+1);
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        
        /*
         * Prologue.
         */
        pw.println("/*");
        pw.print  (" * Generated by "+getClass().getName()+" on: ");
        pw.println(new Date().toString());
        pw.println(" */");
        pw.println("package "+packagename+";");
        pw.println();

        /*
         * Class header.
         */
        pw.println("import "+COMPONENT+";");
        pw.println("import "+COMPONENT_TYPE+";");
        pw.println("import "+INTERFACE_TYPE+";");
        pw.println("import "+FRACTAL+";");
        pw.println("import "+NO_SUCH_INTERFACE_EXCEPTION+";");        
        pw.println();
        
        pw.println("/**");
        pw.println(" * Implementation of the "+classname+" membrane factory component.");
        pw.println(" *");
        pw.println(" * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>");
        pw.println(" * @since 2.5");
        pw.println(" */");
        pw.print  ("public class "+classname);
        pw.println(" implements "+FACTORY+", "+BINDING_CONTROLLER+" {");
        pw.println();
        
        /*
         * Empty implementation of the getFcContentDesc() method.
         */
        pw.println("  public Object getFcContentDesc() {");
        pw.println("    throw new java.lang.UnsupportedOperationException();");
        pw.println("  }");
        pw.println();

        /*
         * Empty implementation of the getFcControllerDesc() method.
         */
        pw.println("  public Object getFcControllerDesc() {");
        pw.println("    throw new java.lang.UnsupportedOperationException();");
        pw.println("  }");
        pw.println();
        
        /*
         * Implementation of the newFcInstance() factory method for creating
         * membrane instances.
         */
        pw.print  ("  public Component newFcInstance() ");
        pw.println("throws "+INSTANTIATION_EXCEPTION+" {");
        pw.println("    try {");
        pw.println("      // --------------------------------------------------");        
        
        context.put("printwriter",pw);        
        org.objectweb.fractal.adl.Factory f =
            FactoryFactory.getFactory(
                "org.objectweb.fractal.koch.mc.BasicFactory",
                backend, new HashMap());
        Object root = f.newComponent(packagename+"."+classname,context);
        
        pw.println("      // --------------------------------------------------");
        pw.println("      return "+root+";");
        pw.println("    }");
        pw.println("    catch( Exception e ) {");
        pw.println("      e.printStackTrace();");
        pw.println("      throw new "+INSTANTIATION_EXCEPTION+"(e.getMessage());");
        pw.println("    }");
        pw.println("  }");
        pw.println();
        
        /*
         * Implementation of the getFcInstanceType() factory method which
         * returns the type of the component created by newFcInstance().
         */
        pw.println("  public "+TYPE+" getFcInstanceType() {");
        pw.println("    try {");
        pw.println("      // --------------------------------------------------");        
        root = f.newComponentType(packagename+"."+classname,context);
        pw.println("      // --------------------------------------------------");
        pw.println("      return "+root+";");
        pw.println("    }");
        pw.println("    catch( Exception e ) {");
        pw.println("      e.printStackTrace();");
        pw.println("      throw new RuntimeException(e.getMessage());");
        pw.println("    }");
        pw.println("  }");
        pw.println();
        
        /*
         * Private fields.
         */
        pw.println("  private "+TYPE_FACTORY+" typeFactory;");
        pw.println("  private "+GENERIC_FACTORY+" genericFactory;");
        pw.println();

        /*
         * Implementation of the listFc() method.
         */
        pw.println("  public String[] listFc() {");
        pw.println("    return new String[]{\"type-factory\",\"generic-factory\"};");
        pw.println("  }");
        pw.println();

        /*
         * Implementation of the bindFc() method.
         */
        pw.println("  public void bindFc( String clientItfName, Object serverItf ) {");
        pw.println("    if( clientItfName.equals(\"type-factory\") ) {");
        pw.println("      typeFactory = ("+TYPE_FACTORY+") serverItf;");
        pw.println("    }");
        pw.println("    if( clientItfName.equals(\"generic-factory\") ) {");
        pw.println("      genericFactory = ("+GENERIC_FACTORY+") serverItf;");
        pw.println("    }");
        pw.println("  }");
        pw.println();

        /*
         * Implementation of the lookupFc() method.
         */
        pw.println("  public Object lookupFc( String clientItfName ) {");
        pw.println("    if( clientItfName.equals(\"type-factory\") ) {");
        pw.println("      return typeFactory;");
        pw.println("    }");
        pw.println("    if( clientItfName.equals(\"generic-factory\") ) {");
        pw.println("      return genericFactory;");
        pw.println("    }");
        pw.println("    return null;");
        pw.println("  }");
        pw.println();

        /*
         * Implementation of the unbindFc() method.
         */
        pw.println("  public void unbindFc( String clientItfName ) {");
        pw.println("    if( clientItfName.equals(\"type-factory\") ) {");
        pw.println("      typeFactory = null;");
        pw.println("    }");
        pw.println("    if( clientItfName.equals(\"generic-factory\") ) {");
        pw.println("      genericFactory = null;");
        pw.println("    }");
        pw.println("  }");

        pw.println("}");
        pw.close();
        String s = sw.toString();

        generateSourceCodeFile(adl,s);        
    }
    
    /**
     * Dump the specified Java source code in its corresponding file.
     */
    public void generateSourceCodeFile( String targetClassName, String code )
    throws IOException {
        
        String filename = targetClassName.replace('.',File.separatorChar)+".java";
        
        File f = new File(genDir,filename);
        File fdir = f.getParentFile();
        fdir.mkdirs();

        FileWriter fw = new FileWriter(f);
        fw.write(code);
        fw.close();
    }
    
    
    // ----------------------------------------------------------------------
    // Constants for fully-qualified name of types used by generated code
    // ----------------------------------------------------------------------

    final static private String FAPI = "org.objectweb.fractal.api.";

    final static private String COMPONENT = FAPI+"Component";
    final static private String NO_SUCH_INTERFACE_EXCEPTION = FAPI+"NoSuchInterfaceException";
    final static private String TYPE = FAPI+"Type";
    
    final static private String BINDING_CONTROLLER = FAPI+"control.BindingController";

    final static private String FACTORY = FAPI+"factory.Factory";
    final static private String GENERIC_FACTORY = FAPI+"factory.GenericFactory";
    final static private String INSTANTIATION_EXCEPTION = FAPI+"factory.InstantiationException";

    final static private String COMPONENT_TYPE = FAPI+"type.ComponentType";
    final static private String INTERFACE_TYPE = FAPI+"type.InterfaceType";
    final static private String TYPE_FACTORY = FAPI+"type.TypeFactory";
    
    final static private String FRACTAL = "org.objectweb.fractal.util.Fractal";
}
