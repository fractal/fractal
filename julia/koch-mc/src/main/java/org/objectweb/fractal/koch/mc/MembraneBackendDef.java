/***
 * Koch Membrane Compiler
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.mc;


/**
 * This class defines a constant which holds the name of the Fractal ADL backend
 * which is used for compiling (generating the source code of) control
 * components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneBackendDef {

    final static public String STATIC_BACKEND;
    static {
        String s = MembraneBackendDef.class.getName();
        STATIC_BACKEND = s.substring(0,s.length()-3);     // remove "Def"
    }
}
