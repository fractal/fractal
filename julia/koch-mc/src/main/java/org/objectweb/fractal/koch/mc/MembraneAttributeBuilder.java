/***
 * Koch Membrane Compiler
 * Copyright (C) 2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.mc;

import java.io.PrintWriter;
import java.util.Map;

import org.objectweb.fractal.adl.attributes.AttributeBuilder;

/**
 * Implementation of the attribute builder backend component which generates
 * attribute value definitions. This class replaces the default Fractal ADL
 * static Fractal attribute builder which uses Java reflection to invoke the
 * getter method of the attribute control interface. This invocation is used to
 * generate escape-friendly values for String attributes. As the interface
 * associated to the signature may not be available in the classpath, we do not
 * want Fractal ADL to return an exception when the getter methods is not found.
 * This class does not perform this check.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class MembraneAttributeBuilder implements AttributeBuilder {

  // --------------------------------------------------------------------------
  // Implementation of the AttributeBuilder interface
  // --------------------------------------------------------------------------
  
  public void setAttribute (
    final Object component, 
    final String attributeController, 
    final String name, 
    final String value,
    final Object context) throws Exception 
  {
    String v = value;
    String attrName = Character.toUpperCase(name.charAt(0)) + name.substring(1);
      StringBuffer buf = new StringBuffer();
      buf.append("\"");
      for (int i = 0; i < v.length(); ++i) {
        char car = v.charAt(i);
        if (car == '\n') {
          buf.append("\\n");
        } else if (car == '\\') {
          buf.append("\\\\");
        } else if (car == '"') {
          buf.append("\\\"");
        } else {
          buf.append(car);
        }
      }
      buf.append("\"");
      v = buf.toString();
    PrintWriter pw = (PrintWriter)((Map)context).get("printwriter");
    pw.print("((");
    pw.print(attributeController);
    pw.print(")Fractal.getAttributeController(");
    pw.print(component);
    pw.print(")).set");
    pw.print(attrName);
    pw.print("(");
    pw.print(v);
    pw.println(");");
  }
}
