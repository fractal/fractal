/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.conform;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.conform.components.D;
import org.objectweb.fractal.julia.conform.components.K;
import org.objectweb.fractal.util.Fractal;

/**
 * This class checks that component interfaces are proxies which implement
 * their corresponding interface type and which delegate to the content (for
 * server interfaces), and to the bound component (for client interfaces) if
 * there is a binding.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class TestProxyInterface extends Test {

  protected Component boot;
  protected TypeFactory tf;
  protected GenericFactory gf;

  protected ComponentType t, u;
  protected Component c, d, r;

  protected boolean isTemplate;

  // -------------------------------------------------------------------------
  // Constructor and setup
  // -------------------------------------------------------------------------

  public TestProxyInterface (final String name) {
    super(name);
  }

  protected void setUp () throws Exception {
    boot = Fractal.getBootstrapComponent();
    tf = Fractal.getTypeFactory(boot);
    gf = Fractal.getGenericFactory(boot);
    t = tf.createFcType(new InterfaceType[] {
      tf.createFcItfType("client", K.class.getName(), true, false, false),
      tf.createFcItfType("clients", K.class.getName(), true, false, true)
    });
    u = tf.createFcType(new InterfaceType[] {
        tf.createFcItfType("server", K.class.getName(), false, false, false),
        tf.createFcItfType("servers", K.class.getName(), false, false, true)
    });
    setUpComponents();
  }

  protected void setUpComponents () throws Exception {
    r = gf.newFcInstance(t, "composite", null);
    c = gf.newFcInstance(t, "primitive", D.class.getName());
    d = gf.newFcInstance(u, "primitive", D.class.getName());
    Fractal.getContentController(r).addFcSubComponent(c);
    Fractal.getContentController(r).addFcSubComponent(d);
    Fractal.getBindingController(c).bindFc("client", d.getFcInterface("server"));
    Fractal.getBindingController(c).bindFc("clients0", d.getFcInterface("server"));
    Fractal.getLifeCycleController(c).startFc();
    Fractal.getLifeCycleController(d).startFc();
  }
  
  public void testServerIsProxy() throws Exception {
      Object itf = d.getFcInterface("server");
      if( ! (itf instanceof K) ) {
          fail();
      }
  }
  
  public void testClientIsProxy() throws Exception {
      Object itf = c.getFcInterface("client");
      if( ! (itf instanceof K) ) {
          fail();
      }
  }
  
  public void testClientBindGet() throws Exception {
      K citf = (K)c.getFcInterface("client");
      citf.set(true);
      K sitf = (K)d.getFcInterface("server");
      assertTrue(sitf.get());
  }  

  public void testClientUnbindGet() throws Exception {
      try {
          Fractal.getLifeCycleController(c).stopFc();
      }
      catch( NoSuchInterfaceException nsie ) {
          // Subclasses deal with template components
          // Template components are not equiped with a lifecycle controller
      }
      Fractal.getBindingController(c).unbindFc("client");
      try {
          K itf = (K)c.getFcInterface("client");
          itf.set(true);
          fail("When unbound, client interfaces should reference null");
      }
      catch( NullPointerException npe ) {}
  }  

  public void testClientCollectionIsProxy() throws Exception {
      Object itf = c.getFcInterface("clients");
      if( ! (itf instanceof K) ) {
          fail();
      }
  }

  public void testServerCollectionIsProxy() throws Exception {
      Object itf = d.getFcInterface("servers");
      if( ! (itf instanceof K) ) {
          fail();
      }
  }

  public void testClientCollectionBindGet() throws Exception {
      K citf = (K)c.getFcInterface("clients0");
      citf.set(true);
      K sitf = (K)d.getFcInterface("server");
      assertTrue(sitf.get());
  }  

  public void testClientCollectionUnbindGet() throws Exception {
      try {
          Fractal.getLifeCycleController(c).stopFc();
      }
      catch( NoSuchInterfaceException nsie ) {
          // When run by subclasses which deal with template
          // components are not equiped with a lifecycle controller
      }
      Fractal.getBindingController(c).unbindFc("clients0");
      try {
          K itf = (K)c.getFcInterface("clients0");
          itf.set(true);
          fail("Client interface should references null");
      }
      catch( NullPointerException npe ) {}
  }  

  // ---

  public static class Interceptor extends TestProxyInterface {

    public Interceptor (String name) {
      super(name);
    }

    protected void setUpComponents () throws Exception {
        r = gf.newFcInstance(t, "composite", null);
        c = gf.newFcInstance(t, "statPrimitive", D.class.getName());
        d = gf.newFcInstance(u, "primitive", D.class.getName());
        Fractal.getContentController(r).addFcSubComponent(c);
        Fractal.getContentController(r).addFcSubComponent(d);
        Fractal.getBindingController(c).bindFc("client", d.getFcInterface("server"));
        Fractal.getBindingController(c).bindFc("clients0", d.getFcInterface("server"));
        Fractal.getLifeCycleController(c).startFc();
        Fractal.getLifeCycleController(d).startFc();
    }
  }
}
