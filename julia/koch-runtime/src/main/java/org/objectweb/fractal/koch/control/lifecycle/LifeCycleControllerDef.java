/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 * Contributors: Romain Rouvoy
 * -----------------------------------------------------------------------------
 * $Id:LifeCycleControllerDef.java 3124 2007-06-19 19:06:11Z seintur $
 * -----------------------------------------------------------------------------
 */

package org.objectweb.fractal.koch.control.lifecycle;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.julia.type.BasicInterfaceType;


/**
 * This interface contains data related to the lifecycle control interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public interface LifeCycleControllerDef {

    /** <code>NAME</code> of the lifecycle control interface. */
    final public static String NAME = "lifecycle-controller";
    
    /** <code>TYPE</code> of the lifecycle control interface. */
    final public static InterfaceType TYPE =
        new BasicInterfaceType(
                NAME,
                LifeCycleCoordinator.class.getName(),
                false, false, false );
}
