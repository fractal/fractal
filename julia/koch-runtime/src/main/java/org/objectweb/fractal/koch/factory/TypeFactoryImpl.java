/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;


/**
 * The implementation of the type factory.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class TypeFactoryImpl implements TypeFactory, BindingController {

    // -----------------------------------------------------------------
    // Implementation of the TypeFactory interface
    // -----------------------------------------------------------------
    
    /**
     * Create an interface type.
     */
    public InterfaceType createFcItfType(
            String name, String signature,
            boolean isClient, boolean isOptional, boolean isCollection)
    throws InstantiationException {
        
        checkCreateFcItfType(name,signature,isClient,isOptional,isCollection);
        
        return new BasicInterfaceType(
                name,signature,isClient,isOptional,isCollection);
    }

    /**
     * Create a component type.
     */
    public ComponentType createFcType(InterfaceType[] interfaceTypes)
    throws InstantiationException {
        
        checkCreateFcType(interfaceTypes);
        
        /*
         * Workaround for null component types.
         * AOKell assumes a component type is non null, whereas Julia envisions
         * situations where this can be the case. To preserve a kind of
         * compatibility, we bypass null component types with empty arrays of
         * interface types.
         */
        if( interfaceTypes == null ) {
            interfaceTypes = new InterfaceType[]{};
        }
        
        return new BasicComponentType(interfaceTypes);
    }
    
    
    /**
     * Perform some conformance tests on the types and values of
     * the given parameters for an interface type creation.
     */
    private void checkCreateFcItfType(
            String name, String signature,
            boolean isClient, boolean isOptional, boolean isCollection)
    throws InstantiationException {
        try {
            Class cl = loader.loadClass(signature,null);
            if( ! cl.isInterface() ) {
                String msg = signature+" is not a Java interface";
                throw new InstantiationException(msg);
            }
        }
        catch( ClassNotFoundException cnfe ) {
            String msg = "Unknown signature: "+signature+" for interface: "+name;
            throw new ChainedInstantiationException(cnfe,null,msg);
        }
    }

    /**
     * Perform some conformance tests on the types and values of
     * the given parameters for a component type creation.
     */
    private void checkCreateFcType(InterfaceType[] interfaceTypes)
    throws InstantiationException {
        
        if( interfaceTypes == null ) {
            return;
        }
        
        Set interfaceNames = new HashSet();
        for (int i = 0; i < interfaceTypes.length; i++) {
            InterfaceType itf = interfaceTypes[i];
            
            // ------------------------
            // Interface name
            // ------------------------
            String name = itf.getFcItfName();
            if( interfaceNames.contains(name) ) {
                throw new InstantiationException(
                        "Duplicate interface name: "+name);
            }
            interfaceNames.add(name);
            
            // ------------------------
            // Collection interfaces
            // ------------------------
            if( itf.isFcCollectionItf() ) {
                for (int j = 0; j < interfaceTypes.length; j++) {
                    if( j != i ) {
                        String otherName = interfaceTypes[j].getFcItfName();
                        if( otherName.startsWith(name) )
                            throw new InstantiationException(
                                    "Invalid interface name: "+otherName+
                                    ". A collection interface: "+name+
                                    " already exists.");
                    }
                }
            }
            
            // ------------------------
            // Interface signature
            // ------------------------
            String signature = itf.getFcItfSignature();
            boolean isClient = itf.isFcClientItf();
            boolean isOptional = itf.isFcOptionalItf();
            boolean isCollection = itf.isFcCollectionItf();
            checkCreateFcItfType(name, signature, isClient, isOptional, isCollection);
        }

    }

    
    // -----------------------------------------------------------------
    // Implementation of the BindingController interface
    // -----------------------------------------------------------------
    
    private Loader loader;
    
    public void bindFc(String clientItfName, Object serverItf) {
        if( clientItfName.equals("loader") ) {
            loader = (Loader) serverItf;
        }
    }

    public String[] listFc() {
        return new String[]{"loader"};
    }

    public Object lookupFc(String clientItfName) {
        if( clientItfName.equals("loader") ) {
            return loader;
        }
        return null;
    }

    public void unbindFc(String clientItfName) {
        if( clientItfName.equals("loader") ) {
            loader = null;
        }
    }
}
