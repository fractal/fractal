/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * This interface defines the attributes which describe the interceptors
 * associated to a control membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public interface InterceptorDefAttributes extends AttributeController {
    
    /**
     * @param interceptors
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    public void setInterceptors( String interceptors );
    
    /**
     * @return
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    public String getInterceptors();

}
