/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.factory;

import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.factory.Template;
import org.objectweb.fractal.julia.type.BasicInterfaceType;


/**
 * This interface contains data related to the factory control interface.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public interface FactoryDef {

    final public static String NAME = "factory";
    
    final public static InterfaceType TYPE =
        new BasicInterfaceType(
                NAME,
                Template.class.getName(),
                false, false, false);
    
    /**
     * The factory controller is specified with the {@link Factory} interface in
     * the Fractal API. Julia extends this interface with the {@link Template}
     * interface. A hidden interface name "/template" is associated to the
     * implementation of this interface.
     */
    final public static String HIDDEN_NAME = "/template";

    final public static InterfaceType HIDDEN_TYPE =
        new BasicInterfaceType(
                HIDDEN_NAME,
                Template.class.getName(),
                false, false, false);    
}