/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.util.Fractal;

/**
 * A factory for creating component-based membranes.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public class MembraneFactoryImpl implements MembraneFactory, BindingController {
    
    // ----------------------------------------------------------------------
    // Implementation of the MembraneFactory interface
    // ----------------------------------------------------------------------

    /**
     * Create a new control membrane.
     * 
     * @param controllerDesc  the controller description of the membrane
     * @return                the component implementing the control membrane  
     */
    public Component newFcMembrane( Object controllerDesc )
    throws InstantiationException {
        
        String itfname = "membranes-"+controllerDesc;
        
        if( ! membranes.containsKey(itfname) ) {
            
            /*
             * No registered component.
             * Try to fetch the ADL for this membrane from a .cfg file.
             */
            String adl = null;
            try {
              Tree t = null;
                if (((String)controllerDesc).startsWith("/koch/")){
                  String suffix = ((String)controllerDesc).substring(6);
                  t = loader.loadTree("/koch/"+suffix);
                }
                  else {
                    t = loader.loadTree("/koch/"+controllerDesc);
                  }
                adl = t.getSubTree(0).toString();  // may throw ArrayOutOfBoundException
            }
            catch (Exception e) {
                String msg = e.getClass()+": "+e.getMessage();
                throw new ChainedInstantiationException(e,null,msg);
            }            
            registerFcNewMembraneDef((String)controllerDesc, adl);
        }
        
        Factory factory = (Factory) membranes.get(itfname);
        Component membrane = factory.newFcInstance();
        return membrane;
    }
    
    /**
     * Register a new membrane definition.
     * 
     * @param controllerDesc  the controller description of the membrane
     * @param adl             the fully-qualified name of the ADL describing this membrane 
     */
    public void registerFcNewMembraneDef( String controllerDesc, String adl )
    throws InstantiationException {
        
        Type type =
            typeFactory.createFcType(
                new InterfaceType[]{
                    typeFactory.createFcItfType("factory", Factory.class.getName(), false, false, false),
                    typeFactory.createFcItfType("generic-factory", GenericFactory.class.getName(), true, false, false),
                    typeFactory.createFcItfType("type-factory", TypeFactory.class.getName(), true, false, false)
                }
            );
        Component c = genericFactory.newFcInstance(type, "mPrimitive", adl);
        
        /*
         * Insert the component as a subcomponent of the boostrap component
         * (which is a composite) and bind it with other components.
         */
        try {
            SuperController sc = Fractal.getSuperController(component);
            Component boot = sc.getFcSuperComponents()[0];
            ContentController cc = Fractal.getContentController(boot);
            cc.addFcSubComponent(c);
            
            BindingController bc = Fractal.getBindingController(c);
            bc.bindFc("type-factory", typeFactory);
            bc.bindFc("generic-factory", genericFactory);
            
            bc = Fractal.getBindingController(component);
            String name = "membranes-"+controllerDesc;
            Object o = c.getFcInterface("factory");
            bc.bindFc(name, o);
            
            Fractal.getLifeCycleController(c).startFc();
        }
        catch (NoSuchInterfaceException e) {
            String msg = "NoSuchInterfaceException: "+e.getMessage();
            throw new ChainedInstantiationException(e,null,msg);
        }
        catch (IllegalContentException e) {
            String msg = "IllegalContentException: "+e.getMessage();
            throw new ChainedInstantiationException(e,null,msg);
        }
        catch (IllegalLifeCycleException e) {
            String msg = "IllegalLifeCycleException: "+e.getMessage();
            throw new ChainedInstantiationException(e,null,msg);
        }
        catch (IllegalBindingException e) {
            String msg = "IllegalBindingException: "+e.getMessage();
            throw new ChainedInstantiationException(e,null,msg);
        }
    }

    /**
     * Return the fully-qualified name of the factory component for the
     * specified controller descriptor. In other terms, return the name of the
     * ADL descriptor for the specified membrane. Return null if no such
     * membrane is registered.
     * 
     * @param controllerDesc  the controller descriptor of the membrane
     * @return                the ADL for the specified membrane or null
     */
    public String getFcMembraneADL( Object controllerDesc ) {
        
        String itfname = "membranes-"+controllerDesc;
        
        if( ! membranes.containsKey(itfname) ) {
            return null;
        }
        
        Interface factory = (Interface) membranes.get(itfname);
        Component membrane = factory.getFcItfOwner();
        try {
            Object content = membrane.getFcInterface("/content");
            String adl = content.getClass().getName();
            return adl;
        }
        catch( NoSuchInterfaceException nsie ) {
            return null;
        }
    }
    
    // ----------------------------------------------------------------------
    // Implementation of the BindingController interface
    // ----------------------------------------------------------------------

    private Component component;
    private GenericFactory genericFactory;
    private TypeFactory typeFactory;
    private Loader loader;
    private Map membranes = new HashMap();
    
    public String[] listFc() {
        String[] result = new String[membranes.size() + 3];
        membranes.keySet().toArray(result);
        result[membranes.size()] = "generic-factory";
        result[membranes.size()+1] = "type-factory";
        result[membranes.size()+2] = "loader";
        return result;
    }

    public Object lookupFc( String s ) {
        if (s.equals("generic-factory")) {
          return genericFactory;
        } else if (s.equals("type-factory")) {
          return typeFactory;
        } else if (s.equals("loader")) {
          return loader;
        } else if (s.startsWith("membranes")) {
          return membranes.get(s);
        }
        return null;
    }

    public void bindFc( String s, Object o ) {
        if (s.equals("component")) {
          component = (Component)o;
        } else if (s.equals("generic-factory")) {
          genericFactory = (GenericFactory)o;
        } else if (s.equals("type-factory")) {
          typeFactory = (TypeFactory)o;
        } else if (s.equals("loader")) {
          loader = (Loader)o;
        } else if (s.startsWith("membranes")) {
          membranes.put(s, o);
        }
    }

    public void unbindFc( String s ) {
        if (s.equals("generic-factory")) {
          genericFactory = null;
        } else if (s.equals("type-factory")) {
          typeFactory = null;
        } else if (s.equals("loader")) {
          loader = null;
        } else if (s.startsWith("membranes")) {
          membranes.remove(s);
        }
    }
}
