/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;

/**
 * A sub class of the {@link IllegalInterceptorException} class.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public class ChainedIllegalInterceptorException extends IllegalInterceptorException {

    private static final long serialVersionUID = 8613243199190332673L;

  /**
   * The exception that caused this exception. May be <tt>null</tt>.
   */
  private final Throwable exception;

  /**
   * The component interface which is associated with the interceptor.
   */
  private transient ComponentInterface itf;

  /**
   * The interceptor.
   */
  private transient Interceptor interceptor;

  /**
   * Constructs a new {@link ChainedIllegalInterceptorException} exception.
   *
   * @param exception the cause of this exception. May be <tt>null</tt>.
   * @param itf the component interface.
   * @param interceptor the interceptor.
   * @param message a detailed error message.
   */

  public ChainedIllegalInterceptorException (
    final Throwable exception,
    final ComponentInterface itf,
    final Interceptor interceptor,
    final String message)
  {
    super(message);
    this.exception = exception;
    this.itf = itf;
    this.interceptor = interceptor;
  }

  /**
   * Returns the exception that caused in this exception.
   *
   * @return the exception that caused this exception. May be <tt>null</tt>.
   */
  public Throwable getException () {
    return exception;
  }

  public ComponentInterface getComponentInterface() {
    return itf;
  }

  public Interceptor getInterceptor() {
    return interceptor;
  }

  // -------------------------------------------------------------------------
  // Overriden Exception methods
  // -------------------------------------------------------------------------

  /**
   * Returns a String representation of this exception.
   *
   * @return a String representation of this exception.
   */
  public String toString () {
    StringBuffer buf = new StringBuffer();
    buf.append("IllegalInterceptorException: ");
    buf.append(getMessage());
    buf.append(" (component interface = ");
    buf.append(itf.toString());
    buf.append(", interceptor = ");
    buf.append(interceptor.toString());
    buf.append(')');
    return buf.toString();
  }

  /**
   * Prints the stack stacktrace.
   */
  public void printStackTrace () {
    if (exception != null) {
      System.err.println(this);
      exception.printStackTrace();
    } else {
      super.printStackTrace();
    }
  }

  /**
   * Prints this exception and its stacktrace to the specified print stream.
   *
   * @param s <tt>PrintStream</tt> to use for output.
   */

  public void printStackTrace (final PrintStream s) {
    if (exception != null) {
      s.println(this);
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }

  /**
   * Prints this exception and its stacktrace to the specified print writer.
   *
   * @param s <tt>PrintWriter</tt> to use for output.
   */
  public void printStackTrace (final PrintWriter s) {
    if (exception != null) {
      s.write(this + "\n");
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }
  
  private void writeObject (final ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    ComponentInterface itf = getComponentInterface();
    out.writeObject(itf instanceof ComponentInterface ? itf : null);
    Interceptor interceptor = getInterceptor();
    out.writeObject(interceptor instanceof Interceptor ? interceptor : null);
  }

  private void readObject (final ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject();
    itf = (ComponentInterface)in.readObject();
    interceptor = (Interceptor)in.readObject();
  }
}
