/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.koch.control.binding.BindingControllerDef;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;
import org.objectweb.fractal.koch.control.content.ContentControllerDef;
import org.objectweb.fractal.koch.control.content.SuperControllerDef;
import org.objectweb.fractal.koch.control.interceptor.IllegalInterceptorException;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.control.interceptor.InterceptorDefAttributes;
import org.objectweb.fractal.koch.control.lifecycle.LifeCycleControllerDef;
import org.objectweb.fractal.koch.control.name.NameControllerDef;
import org.objectweb.fractal.koch.loader.TreeParser;
import org.objectweb.fractal.koch.loader.TreeParserException;
import org.objectweb.fractal.util.ContentControllerHelper;

/**
 * A generic factory which instanciates components with a component-based
 * membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class BasicGenericFactoryImpl implements GenericFactory, BindingController {
    
    // -----------------------------------------------------------------
    // Implementation of the GenericFactory interface
    // -----------------------------------------------------------------
    
    /**
     * Create a new component.
     * 
     * @param type  the component type
     * @param controllerDesc
     *          the description of the control membrane associated
     *          to the component
     * @param contentDesc
     *          the description of the content of the component
     */
    public Component newFcInstance(
        Type type, Object controllerDesc, Object contentDesc )
    throws InstantiationException {
        
        if( controllerDesc instanceof Component ) {
            Component membrane = (Component) controllerDesc;
            return newFcInstance(type, controllerDesc, membrane, contentDesc);
        }
        
        /*
         * When called from Fractal ADL, the component creation process may be
         * accompagnied with some hints. One of the them is the class loader to
         * be used. In this case, the controllerDesc parameter is an array where
         * the 1st element contains the class loader.
         */
        if( controllerDesc.getClass().isArray() ) {
            Object[] array = (Object[]) controllerDesc;
            if( tlclassloader == null ) {
                tlclassloader = new ThreadLocal();
            }
            tlclassloader.set(array[0]);
            controllerDesc = array[1];
        }
        
        checkFcType(type,controllerDesc);
        checkFcControllerDesc(controllerDesc);
        checkFcContentDesc(type,controllerDesc,contentDesc);
                
        /*
         * Build the contextual information used by the Julia loader to create
         * the content of attribute control components. Store this information
         * in a ThreadLocal variable which will be read latter on by
         * BasicGenericFactory#loadFcContent.
         */
        Map context = (Map) threadlocal.get();
        if( context == null ) {
            context = new HashMap();
            threadlocal.set(context);
        }
        try {
            ComponentType ct = (ComponentType) type;
            InterfaceType it = ct.getFcInterfaceType("attribute-controller");
            String signature = it.getFcItfSignature();
            Tree tree = new Tree(signature);
            context.put("attributeControllerInterface", tree);
        }
        catch (NoSuchInterfaceException e) {
            /*
             * Empty contextual information if there is no attribute-controller
             * interface.
             */
        }

        /*
         * Create the content.
         */
        Object content = newFcContent(type,controllerDesc,contentDesc);
        
        /*
         * Create the membrane and bind it to the content.
         */
        Component compctrlitf =
            newFcMembrane(type,controllerDesc,contentDesc,content);
        
        /* Erase any class loader which may have been stored. */
        if( tlclassloader != null ) {
            tlclassloader.set(null);
        }
        
        return compctrlitf;
    }
    
    /*
     * ThreadLocal instance which hold contextual information used by the Julia
     * loader to create the content of attribute control components.
     */
    protected static ThreadLocal threadlocal = new ThreadLocal();

    
    // -----------------------------------------------------------------
    // Membrane related methods
    // -----------------------------------------------------------------
        
    /**
     * Create a new control membrane.
     * 
     * @param type  the type of the component associated to this membrane
     * @param controllerDesc
     *          the controller description of this membrane
     *          (typically a String such as "primitive" or any of the other
     *          registered membrane identifier)
     * @param contentDesc 
     *          the content description of the component associated
     *          to this membrane (e.g. the fully qualified name of the class
     *          implementing a primitive component)
     * @param content
     *          the instance implementing the component associated
     *          to this membrane
     *          
     * @return  the fcinterface for the component interface
     *          exported by the given control membrane  
     */
    protected Component newFcMembrane(
            Type type, Object controllerDesc, Object contentDesc,
            Object content )
    throws InstantiationException {
        
        /*
         * Control membrane for controllers, i.e. components associated to the
         * mPrimitive and mComposite controller descriptions.
         */
        if( controllerDesc.equals("mPrimitive") ) {
            Component mprim = newFcMPrimitive(type, content);
            return mprim;
        }
        if( controllerDesc.equals("mComposite") ) {
            Component mcomp = newFcMComposite(type, content);
            return mcomp;
        }
        
        /*
         * Create the membrane.
         */
        Component membrane = membraneFactory.newFcMembrane(controllerDesc);
        
        /*
         * Bind the membrane to the content.
         */
        Component compctrlitf =
            glueFcMembrane(type,controllerDesc,contentDesc,content,membrane);

        return compctrlitf;
    }
    
    /**
     * Glue a control membrane to the component defined by the given type,
     * content description and content.
     * 
     * @param type  the type of the component associated to this membrane
     * @param controllerDesc
     *          the controller description of this membrane
     *          (typically a String such as "primitive" or any of the other
     *          registered membrane identifier)
     * @param contentDesc 
     *          the content description of the component associated
     *          to this membrane
     * @param content   the content of the component associated to this membrane
     * @param membrane  the control membrane
     * 
     * @return  the fcinterface for the component interface
     *          exported by the given control membrane  
     */
    protected Component glueFcMembrane(
            Type type, Object controllerDesc,
            Object contentDesc, Object content, Component membrane )
    throws InstantiationException {
        
        /*
         * Check that the membrane fits all the requirements.
         */
        checkFcMembrane(membrane);
        
        /*
         * Create the initialisation context.
         */
        InitializationContext ic = new InitializationContext();
        ic.controllers = new ArrayList();
        ic.interfaces = new HashMap();
        ic.internalInterfaces = new HashMap();
        
        ic.interfaces.put("/content",content);
        ic.interfaces.put("/controllerDesc",controllerDesc);
        
        /*
         * In the case of template components, Julia factory controller expects
         * ic.content to reference contentDesc (which is an array describing
         * the component associated to the template). contentDesc may be null
         * for composite components.
         */
        if( contentDesc == null ) {
            ic.content = content;
        }
        else {
            ic.content =
                (contentDesc.getClass().isArray()) ? contentDesc : content;
        }

        /*
         * Retrieve all controllers.
         */
        Map ctrls = new HashMap();
        List l = ContentControllerHelper.getAllSubComponents(membrane);
        for (Iterator iter = l.iterator(); iter.hasNext();) {
            Component comp = (Component) iter.next();

            // some component in the membrane may be non control component
            // without a content (this is the case of the bootstrap membrane
            // which includes the Julia bootstrap component
            try {
                Object ctrl = comp.getFcInterface("/content");
                // composite control components may have a null content
                if( ctrl != null ) {
                    ic.controllers.add(ctrl);
                    ctrls.put(ctrl,comp);
                }
            }
            catch (NoSuchInterfaceException e) {
            }
        }
        
        /*
         * Initialize the interface and the interface type of the component
         * controller.
         */
        Interface compctrlitf = (Interface) getFcInterface(membrane,"//component");
        Component compctrl = (Component) compctrlitf;
        InterfaceType srcit = (InterfaceType) compctrlitf.getFcItfType();
        InterfaceType it = downToLevel0InterfaceType(srcit);
        InterfaceInstanceGenerator itfgen = new InterfaceInstanceGenerator(loader);
        Interface proxy = itfgen.generate(it,compctrl,compctrl,false);
        Component proxyForCompctrl = (Component) proxy;
        ic.interfaces.put( "component", proxyForCompctrl );
        
        List fullITs = new ArrayList();
        fullITs.add(it);
        
        /*
         * Proceed by initializing the interface and the interface type of the
         * other controllers.
         */
        Object[] itfs = membrane.getFcInterfaces();
        Object cloneableAttrCtrlImpl = null;
        for (int i = 0; i < itfs.length; i++) {
            Interface itf = (Interface) itfs[i];
            String name = itf.getFcItfName();
            
            if( name.startsWith("//") && !name.equals("//component") ) {
                
                srcit = (InterfaceType) itf.getFcItfType();
                it = downToLevel0InterfaceType(srcit);
                
                // Control (external) interface
                proxy = itfgen.generate(it,proxyForCompctrl,itf,false);
                name = name.substring(2);    // strip //
                ic.interfaces.put( name, proxy );

                // Control (internal) interface
                if( name.equals("factory") ) {
                    InterfaceType intit = newSymetricInterfaceType(it);
                    Interface internal = itfgen.generate(intit,proxyForCompctrl,itf,true);
                    ic.internalInterfaces.put(name,internal);
                }
                
                // Cloneable attribute controller implementation
                if( name.equals("/cloneable-attribute-controller") ) {
                    cloneableAttrCtrlImpl = getFcCtrlCompImpl(itf);
                }

                // Component type
                if( name.charAt(0) != '/' ) {
                    fullITs.add(it);
                }
            }
        }
        
        /*
         * Build the component type by appending the business type to the
         * already computed control type.
         */
        ComponentType comptype = (ComponentType) type;
        InterfaceType[] its = comptype.getFcInterfaceTypes();
        fullITs.addAll( Arrays.asList(its) );
        
        InterfaceType[] full = new InterfaceType[fullITs.size()];
        ic.type = new BasicComponentType( (InterfaceType[]) fullITs.toArray(full) );

        /*
         * Build business component interfacess.
         */
        for (int i = 0; i < its.length; i++) {
            
            String name = its[i].getFcItfName();
            
            if( its[i].isFcCollectionItf() ) {
                // Julia naming convention for collection interfaces
                name = "/collection/"+name;
            }
            
            /*
             * If an implementation of a cloneable attribute controller is
             * available (this is the case for parametric primitive and
             * composite templates), let the attribute controller interface
             * delegate to this instance instead of the content.
             * 
             * The idea is that the state of the component (as defined by the
             * attributes) must be cloned when a new instance of the template is
             * created. Hence, each setting of a attribute must be directed
             * towards the cloneable attribute controller to be let it be cloned
             * latter on.
             */
            Object delegate =
                name.equals("attribute-controller") && cloneableAttrCtrlImpl!=null ?
                cloneableAttrCtrlImpl :
                content;
            
            // External interface            
            Interface ext = itfgen.generate(its[i],proxyForCompctrl,delegate,false);
            ic.interfaces.put(name,ext);
            if( its[i].isFcCollectionItf() ) {
                /*
                 * Add a 2nd reference for this interface.
                 * It will be possible to retrieve this reference to dynamically
                 * manage (add and remove) the interceptors associated to a
                 * collection interface. This is not possible with
                 * "/collection/"+name which is never returned as this by
                 * getFcInterface (a clone is always returned).
                 */
                ic.interfaces.put("/koch"+name,ext);
            }
            
            // Internal interface
            if( ! name.equals("attribute-controller") ) {
                InterfaceType intit = newSymetricInterfaceType(its[i]);
                Interface internal =
                    itfgen.generate(intit,proxyForCompctrl,content,true);
                ic.internalInterfaces.put(name,internal);
                if( its[i].isFcCollectionItf() ) {
                    ic.internalInterfaces.put("/koch"+name,internal);
                }
            }
        }

        /*
         * Initialize controllers.
         * 
         * Iterate on ic.controllers which is a List, to initialize the
         * component controller first, and the ContentController second.
         * When initialized, controllers (e.g. the interceptor controller) may
         * assume that the component controller has already been initialized to
         * retrieve for example, the component type or the array of internal
         * fcinterfaces.
         */
        // First the Component control-component
        for (Iterator iter = ic.controllers.iterator(); iter.hasNext();) {
            Object o = iter.next();
            if( o instanceof Component ) {
                Controller ctrl = (Controller) o;
                ctrl.initFcController(ic);
            }
        }
        
        // Second the Content control-component
        for (Iterator iter = ic.controllers.iterator(); iter.hasNext();) {
            Object o = iter.next();
            if( o instanceof ContentController ) {
                Controller ctrl = (Controller) o;
                Component ctrlcomp = (Component) ctrls.get(ctrl);
                InitializationContext localic = new InitializationContext();
                localic.content = ic.content;
                localic.type = ic.type;
                localic.interfaces = new ControlComponentBoundMap(ctrlcomp);
                localic.internalInterfaces = ic.internalInterfaces;
                ctrl.initFcController(localic);
            }
        }
        
        // Remaining control-components
        for (Iterator iter = ic.controllers.iterator(); iter.hasNext();) {
            Object o = iter.next();

            if( o instanceof Component || o instanceof ContentController ) {
                continue;
            }

            if( !(o instanceof Controller) ) {
                /*
                 * This is not mandatory for controllers to implement
                 * {@link Controller}. For example, simple controllers which do
                 * not require any information from the initialization context
                 * may avoid implementing {@link Controller}.
                 */
                continue;
            }
            
            Controller ctrl = (Controller) o;
            Component ctrlcomp = (Component) ctrls.get(ctrl);

            InitializationContext localic = new InitializationContext();
            localic.content = ic.content;
            localic.type = ic.type;
            
            /*
             * It seems that Julia controllers do not use ic.controllers.
             * This list seems to be only used by the generic factory.
             */
            localic.controllers = null;
            localic.interfaces = new ControlComponentBoundMap(ctrlcomp);
            localic.internalInterfaces = null;                    
            
            ctrl.initFcController(localic);
        }
        
        /*
         * Initialize interceptors.
         */
        InterceptorController interceptctrl = null;
        for (Iterator iter = ic.controllers.iterator(); iter.hasNext();) {
            Object o = iter.next();
            if( o instanceof InterceptorController ) {
                interceptctrl = (InterceptorController) o;
            }
        }
        if( interceptctrl != null ) {
            
            // This membrane defines an interceptor controller
            Object o = getFcCtrlCompImpl(interceptctrl);
            
            // Check that the interceptor controller provides the attribute
            // describing the interceptor generator
            if( !(o instanceof InterceptorDefAttributes) ) {
                String msg =
                    "The interceptor controller for the membrane of component "+
                    contentDesc+" does not implement the InterceptorDefAttributes interface";
                throw new InstantiationException(msg);
            }
            InterceptorDefAttributes ida = (InterceptorDefAttributes) o;
            
            // Retrieve an instance of the interceptor instance generator
            String interceptDesc = ida.getInterceptors();
            ClassGenerator interceptcg = null;
            try {
                interceptcg = new InterceptorClassGenerator(loader,interceptDesc);
            }
            catch (TreeParserException e) {
                final String msg =
                    "Bad interceptor descriptor: "+e.getMessage()+"\n"+interceptDesc;
                throw new InstantiationException(msg);
            }
            InstanceGenerator interceptig = new InstanceGenerator(interceptcg);
            
            // Prepare a context for initializing interceptors.
            // Julia generated interceptors assume that ic.interfaces directly
            // references the controller implementations.
            InitializationContext intic = new InitializationContext();
            intic.interfaces = new HashMap();
            for (int i = 0; i < itfs.length; i++) {
                Interface itf = (Interface) itfs[i];
                String name = itf.getFcItfName();                
                if( name.startsWith("//") && !name.equals("//component") ) {
                    Object end = getFcCtrlCompImpl(itf);
                    intic.interfaces.put(name.substring(2),end);
                }
            }
            
            ContentController cc = (ContentController)
                ic.interfaces.get(ContentControllerDef.NAME);

            // Add an interceptor for each business interface
            for (int i = 0; i < its.length; i++) {
                String itfName = its[i].getFcItfName();
                if( its[i].isFcCollectionItf() ) {
                    itfName = "/koch/collection/" + itfName;
                }
                
                // Skip control interfaces
                if( isFcControllerInterfaceName(itfName) ) {
                    continue;
                }
                
                // Skip interfaces which do not match the interceptor
                if( ! interceptcg.match(its[i]) ) {
                    continue;
                }
                
                Object itf = ic.interfaces.get(itfName);
                ComponentInterface citf = (ComponentInterface) itf;
                try {
                    addFcInterceptor(interceptctrl, interceptig, citf, intic, cc);
                }
                catch (IllegalLifeCycleException e) {
                    throw new ChainedInstantiationException(e,null,"");
                }
                catch (IllegalInterceptorException e) {
                    throw new ChainedInstantiationException(e,null,"");
                }
            }
        }
        
        return proxyForCompctrl;
    }

    
    // -----------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------
        
    /**
     * Create a new component.
     * 
     * @param type         the component type
     * @param controllerDesc
     *          the controller description of this membrane
     *          (typically a String such as "primitive" or any of the other
     *          registered membrane identifier)
     * @param membrane     the control membrane
     * @param contentDesc  the description of the content of the component
     */
    private Component newFcInstance(
        Type type, Object controllerDesc, Component membrane, Object contentDesc )
    throws InstantiationException {
        
        /*
         * Instantiate the content.
         * contentDesc may be null for composite components.
         */
        Object content = null;
        if( contentDesc != null ) {
            if( !(contentDesc instanceof String) ) {
                final String msg = "contentDesc should be a String";
                throw new InstantiationException(msg);
            }
            String contentDescStr = (String) contentDesc;
            Class cl = null;
            try {
                cl = loadClass(contentDescStr);
            }
            catch (ClassNotFoundException cnfe) {
                throw new ChainedInstantiationException(cnfe,null,"");
            }
            checkFcContentClassImplementsServerInterfaces(type,cl);
            content = instanciateClass(cl);
        }
        
        /*
         * Bind the membrane to the content.
         */
        Component compctrlitf =
            glueFcMembrane(type,controllerDesc,contentDesc,content,membrane);

        return compctrlitf;
    }
        
    
    /**
     * Instantiate the content part of a component.
     */
    protected Object newFcContent(
            Type type, Object controllerDesc, Object contentDesc)
    throws InstantiationException {
        
        /*
         * Adaptation for templates components.
         * Templates are created with something like:
         * Component cTmpl = cf.newFcInstance(
         *      cType, "flatPrimitiveTemplate",
         *      new Object[] { "flatPrimitive", "ClientImpl" });
         */
        if( controllerDesc instanceof String ) {
            if( ((String)controllerDesc).endsWith("Template") ) {
                Object[] cont = (Object[]) contentDesc;
                contentDesc = cont[1];
            }
        }
        
        if( contentDesc == null ) {
            /*
             * contentDesc is null for composite components (this is not
             * mandatory, a composite can have a content, but this is the case
             * most of the time).
             */
            return null;
        }
        
        if( ! (contentDesc instanceof String) ) {
            /*
             * When contentDesc is not a String, Julia assumes that this is not
             * a content descriptor, but that this is the content.
             */
            return contentDesc;
        }
        
        /*
         * Load the content class.
         */
        String contentDescStr = (String) contentDesc;        
        Class contentClass = loadFcContentClass(type,controllerDesc,contentDescStr);
        
        /*
         * Perform checkings on the content class.
         */
        checkFcContentClassImplementsServerInterfaces(type,contentClass);
        if( isFcContentToBeCheckedForBC(controllerDesc) ) {
            checkFcContentClassforBC(type,contentClass);
        }

        /*
         * Instantiate the content class.
         */
        Object content = null;
        try {
            content = contentClass.newInstance();
        }
        catch(Exception e) {
            final String msg =
                e.getClass().getName()+" when instantiating content class "+
                contentDescStr+". Message: "+e.getMessage();
            throw new ChainedInstantiationException(e,null,msg);
        }
        
        return content;
    }
    
    /**
     * Load the specified content class.
     * 
     * First, try to load the class from the classpath.
     * If that fails, assume that the specified contentDesc corresponds to a
     * configuration string such as the ones which can be found in the julia.cfg
     * configuration file. This is case for control component where the content
     * class is dynamically generated with the ASM mixin class generator.
     * 
     * @param type            the component type
     * @param controllerDesc  the controller descriptor
     * @param contentDescStr  the content descriptor
     */
    protected Class loadFcContentClass(
        Type type, Object controllerDesc, String contentDescStr )
    throws InstantiationException {

        char first = contentDescStr.charAt(0);
        if( first=='(' || first=='\'' ) {

            /*
             * This is a descriptor defined in a julia.cfg configuration file.
             * Get the contextual information from the ThreadLocal variable.
             * (Generate and) load the class.
             */
            Map context = (Map) threadlocal.get();
            try {
                TreeParser tp = new TreeParser(contentDescStr);
                Tree src = tp.parseTree();
                Tree eval = loader.evalTree(src, context);
                eval = eval.getSubTree(0);
                Class cl = loader.loadClass(eval, null);
                return cl;
            }
            catch (Exception e) {
                final String msg = "Content class "+contentDescStr+" not found";
                throw new ChainedInstantiationException(e,null,msg);
            }
        }
        else {
            /*
             * This is the fully-qualified name of a class.
             * Load the class.
             */
            try {
                Class contentClass = loadClass(contentDescStr);
                return contentClass;
            }
            catch( ClassNotFoundException cnfe ) {
                final String msg = "Content class "+contentDescStr+" not found";
                throw new ChainedInstantiationException(cnfe,null,msg);
            }
        }
    }
    
    /**
     * Check that the given control membrane fits all the requirements.
     */
    private void checkFcMembrane( Component membrane ) throws InstantiationException {
        
        try {
            membrane.getFcInterface("//component");
        }
        catch (NoSuchInterfaceException nsie) {
            final String msg =
                "The given membrane should provide a "+nsie.getMessage()+
                " interface";
            throw new ChainedInstantiationException(nsie,null,msg);
        }
    }
    
    /**
     * Given the interface type of a control membrane, return the corresponding
     * type for the application (level 0) interface. This consists in removing
     * the 2 slash characters which are in front of the interface name.
     */
    private static InterfaceType downToLevel0InterfaceType( InterfaceType srcit ) {
        InterfaceType it =
            new BasicInterfaceType(
                srcit.getFcItfName().substring(2),     // remove //
                srcit.getFcItfSignature(),
                srcit.isFcClientItf(),
                srcit.isFcOptionalItf(),
                srcit.isFcCollectionItf()
            );
        return it;
    }

    /**
     * The interceptor descriptor used to generate lifecycle interceptors for
     * mPrimitive components.
     */
    final private static String interceptorDesc =
        "(org.objectweb.fractal.julia.asm.InterceptorClassGenerator org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator)";
    
    /**
     * Create a new instance of a mPrimitive component.
     */
    private Component newFcMPrimitive( Type type, Object content )
    throws InstantiationException {

        MPrimitiveImpl mprim = new MPrimitiveImpl(type,content);

        /*
         * Create a context for initializing interceptors.
         */
        InitializationContext ic = new InitializationContext();
        ic.interfaces = new HashMap();
        ic.interfaces.put(ComponentControllerDef.NAME,mprim);
        ic.interfaces.put(LifeCycleControllerDef.NAME,mprim);
        // TODO other interfaces
        
        /*
         * Retrieve the interface and the interceptor generators.
         */
        InterfaceInstanceGenerator itfgen = new InterfaceInstanceGenerator(loader);
        ClassGenerator interceptcg = null;
        try {
            interceptcg = new InterceptorClassGenerator(loader,interceptorDesc);
        }
        catch (TreeParserException tpe) {
            throw new ChainedInstantiationException(tpe,null,"");
        }
        InstanceGenerator interceptig = new InstanceGenerator(interceptcg);
        
        /*
         * Create interfaces and interceptors.
         */
        Map fcInterfaces = new HashMap();
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String name = its[i].getFcItfName();
            if( its[i].isFcCollectionItf() ) {
                // Julia naming convention for collection fcinterfaces
                name = "/collection/"+name;
            }
            Object delegate = content;
            if( ! its[i].isFcClientItf() && ! name.equals("attribute-controller") ) {
                // Add lifecycle interceptors to server interfaces
                Interceptor interceptor = (Interceptor) interceptig.generate(its[i],ic);
                interceptor.initFcController(ic);
                interceptor.setFcItfDelegate(content);
                delegate = interceptor;
            }
            Object itf = itfgen.generate(its[i],mprim,delegate,false);
            fcInterfaces.put(name,itf);
        }
        
        fcInterfaces.put(
            ComponentControllerDef.NAME,
            itfgen.generate(ComponentControllerDef.TYPE,mprim,mprim,false) );
        fcInterfaces.put(
            BindingControllerDef.NAME,
            itfgen.generate(BindingControllerDef.TYPE,mprim,mprim,false) );
        fcInterfaces.put(
            NameControllerDef.NAME,
            itfgen.generate(NameControllerDef.TYPE,mprim,mprim,false) );
        fcInterfaces.put(
            SuperControllerDef.NAME,
            itfgen.generate(SuperControllerDef.TYPE,mprim,mprim,false) );
        fcInterfaces.put(
            LifeCycleControllerDef.NAME,
            itfgen.generate(LifeCycleControllerDef.TYPE,mprim,mprim,false) );
        
        mprim.setFcInterfaces(fcInterfaces);
        return mprim;
    }
    
    /**
     * Create a new instance of a mComposite component.
     */
    private Component newFcMComposite( Type type, Object content )
    throws InstantiationException {

        MCompositeImpl mcomp = new MCompositeImpl(type,content);
        
        /*
         * Create a context for initializing interceptors.
         */
        InitializationContext ic = new InitializationContext();
        ic.interfaces = new HashMap();
        ic.interfaces.put(ComponentControllerDef.NAME,mcomp);
        // TODO other interfaces

        /*
         * Retrieve the interface generator.
         */
        InterfaceInstanceGenerator itfgen = new InterfaceInstanceGenerator(loader);

        /*
         * Create interfaces.
         */
        Map fcInterfaces = new HashMap();
        Map fcInternalInterfaces = new HashMap();
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String name = its[i].getFcItfName();
            if( its[i].isFcCollectionItf() ) {
                // Julia naming convention for collection fcinterfaces
                name = "/collection/"+name;
            }
            Object itf = itfgen.generate(its[i],mcomp,content,false);
            fcInterfaces.put(name,itf);
            
            InterfaceType intit = newSymetricInterfaceType(its[i]);
            Object intitf = itfgen.generate(intit,mcomp,content,false);
            fcInternalInterfaces.put(name,intitf);
        }
        
        fcInterfaces.put(
            ComponentControllerDef.NAME,
            itfgen.generate(ComponentControllerDef.TYPE,mcomp,mcomp,false) );
        fcInterfaces.put(
            BindingControllerDef.NAME,
            itfgen.generate(BindingControllerDef.TYPE,mcomp,mcomp,false) );
        fcInterfaces.put(
            NameControllerDef.NAME,
            itfgen.generate(NameControllerDef.TYPE,mcomp,mcomp,false) );
        fcInterfaces.put(
            SuperControllerDef.NAME,
            itfgen.generate(SuperControllerDef.TYPE,mcomp,mcomp,false) );
        fcInterfaces.put(
            ContentControllerDef.NAME,
            itfgen.generate(ContentControllerDef.TYPE,mcomp,mcomp,false) );
        fcInterfaces.put(
            LifeCycleControllerDef.NAME,
            itfgen.generate(LifeCycleControllerDef.TYPE,mcomp,mcomp,false) );
        
        mcomp.setFcInterfaces(fcInterfaces);
        mcomp.setFcInternalInterfaces(fcInternalInterfaces);
        return mcomp;
    }
    
    /**
     * Return the implementation of the control component bound to the specified
     * interface.
     */
    private static Object getFcCtrlCompImpl( Object itf ) {
        while( itf instanceof ComponentInterface ) {
            ComponentInterface ci = (ComponentInterface) itf;
            itf = ci.getFcItfImpl();
        }
        while( itf instanceof Interceptor ) {
            Interceptor i = (Interceptor) itf;
            itf = i.getFcItfDelegate();
        }
        return itf;
    }
    
    /**
     * Generate and add an instance of an interceptor on the specified
     * fcinterface.
     * 
     * @param interceptctrl  the interceptor controller
     * @param interceptig    the interceptor instance generator
     * @param itf  the fcinterface where the interceptor must be added
     * @param ic   the context for initializing the generated interceptor
     * @param cc   the content controller for retrieving the internal
     *             interface associated to itf (may be null if the component
     *             is not composite)
     * @throws IllegalLifeCycleException 
     * @throws IllegalInterceptorException 
     */
    private static void addFcInterceptor(
        InterceptorController interceptctrl, InstanceGenerator interceptig,
        ComponentInterface itf,
        InitializationContext ic, ContentController cc )
    throws InstantiationException, IllegalLifeCycleException, IllegalInterceptorException {
        
        InterfaceType it = (InterfaceType) itf.getFcItfType();
        String itfName = itf.getFcItfName();
        
        /*
         * Create the interceptor.
         * 
         * generate returns null if the generation is not appropriate
         * e.g. the code generator mode (IN, OUT, or IN_OUT) does not
         * apply for this interface type.
         */
        Object content = interceptig.generate(it,ic);
        Interceptor interceptor = (Interceptor) content;
        interceptor.initFcController(ic);
        
        // Add the interceptor
        interceptctrl.addFcInterceptor(itf, interceptor);
        
        // The same for internal interfaces
        if( cc != null ) {
            if( it.isFcCollectionItf() ) {
                itfName = "/koch/collection/" + itfName;
            }
            ComponentInterface intitf =
                (ComponentInterface) getFcInternalInterface(cc, itfName);
            interceptctrl.addFcInterceptor(intitf, interceptor);
        }
    }

    
    // -----------------------------------------------------------------
    // Checks
    // -----------------------------------------------------------------
    
    /**
     * Check that the value given for the type is legal.
     */
    protected void checkFcType( Type type, Object controllerDesc )
        throws InstantiationException {
        
        /** type must be a component type. */
        if( ! (type instanceof ComponentType) )
            throw new InstantiationException(
                    "Argument type must be an instance of ComponentType");
        
        /** The Java interfaces referenced in the type must exist. */
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String itSignature = its[i].getFcItfSignature();
            try {
                loadClass(itSignature);
            }
            catch(ClassNotFoundException cnfe) {
                String msg =
                    "Interface "+itSignature+
                    " referenced in Fractal interface type "+
                    its[i].getFcItfName()+" not found";
                throw new ChainedInstantiationException(cnfe,null,msg);
            }
        }        
    }
        
    /**
     * Check that the given value is a legal controller description.
     */
    protected void checkFcControllerDesc( Object controllerDesc )
    throws InstantiationException {
        
        if( controllerDesc == null ) {
            throw new InstantiationException(
                    "Argument controllerDesc must be non null");        
        }
    }
    
    /**
     * Check that the value given for the content description is legal.
     */
    protected void checkFcContentDesc(
            Type type, Object controllerDesc, Object contentDesc )
    throws InstantiationException {
        
       /** Check for template components. */
        if( controllerDesc instanceof String ) {
            if( ((String)controllerDesc).endsWith("Template") ) {
                
                // Templates are created with something like:
                // Component cTmpl = cf.newFcInstance(
                //      cType, "primitiveTemplate",
                //      new Object[] { "primitive", "ClientImpl" });
                
                if( ! contentDesc.getClass().isArray() ) {
                    throw new InstantiationException(
                            "When instantiating a "+controllerDesc+
                            " contentDesc should be an array");
                }
                
                Object[] tab = (Object[]) contentDesc;
                if( tab.length != 2 ) {
                    throw new InstantiationException(
                            "When instantiating a "+controllerDesc+
                            " contentDesc should be an array of size 2");
                }
                
                checkFcControllerDesc(tab[0]);
                checkFcContentDesc(type,tab[0],tab[1]);
            }
        }
    }
    
    /**
     * Check that the content class implements the server interfaces defined in
     * the type.
     */
    protected void checkFcContentClassImplementsServerInterfaces(
            Type type, Class contentClass )
    throws InstantiationException {
        
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String name = its[i].getFcItfName();
            if( ! its[i].isFcClientItf() && isFcImplementableInterface(name) ) {
                
                String itSignature = its[i].getFcItfSignature();
                
                // Checked before by checkFcType
                // but we need the class here anyway
                Class itClass;
                try {
                    itClass = loadClass(itSignature);
                }
                catch(ClassNotFoundException cnfe) {
                    String msg =
                        "Interface "+itSignature+
                        " referenced in Fractal interface type "+
                        its[i].getFcItfName()+" not found";
                    throw new ChainedInstantiationException(cnfe,null,msg);
                }
                
                if( ! itClass.isAssignableFrom(contentClass) ) {
                    String msg =
                        "Component content class "+contentClass.getName()+
                        " should implement the server interface "+
                        itSignature;
                    throw new InstantiationException(msg);
                }
            }
        }
    }

    /**
     * Return true if the content class associated to the given controller
     * description must implement the BindingController interface when its type
     * defines at least one client interface. 
     */
    protected boolean isFcContentToBeCheckedForBC( Object controllerDesc ) {

        /*
         * The content class of component controllers need not implement
         * BindingController.
         */
        if( controllerDesc.equals("mPrimitive") ||
            controllerDesc.equals("mComposite") ) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Check that the content class implements the BindingController interface
     * if at least one client interface is defined in its type.
     */
    protected void checkFcContentClassforBC( Type type, Class contentClass )
    throws InstantiationException {
        
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            if( its[i].isFcClientItf() ) {
                if( ! BindingController.class.isAssignableFrom(contentClass) ) {
                    throw new InstantiationException(
                            "Content class "+contentClass.getName()+
                            " is associated to the client interface type "+
                            its[i].getFcItfName()+
                            " and must then implement the "+
                            BindingController.class.getName()+" interface");
                }
            }
        }
    }


    // -----------------------------------------------------------------
    // Utility methods
    // -----------------------------------------------------------------
    
    /**
     * A thread-local variable to hold the class loader instance which may be
     * transmitted to {@link #newFcInstance(Type, Object, Object)}.
     */
    private ThreadLocal tlclassloader = null;
    
    /**
     * Load the class whose name is given.
     */
    private Class loadClass( String name ) throws ClassNotFoundException {
        
        if( tlclassloader != null ) {
            ClassLoader classloader = (ClassLoader) tlclassloader.get();
            if( classloader != null ) {
                Class cl = classloader.loadClass(name);
                return cl;
            }
        }
        
        Class cl = loader.loadClass(name,null);
        return cl;
    }
    
    /**
     * Instantiate the given class.
     */
    private Object instanciateClass( Class cl ) throws InstantiationException {
        
        try {
            Object o = cl.newInstance();
            return o;
        }
        catch( java.lang.InstantiationException ie ) {
            final String msg =
                "java.lang.InstantiationException when instantiating class: "+
                cl.getName();
            throw new ChainedInstantiationException(ie,null,msg);
        }
        catch( IllegalAccessException iae ) {
            final String msg =
                "IllegalAccessException when instantiating class: "+
                cl.getName();
            throw new ChainedInstantiationException(iae,null,msg);
        }
    }
    
    /**
     * Return a copy of the given interface type where the direction of the
     * interface has been inverted: if the direction of the specified interface
     * type is client (resp. server), the direction of the returned interface
     * type is server (resp. client).
     */
    private static InterfaceType newSymetricInterfaceType( InterfaceType it ) {
        return
            new BasicInterfaceType(
                it.getFcItfName(),
                it.getFcItfSignature(),
                ! it.isFcClientItf(),   // client <-> server
                it.isFcOptionalItf(),
                it.isFcCollectionItf()
            );        
    }

    /**
     * Return true if the specified name corresponds to the name of a Fractal
     * controller interface.
     * Note: factory is not considered as a controller interface but as a
     * business one.
     */
    private static boolean isFcControllerInterfaceName(String itfName) {
        if( itfName.equals("component") ||
            itfName.endsWith("-controller") ) {
            return true;
        }
        return false;
    }

    /**
     * Return true if the specified name corresponds to the name of an interface
     * which should be implemented by a content class.
     */
    private static boolean isFcImplementableInterface( String itfName ) {
        if( itfName.equals("attribute-controller") ) {
            return true;
        }
        if( itfName.equals("factory") ) {
            return false;
        }
        return ! isFcControllerInterfaceName(itfName);
    }

    private static Object getFcInterface( Component c, String itfName ) {
        try {
            return c.getFcInterface(itfName);
        }
        catch( NoSuchInterfaceException nsie ) {
            throw new RuntimeException("No such interface: "+nsie.getMessage());
        }        
    }
    
    private static Object getFcInternalInterface( ContentController cc, String itfName ) {
        try {
            return cc.getFcInternalInterface(itfName);
        }
        catch( NoSuchInterfaceException nsie ) {
            throw new RuntimeException("No such interface: "+nsie.getMessage());
        }        
    }
    
    // -----------------------------------------------------------------
    // Implementation of the BindingController interface
    // -----------------------------------------------------------------

    private Loader loader;
    private MembraneFactory membraneFactory;
    
    public void bindFc(String clientItfName, Object serverItf) {
        if( clientItfName.equals("loader") ) {
            loader = (Loader) serverItf;
        }
        if( clientItfName.equals("membrane-factory") ) {
            membraneFactory = (MembraneFactory) serverItf;
        }
    }

    public String[] listFc() {
        return new String[]{"loader","membrane-factory"};
    }

    public Object lookupFc(String clientItfName) {
        if( clientItfName.equals("loader") ) {
            return loader;
        }
        if( clientItfName.equals("membrane-factory") ) {
            return membraneFactory;
        }
        return null;
    }

    public void unbindFc(String clientItfName) {
       if( clientItfName.equals("loader") ) {
            loader = null;
        }
        if( clientItfName.equals("membrane-factory") ) {
            membraneFactory = null;
        }
    }
}
