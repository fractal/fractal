/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.type.BasicInterfaceType;

/**
 * Interface for managing the interceptors associated with the client and server
 * interfaces of a component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public interface InterceptorController {

    final public static String NAME = "/interceptor-controller";
    
    final public static InterfaceType TYPE = 
        new BasicInterfaceType(
                NAME,
                InterceptorController.class.getName(),
                TypeFactory.SERVER,
                TypeFactory.MANDATORY,
                TypeFactory.SINGLE );

    /**
     * Appends the specified interceptor at the end of the list of already
     * existing interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     * 
     * @throws IllegalInterceptorException if the interceptor can not be added.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      LifeCycleController} interface, but it is not in an appropriate
     *      state to perform this operation.
     */
    public void addFcInterceptor( ComponentInterface itf, Interceptor interceptor )
    throws IllegalInterceptorException, IllegalLifeCycleException;

    /**
     * Returns the array of interceptors associated to the specified Fractal
     * interface.
     * 
     * @param itf  the Fractal interface
     * @return     the array of interceptors
     */
    public Interceptor[] getFcInterceptors( ComponentInterface itf );
    
    /**
     * Removes the specified interceptor from the list of already existing
     * interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     * @return             true if the list contained the specified interceptor
     * 
     * @throws IllegalInterceptorException if the interceptor can not be removed.
     * @throws IllegalLifeCycleException if this component has a {@link
     *      LifeCycleController} interface, but it is not in an appropriate
     *      state to perform this operation.
     */
    public boolean removeFcInterceptor( ComponentInterface itf, Interceptor interceptor )
    throws IllegalInterceptorException, IllegalLifeCycleException;
}
