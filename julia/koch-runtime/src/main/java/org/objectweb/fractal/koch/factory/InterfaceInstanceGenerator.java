/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.loader.Loader;

/**
 * This class generates Fractal {@link org.objectweb.fractal.api.Interface}
 * instances.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class InterfaceInstanceGenerator extends InstanceGenerator {
    
    public InterfaceInstanceGenerator( Loader loader ) {
        super(new InterfaceClassGenerator(loader));
    }
        
    /**
     * Return an {@link org.objectweb.fractal.api.Interface} instance
     * implementing a given Fractal {@link InterfaceType}.
     * 
     * @param it        the interface type
     * @param itfOwner  the component associated to the interface
     * @param content   the content to which the call must be delegated
     * @param isFcInternalInterface  true is the interface is internal
     * @return  the {@link org.objectweb.fractal.api.Interface} instance
     */
    public Interface generate(
        InterfaceType it, Component itfOwner, Object content,
        boolean isFcInternalInterface )
    throws org.objectweb.fractal.api.factory.InstantiationException {
        
        try {
            Class cl = generator.generate(it,null);            
            Constructor ctr =
                cl.getConstructor(
                        new Class[]{
                                Component.class, String.class, Type.class,
                                boolean.class, Object.class} );
            
            /*
             * Server interfaces delegate calls to the content, whereas the
             * delegate field (_delegate) is null when a client interface is
             * instanciated. In this case, the value will be set later on when a
             * binding will be established.
             * 
             * More precisely, the value is null for client external interfaces
             * and for their internal counterparts (which are of type server.)
             */
						boolean isClient = it.isFcClientItf();
						Object delegate = ((isClient && !isFcInternalInterface) || (!isClient && isFcInternalInterface)) ? null	: content;
            
            Object i = ctr.newInstance(
                    new Object[]{
                            itfOwner,it.getFcItfName(),it,
                            new Boolean(isFcInternalInterface),delegate});

            return (Interface) i;
        }
        catch (NoSuchMethodException e) {
            throw generateExceptionHandler(e);
        }
        catch (InstantiationException e) {
            throw generateExceptionHandler(e);
        }
        catch (IllegalAccessException e) {
            throw generateExceptionHandler(e);
        }
        catch (InvocationTargetException e) {
            throw generateExceptionHandler(e);
        }
        catch (ClassNotFoundException e) {
            throw generateExceptionHandler(e);
        }
    }
    
}
