/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.HashMap;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.BasicComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;


/**
 * This class generates {@link org.objectweb.fractal.api.Interface}
 * implementations.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class InterfaceClassGenerator implements ClassGenerator {

    private Loader loader;
    
    public InterfaceClassGenerator( Loader loader ) {
        this.loader = loader;
    }
    
    /**
     * Return true if the mode (IN, OUT or IN_OUT) of the class generator matchs
     * the mode (client or server) of the given interface type.
     */
    public boolean match( InterfaceType it ) {
        return true;
    }

		private final static Tree ttmp = new Tree(
			new Tree[] {
					new Tree(
							org.objectweb.fractal.julia.asm.InterfaceClassGenerator.class
									.getName()),
					new Tree(BasicComponentInterface.class.getName()) });

		private final static HashMap classMap = new HashMap();

    /**
     * Generate a class implementing the given Fractal interface type.
     * 
     * @param it  the corresponding interface type
     * @param ic  the context for initializing the generated class
     * @return    a class implementing the given Fractal interface type
     */
    public Class generate( InterfaceType it, InitializationContext ic )
    throws ClassNotFoundException {
        
        /*
         * Build the {@link Tree} structure needed as input by the Julia
         * bytecode generation framework.
         */
        String signature = it.getFcItfSignature();
				Class cl = (Class) classMap.get(signature);
				if (cl == null) {
					Tree args = new Tree(new Tree[] { ttmp,
						new Tree(new Tree[] { new Tree(signature) }) });

					cl = loader.loadClass(args, null);
					classMap.put(signature, cl);
        }
        return cl;
    }  
}
