/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;

/**
 * An interface to create component-based membranes.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public interface MembraneFactory {

    /**
     * Create a new control membrane.
     * 
     * @param controllerDesc  the controller description of the membrane
     * @return                the component implementing the control membrane  
     */
    public Component newFcMembrane( Object controllerDesc )
    throws InstantiationException;

    /**
     * Register a new membrane definition.
     * 
     * @param controllerDesc  the controller description of the membrane
     * @param adl             the fully-qualified name of the ADL describing this membrane 
     */
    public void registerFcNewMembraneDef( String controllerDesc, String adl )
    throws InstantiationException;
    
    /**
     * Return the fully-qualified name of the factory component for the
     * specified controller descriptor. In other terms, return the name of the
     * ADL descriptor for the specified membrane. Return null if no such
     * membrane is registered.
     * 
     * @param controllerDesc  the controller descriptor of the membrane
     * @return                the ADL for the specified membrane or null
     */
    public String getFcMembraneADL( Object controllerDesc );
}
