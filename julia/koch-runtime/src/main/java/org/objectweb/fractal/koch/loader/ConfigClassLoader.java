/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.loader;

import java.util.Map;

import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;

/**
 * A class loader equipped with a Julia configuration loader.
 * 
 * This class loader can load classes which are defined with a fully-qualified
 * name and classes which are described by a julia.cfg like descriptor. In this
 * last case, the class loader delegates the loading to the Julia configuration
 * loader.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public class ConfigClassLoader extends ClassLoader {
    
    public ConfigClassLoader( Loader loader, Map context ) {
        super();
        this.loader = loader;
        this.context = context;
    }    
    private Loader loader;
    private Map context;

    public Class loadClass(String name) throws ClassNotFoundException {

        // Try to load the class directly
        try {
            return loadClass(name, false);
        }
        catch( ClassNotFoundException cnfe ) {}
        
        /*
         * The class can not be loaded from the classpath.
         * Assume the given name is a julia configuration descriptor and use the
         * loader to generate and load the class.
         */
        try {
            Tree t = null;
            if( name.startsWith("'") ) {
                name = name.substring(1);
                t = loader.loadTree(name);
            }
            else {
                TreeParser tp = new TreeParser(name);
                t = tp.parseTree();
            }
            t = loader.evalTree(t, context);
            t = t.getSubTree(0);
            Class cl = loader.loadClass(t,null);
            return cl;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ClassNotFoundException(name);
        }
    }
}
