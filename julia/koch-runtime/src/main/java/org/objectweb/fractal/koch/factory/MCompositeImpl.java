/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.koch.control.component.ComponentControllerDef;

/**
 * Implementation of the control membrane for mComposite components.
 * mComposite components are composite control components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class MCompositeImpl
    implements
        Component, BindingController, NameController, SuperControllerNotifier,
        ContentController, LifeCycleCoordinator {
    
    private Type type;
    private Object content;
    
    public MCompositeImpl( Type type, Object content ) {
        this.type = type;
        this.content = content;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the Component interface
    // -----------------------------------------------------------------
    
    public Type getFcType() {
        return type;
    }

    public Object getFcInterface(String interfaceName)
    throws NoSuchInterfaceException {

        if( fcInterfaces.containsKey(interfaceName) ) {
            Object itf = fcInterfaces.get(interfaceName);
            return itf;
        }

        if( interfaceName.equals("/content") ) {
            return content;
        }
        
        // Collection interfaces
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (int i = 0; i < its.length; i++) {
            String itname = its[i].getFcItfName();
            if( its[i].isFcCollectionItf() && interfaceName.startsWith(itname) ) {
                String colitname = "/collection/" + itname;
                ComponentInterface citf = (ComponentInterface) fcInterfaces.get(colitname);
                ComponentInterface clone = (ComponentInterface) citf.clone();
                clone.setFcItfName(interfaceName);
                fcInterfaces.put(interfaceName, clone);
                return clone;
            }
        }
        
        throw new NoSuchInterfaceException(interfaceName);
    }
    
    public Object[] getFcInterfaces() {        
        return fcInterfaces.values().toArray();
    }
    
    private Map fcInterfaces = new HashMap();
    
    
    // -----------------------------------------------------------------
    // Implementation of the BindingController interface
    // -----------------------------------------------------------------
    
    public String[] listFc() {
        
        Set names = new HashSet();
        for (Iterator iter = fcInterfaces.entrySet().iterator(); iter.hasNext();) {
            Map.Entry entry = (Map.Entry) iter.next();
            ComponentInterface itf = (ComponentInterface) entry.getValue();
            Object target = itf.getFcItfImpl();
            if( target != null && (target instanceof ComponentInterface) ) {
                String itfName = (String) entry.getKey();
                names.add(itfName);
            }
        }
        return (String[]) names.toArray( new String[names.size()] );
    }
    
    public Object lookupFc( String arg0 ) throws NoSuchInterfaceException {
        
        // Singleton client interfaces
        if( fcInterfaces.containsKey(arg0) ) {
            Object o = fcInterfaces.get(arg0);
            ComponentInterface itf = (ComponentInterface) o;
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            if( it.isFcClientItf() ) {
                return itf.getFcItfImpl();
            }
        }
        
        // Collection client interfaces
        if( fcInterfaces.containsKey("/collection/"+arg0) ) {
            List bound = new ArrayList();
            for (Iterator iter = fcInterfaces.keySet().iterator(); iter.hasNext();) {
                String itfName = (String) iter.next();
                if( itfName.startsWith(arg0) ) {
                    Object o = lookupFc(itfName);
                    bound.add(o);
                }
            }
            return bound;
        }
        
        throw new NoSuchInterfaceException(arg0);
    }
    
    public void bindFc( String arg0, Object arg1 )
    throws NoSuchInterfaceException {
        
        /*
         * Let the client interface delegate to the target.
         */
        Object o = getFcInterface(arg0);
        ComponentInterface itf = (ComponentInterface) o;        
        itf.setFcItfImpl(arg1);
        
        /*
         * Let the corresponding internal interface delegate to the target.
         */
        o = getFcInternalInterface(arg0);
        ComponentInterface intitf = (ComponentInterface) o;        
        intitf.setFcItfImpl(arg1);
    }
    
    public void unbindFc( String arg0 ) throws NoSuchInterfaceException {

        Object o = getFcInterface(arg0);
        ComponentInterface itf = (ComponentInterface) o;
        itf.setFcItfImpl(null);

        o = getFcInternalInterface(arg0);
        ComponentInterface intitf = (ComponentInterface) o;        
        intitf.setFcItfImpl(null);
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the NameController interface
    // -----------------------------------------------------------------
    
    private String fcName;
    
    public String getFcName() {
        return fcName;
    }

    public void setFcName(String arg0) {
        fcName = arg0;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the SuperControllerNotifier interface
    // -----------------------------------------------------------------
    
    protected List fcSupers = new ArrayList(1);
    
    public Component[] getFcSuperComponents() {
        return (Component[]) fcSupers.toArray(new Component[fcSupers.size()]);
    }

    public void addedToFc( Component c ) {
        fcSupers.add(c);
    }
    
    public void removedFromFc( Component c ) {
        fcSupers.remove(c);
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the ContentController interface
    // -----------------------------------------------------------------

    private List fcContent = new ArrayList();
    private Map fcInternalInterfaces = new HashMap();
    
    public Object[] getFcInternalInterfaces() {
        return fcInternalInterfaces.values().toArray();
    }
    
    public Object getFcInternalInterface(String interfaceName)
    throws NoSuchInterfaceException {

        if( fcInternalInterfaces.containsKey(interfaceName) ) {
            Object itf = fcInternalInterfaces.get(interfaceName);
            return itf;
        }

        throw new NoSuchInterfaceException(interfaceName);
    }

    public Component[] getFcSubComponents() {
        return (Component[]) fcContent.toArray(new Component[fcContent.size()]);
    }

    public void addFcSubComponent(Component arg0) {
        fcContent.add(arg0);
        if( arg0 instanceof MPrimitiveImpl ) {
            ((MPrimitiveImpl)arg0).fcSupers.add(this);
        }
        else {
            /*
             * If a regular component is being added in the current mComposite
             * and if this component provides a SuperNotifier interface,
             * register the parent link.
             */
            try {
                SuperController sc = (SuperController) arg0.getFcInterface("super-controller");
                if( sc instanceof SuperControllerNotifier ) {
                    ((SuperControllerNotifier)sc).addedToFc(this);
                }
            }
            catch (NoSuchInterfaceException e) {}
        }
    }
    
    public void removeFcSubComponent(Component arg0) {
        fcContent.remove(arg0);
        if( arg0 instanceof MPrimitiveImpl ) {
            ((MPrimitiveImpl)arg0).fcSupers.remove(this);
        }
        else {
            /*
             * If a regular component is being removed from the current
             * mComposite and if this component provides a SuperNotifier
             * interface, remove the parent link.
             */
            try {
                SuperController sc = (SuperController) arg0.getFcInterface("super-controller");
                if( sc instanceof SuperControllerNotifier ) {
                    ((SuperControllerNotifier)sc).removedFromFc(this);
                }
            }
            catch (NoSuchInterfaceException e) {}
        }
    }


    // -----------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------
    
    public void setFcInterfaces( Map fcInterfaces ) {
        this.fcInterfaces = fcInterfaces;
        weaveableC = (Component) fcInterfaces.get(ComponentControllerDef.NAME);
    }
    
    public void setFcInternalInterfaces( Map fcInternalInterfaces ) {
        this.fcInternalInterfaces = fcInternalInterfaces;
    }    


    // -----------------------------------------------------------------
    // Implementation of the LifeCycleCoordinator interface
    // -----------------------------------------------------------------
    
    /** 
     * The {@link Component} interface of the component to which this controller
     * object belongs.
     * @see org.objectweb.fractal.julia.UseComponentMixin#weaveableC
     */
    private Component weaveableC;
    
    /** 
     * The components that are currently active.
     * @see org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin#fcActive
     */
    private List fcActive;
    
    /** 
     * Checks the mandatory client interfaces of the component (and of all its sub
     * components) and then calls the overriden method.
     * 
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin#startFc()
     */
    public void startFc() throws IllegalLifeCycleException {
        Component thisComponent;
        try {
            thisComponent = ((Component)(weaveableC.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            throw new ChainedIllegalLifeCycleException(e , weaveableC , "Cannot start the component");
        }
        List allSubComponents = org.objectweb.fractal.julia.control.content.Util.getAllSubComponents(thisComponent);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component subComponent = ((Component)(allSubComponents.get(i)));
            try {
                checkFcMandatoryInterfaces(subComponent);
            } catch (IllegalBindingException e) {
                throw new ChainedIllegalLifeCycleException(e , weaveableC , "Cannot start the component");
            }
        }
        startFc$0();
    }
    
    /** 
     * Indicates if this component is started or not.
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#fcStarted
     */
    private boolean fcStarted;
    
    /** 
     * Sets the lifecycle state of this component and of all its direct and
     * indirect sub components that have a {@link LifeCycleCoordinator} interface.
     *
     * @param started <tt>true</tt> to set the lifecycle state of the components
     *      to {@link #STARTED STARTED}, or <tt>false</tt> to set this state to
     *      {@link #STOPPED STOPPED}.
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin#setFcState(boolean)
     */
    public void setFcState(final boolean started) throws IllegalLifeCycleException {
        Component thisComponent;
        try {
            thisComponent = ((Component)(weaveableC.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            throw new ChainedIllegalLifeCycleException(e , weaveableC , "Cannot set the lifecycle state");
        }
        List allSubComponents = org.objectweb.fractal.julia.control.content.Util.getAllSubComponents(thisComponent);
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component c = ((Component)(allSubComponents.get(i)));
            LifeCycleCoordinator lc;
            try {
                lc = ((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller")));
            } catch (Exception e) {
                try {
                    lc = ((LifeCycleCoordinator)(c.getFcInterface("/lifecycle-coordinator")));
                } catch (NoSuchInterfaceException f) {
                    continue;
                }
            }
            if (started) {
                lc.setFcStarted();
            } else {
                lc.setFcStopped();
            }
        }
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#getFcState()
     */
    public String getFcState() {
        return fcStarted ? LifeCycleController.STARTED : LifeCycleController.STOPPED;
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#startFc()
     */
    private void startFc$0() throws IllegalLifeCycleException {
        Component id;
        try {
            id = ((Component)(weaveableC.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            throw new ChainedIllegalLifeCycleException(e , weaveableC , "Cannot start component");
        }
        LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(id);
        for (int i = 0 ; i < (clccs.length) ; ++i) {
            clccs[i].setFcStarted();
        }
        setFcState(true);
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#stopFc()
     */
    public void stopFc() throws IllegalLifeCycleException {
        Component id;
        try {
            id = ((Component)(weaveableC.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            throw new ChainedIllegalLifeCycleException(e , weaveableC , "Cannot stop component");
        }
        LifeCycleCoordinator[] clccs = getFcLifeCycleControllers(id);
        stopFc(clccs);
        setFcState(false);
    }
    
    /** 
     * Checks that all the mandatory client interface of the given component are
     * bound.
     *
     * @param c a component.
     * @throws IllegalBindingException if a mandatory client interface of the
     *      given component is not bound.
     * @see org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin#checkFcMandatoryInterfaces(org.objectweb.fractal.api.Component)
     */
    public void checkFcMandatoryInterfaces(final Component c) throws IllegalBindingException {
        BindingController bc;
        try {
            bc = ((BindingController)(c.getFcInterface("binding-controller")));
        } catch (NoSuchInterfaceException e) {
            return ;
        }
        ComponentType compType = ((ComponentType)(c.getFcType()));
        String[] names = bc.listFc();
        for (int i = 0 ; i < (names.length) ; ++i) {
            InterfaceType itfType;
            try {
                itfType = compType.getFcInterfaceType(names[i]);
            } catch (NoSuchInterfaceException e) {
                continue;
            }
            if ((itfType.isFcClientItf()) && (!(itfType.isFcOptionalItf()))) {
                Object sItf;
                try {
                    sItf = bc.lookupFc(names[i]);
                } catch (NoSuchInterfaceException e) {
                    continue;
                }
                if (sItf == null) {
                    throw new ChainedIllegalBindingException(null , c , null , names[i] , null , "Mandatory client interface unbound");
                } 
            } 
        }
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin#fcActivated(org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator)
     */
    public boolean fcActivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            if ((fcActive.size()) > 0) {
                if (!(fcActive.contains(component))) {
                    fcActive.add(component);
                } 
                return true;
            } 
            return false;
        }
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#setFcStarted()
     */
    public boolean setFcStarted() {
        if (!(fcStarted)) {
            fcStarted = true;
            return true;
        } 
        return false;
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin#fcInactivated(org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator)
     */
    public void fcInactivated(final LifeCycleCoordinator component) {
        synchronized(fcActive) {
            fcActive.remove(component);
            fcActive.notifyAll();
        }
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#setFcStopping(org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator)
     */
    public void setFcStopping(final LifeCycleCoordinator coordinator) {
        throw new Error("Internal error");
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#setFcStopped()
     */
    public boolean setFcStopped() {
        if (fcStarted) {
            fcStarted = false;
            return true;
        } 
        return false;
    }
    
    /** 
     * Stops the given components simultaneously. This method sets the state of
     * the components to "<tt>STOPPING</tt>", waits until all the components
     * are simultaneoulsy inactive (their state is known thanks to the {@link
     * #fcActivated fcActivated} and {@link #fcInactivated fcInactivated} callback
     * methods), and then sets the state of the components to {@link #STOPPED
     * STOPPED}.
     *
     * @param components the {@link LifeCycleCoordinator} interface of the
     *      components to be stopped.
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin#stopFc(org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator[])
     */
    public void stopFc(final LifeCycleCoordinator[] components) throws IllegalLifeCycleException {
        fcActive = new ArrayList();
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                fcActive.add(components[i]);
            } 
        }
        LifeCycleCoordinator c;
        try {
            c = ((LifeCycleCoordinator)(weaveableC.getFcInterface("lifecycle-controller")));
        } catch (Exception e) {
            try {
                c = ((LifeCycleCoordinator)(weaveableC.getFcInterface("/lifecycle-coordinator")));
            } catch (NoSuchInterfaceException f) {
                throw new ChainedIllegalLifeCycleException(f , weaveableC , "Cannot stop components");
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopping(c);
            } 
        }
        synchronized(fcActive) {
            while ((fcActive.size()) > 0) {
                try {
                    fcActive.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        for (int i = 0 ; i < (components.length) ; ++i) {
            if (components[i].getFcState().equals(LifeCycleController.STARTED)) {
                components[i].setFcStopped();
            } 
        }
        fcActive = null;
    }
    
    /** 
     * Returns the components that must be stopped in order to stop the given
     * component. These components are the direct or indirect primitive sub
     * components of this component that provide a {@link LifeCycleCoordinator}
     * interface, as well as the primitive client components of these components.
     *
     * @param id a composite component.
     * @return the components that must be stopped in order to stop the given
     *      component.
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#getFcLifeCycleControllers(org.objectweb.fractal.api.Component)
     */
    public LifeCycleCoordinator[] getFcLifeCycleControllers(final Component id) throws IllegalLifeCycleException {
        List clccList = getFcInternalLifeCycleControllers();
        Object[] sItfs = id.getFcInterfaces();
        Set visited = new HashSet();
        for (int i = 0 ; i < (sItfs.length) ; ++i) {
            Interface sItf = ((Interface)(sItfs[i]));
            if (!(((InterfaceType)(sItf.getFcItfType())).isFcClientItf())) {
                getSExtLifeCycleControllers(sItf ,clccList ,visited);
            } 
        }
        LifeCycleCoordinator[] clccs;
        clccs = new LifeCycleCoordinator[clccList.size()];
        return ((LifeCycleCoordinator[])(clccList.toArray(clccs)));
    }
    
    /** 
     * Finds the primitive client components that are bound to the given server
     * interface.
     *
     * @param serverItf a server interface.
     * @param clccList where to put the {@link LifeCycleCoordinator}
     *      interfaces of the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#getSExtLifeCycleControllers(org.objectweb.fractal.api.Interface,java.util.List,java.util.Set)
     */
    private void getSExtLifeCycleControllers(final Interface serverItf, final List clccList, final Set visited) throws IllegalLifeCycleException {
        Object[] comps;
        try {
            comps = org.objectweb.fractal.julia.control.binding.Util.getFcPotentialClientsOf(serverItf).toArray();
        } catch (Exception e) {
            throw new ChainedIllegalLifeCycleException(e , serverItf.getFcItfOwner() , "Cannot get the LifeCycleCoordinator interfaces");
        }
        for (int i = 0 ; i < (comps.length) ; ++i) {
            Component comp = ((Component)(comps[i]));
            Interface[] clientItfs;
            try {
                List l = org.objectweb.fractal.julia.control.binding.Util.getFcClientItfsBoundTo(comp ,serverItf);
                clientItfs = ((Interface[])(l.toArray(new Interface[l.size()])));
            } catch (Exception e) {
                throw new ChainedIllegalLifeCycleException(e , serverItf.getFcItfOwner() , "Cannot get the LifeCycleCoordinator interfaces");
            }
            for (int j = 0 ; j < (clientItfs.length) ; ++j) {
                getCExtLifeCycleControllers(clientItfs[j] ,clccList ,visited);
            }
        }
    }
    
    /** 
     * Finds the primitive client components that are bound to the given client
     * interface.
     *
     * @param clientItf a client interface.
     * @param clccList where to put the {@link LifeCycleCoordinator} interfaces of
     *      the primitive client components that are found.
     * @param visited the already visited interfaces.
     * @throws IllegalLifeCycleException if a problem occurs.
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#getCExtLifeCycleControllers(org.objectweb.fractal.api.Interface,java.util.List,java.util.Set)
     */
    private void getCExtLifeCycleControllers(final Interface clientItf, final List clccList, final Set visited) throws IllegalLifeCycleException {
        Component component = clientItf.getFcItfOwner();
        ContentController cc = null;
        try {
            cc = ((ContentController)(component.getFcInterface("content-controller")));
        } catch (NoSuchInterfaceException e) {
        }
        if (cc != null) {
            Interface itf;
            String name = clientItf.getFcItfName();
            try {
                if (!(clientItf.isFcInternalItf())) {
                    itf = ((Interface)(cc.getFcInternalInterface(name)));
                } else {
                    itf = ((Interface)(component.getFcInterface(name)));
                }
            } catch (NoSuchInterfaceException e) {
                throw new ChainedIllegalLifeCycleException(e , component , "Cannot find the LifeCycleCoordinator interfaces");
            }
            if (!(visited.contains(itf))) {
                visited.add(itf);
                getSExtLifeCycleControllers(itf ,clccList ,visited);
            } 
        } else if (!(visited.contains(clientItf))) {
            visited.add(clientItf);
            //Component c = clientItf.getFcItfOwner();
            LifeCycleCoordinator lcc;
            try {
                lcc = ((LifeCycleCoordinator)(component.getFcInterface("lifecycle-controller")));
            } catch (Exception e) {
                try {
                    lcc = ((LifeCycleCoordinator)(component.getFcInterface("/lifecycle-coordinator")));
                } catch (NoSuchInterfaceException f) {
                    throw new ChainedIllegalLifeCycleException(f , component , "Primitive client without a LifeCycleCoordinator");
                }
            }
            if (!(clccList.contains(lcc))) {
                clccList.add(lcc);
            } 
        } 
    }
    
    /** 
     * @see org.objectweb.fractal.julia.control.lifecycle.OptimizedLifeCycleControllerMixin#getFcInternalLifeCycleControllers()
     */
    public List getFcInternalLifeCycleControllers() throws IllegalLifeCycleException {
        Component thisComponent;
        try {
            thisComponent = ((Component)(weaveableC.getFcInterface("component")));
        } catch (NoSuchInterfaceException e) {
            throw new ChainedIllegalLifeCycleException(e , weaveableC , ("The OptimizedLifeCycleControllerMixin requires " + "components to provide the Component interface"));
        }
        List allSubComponents = org.objectweb.fractal.julia.control.content.Util.getAllSubComponents(thisComponent);
        List result = new ArrayList();
        for (int i = 0 ; i < (allSubComponents.size()) ; ++i) {
            Component c = ((Component)(allSubComponents.get(i)));
            try {
                c.getFcInterface("content-controller");
            } catch (NoSuchInterfaceException e) {
                try {
                    result.add(((LifeCycleCoordinator)(c.getFcInterface("lifecycle-controller"))));
                } catch (Exception f) {
                    try {
                        result.add(c.getFcInterface("/lifecycle-coordinator"));
                    } catch (NoSuchInterfaceException ignored) {
                    }
                }
            }
        }
        return result;
    }
}