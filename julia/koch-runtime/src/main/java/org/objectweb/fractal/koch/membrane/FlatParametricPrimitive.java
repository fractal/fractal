/*
 * Generated by org.objectweb.fractal.koch.mc.MembraneCompiler on: Sat Dec 29 18:02:35 CET 2007
 */
package org.objectweb.fractal.koch.membrane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.fractal.api.NoSuchInterfaceException;

/**
 * Implementation of the FlatParametricPrimitive membrane factory component.
 *
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class FlatParametricPrimitive implements org.objectweb.fractal.api.factory.Factory, org.objectweb.fractal.api.control.BindingController {

  public Object getFcContentDesc() {
    throw new java.lang.UnsupportedOperationException();
  }

  public Object getFcControllerDesc() {
    throw new java.lang.UnsupportedOperationException();
  }

  public Component newFcInstance() throws org.objectweb.fractal.api.factory.InstantiationException {
    try {
      // --------------------------------------------------
InterfaceType IT0 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", false, false, false);
ComponentType CT0 = typeFactory.createFcType(new InterfaceType [] { IT0 });
Component C0 = genericFactory.newFcInstance(CT0, "mPrimitive", "'component-impl");
try { Fractal.getNameController(C0).setFcName("Comp"); } catch (NoSuchInterfaceException ignored) { }
Fractal.getLifeCycleController(C0).startFc();
InterfaceType IT1 = typeFactory.createFcItfType("//name-controller", "org.objectweb.fractal.api.control.NameController", false, false, false);
ComponentType CT1 = typeFactory.createFcType(new InterfaceType [] { IT1 });
Component C1 = genericFactory.newFcInstance(CT1, "mPrimitive", "'name-controller-impl");
try { Fractal.getNameController(C1).setFcName("NC"); } catch (NoSuchInterfaceException ignored) { }
Fractal.getLifeCycleController(C1).startFc();
InterfaceType IT2 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", true, false, false);
InterfaceType IT3 = typeFactory.createFcItfType("//lifecycle-controller", "org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator", false, false, false);
ComponentType CT2 = typeFactory.createFcType(new InterfaceType [] { IT2, IT3 });
Component C2 = genericFactory.newFcInstance(CT2, "mPrimitive", "'lifecycle-controller-impl");
try { Fractal.getNameController(C2).setFcName("LC"); } catch (NoSuchInterfaceException ignored) { }
InterfaceType IT4 = typeFactory.createFcItfType("//lifecycle-controller", "org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator", true, false, false);
InterfaceType IT5 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", true, false, false);
InterfaceType IT6 = typeFactory.createFcItfType("//binding-controller", "org.objectweb.fractal.api.control.BindingController", false, false, false);
ComponentType CT3 = typeFactory.createFcType(new InterfaceType [] { IT4, IT5, IT6 });
Component C3 = genericFactory.newFcInstance(CT3, "mPrimitive", "'flat-container-binding-controller-impl");
try { Fractal.getNameController(C3).setFcName("BC"); } catch (NoSuchInterfaceException ignored) { }
InterfaceType IT7 = typeFactory.createFcItfType("//lifecycle-controller", "org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator", true, false, false);
InterfaceType IT8 = typeFactory.createFcItfType("//binding-controller", "org.objectweb.fractal.api.control.BindingController", true, false, false);
InterfaceType IT9 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", true, false, false);
InterfaceType IT10 = typeFactory.createFcItfType("///interceptor-controller", "org.objectweb.fractal.koch.control.interceptor.InterceptorController", false, false, false);
InterfaceType IT11 = typeFactory.createFcItfType("attribute-controller", "org.objectweb.fractal.koch.control.interceptor.InterceptorDefAttributes", false, false, false);
ComponentType CT4 = typeFactory.createFcType(new InterfaceType [] { IT7, IT8, IT9, IT10, IT11 });
Component C4 = genericFactory.newFcInstance(CT4, "mPrimitive", "'interceptor-controller-impl");
try { Fractal.getNameController(C4).setFcName("IC"); } catch (NoSuchInterfaceException ignored) { }
((org.objectweb.fractal.koch.control.interceptor.InterceptorDefAttributes)Fractal.getAttributeController(C4)).setInterceptors("(org.objectweb.fractal.julia.asm.InterceptorClassGenerator org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator)");
InterfaceType IT12 = typeFactory.createFcItfType("///membrane-controller", "org.objectweb.fractal.koch.control.membrane.MembraneController", false, false, false);
ComponentType CT5 = typeFactory.createFcType(new InterfaceType [] { IT12 });
Component C5 = genericFactory.newFcInstance(CT5, "mPrimitive", "'membrane-controller-impl");
try { Fractal.getNameController(C5).setFcName("MC"); } catch (NoSuchInterfaceException ignored) { }
Fractal.getLifeCycleController(C5).startFc();
InterfaceType IT13 = typeFactory.createFcItfType("//name-controller", "org.objectweb.fractal.api.control.NameController", false, false, false);
InterfaceType IT14 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", false, false, false);
InterfaceType IT15 = typeFactory.createFcItfType("//binding-controller", "org.objectweb.fractal.api.control.BindingController", false, false, false);
InterfaceType IT16 = typeFactory.createFcItfType("//lifecycle-controller", "org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator", false, false, false);
InterfaceType IT17 = typeFactory.createFcItfType("///interceptor-controller", "org.objectweb.fractal.koch.control.interceptor.InterceptorController", false, false, false);
InterfaceType IT18 = typeFactory.createFcItfType("///membrane-controller", "org.objectweb.fractal.koch.control.membrane.MembraneController", false, false, false);
ComponentType CT6 = typeFactory.createFcType(new InterfaceType [] { IT13, IT14, IT15, IT16, IT17, IT18 });
Component C6 = genericFactory.newFcInstance(CT6, "mComposite", null);
try { Fractal.getNameController(C6).setFcName("org.objectweb.fractal.koch.membrane.FlatParametricPrimitive"); } catch (NoSuchInterfaceException ignored) { }
Fractal.getContentController(C6).addFcSubComponent(C0);
Fractal.getContentController(C6).addFcSubComponent(C1);
Fractal.getContentController(C6).addFcSubComponent(C2);
Fractal.getContentController(C6).addFcSubComponent(C3);
Fractal.getContentController(C6).addFcSubComponent(C4);
Fractal.getContentController(C6).addFcSubComponent(C5);
Fractal.getBindingController(C6).bindFc("//component", C0.getFcInterface("//component"));
Fractal.getBindingController(C6).bindFc("//name-controller", C1.getFcInterface("//name-controller"));
Fractal.getBindingController(C6).bindFc("//lifecycle-controller", C2.getFcInterface("//lifecycle-controller"));
Fractal.getBindingController(C6).bindFc("//binding-controller", C3.getFcInterface("//binding-controller"));
Fractal.getBindingController(C6).bindFc("///interceptor-controller", C4.getFcInterface("///interceptor-controller"));
Fractal.getBindingController(C3).bindFc("//component", C0.getFcInterface("//component"));
Fractal.getBindingController(C3).bindFc("//lifecycle-controller", C2.getFcInterface("//lifecycle-controller"));
Fractal.getBindingController(C2).bindFc("//component", C0.getFcInterface("//component"));
Fractal.getBindingController(C4).bindFc("//component", C0.getFcInterface("//component"));
Fractal.getBindingController(C4).bindFc("//lifecycle-controller", C2.getFcInterface("//lifecycle-controller"));
Fractal.getBindingController(C4).bindFc("//binding-controller", C3.getFcInterface("//binding-controller"));
Fractal.getBindingController(C6).bindFc("///membrane-controller", C5.getFcInterface("///membrane-controller"));
Fractal.getLifeCycleController(C2).startFc();
Fractal.getLifeCycleController(C3).startFc();
Fractal.getLifeCycleController(C4).startFc();
Fractal.getLifeCycleController(C6).startFc();
      // --------------------------------------------------
      return C6;
    }
    catch( Exception e ) {
      e.printStackTrace();
      throw new org.objectweb.fractal.api.factory.InstantiationException(e.getMessage());
    }
  }

  public org.objectweb.fractal.api.Type getFcInstanceType() {
    try {
      // --------------------------------------------------
InterfaceType IT19 = typeFactory.createFcItfType("//name-controller", "org.objectweb.fractal.api.control.NameController", false, false, false);
InterfaceType IT20 = typeFactory.createFcItfType("//component", "org.objectweb.fractal.api.Component", false, false, false);
InterfaceType IT21 = typeFactory.createFcItfType("//binding-controller", "org.objectweb.fractal.api.control.BindingController", false, false, false);
InterfaceType IT22 = typeFactory.createFcItfType("//lifecycle-controller", "org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator", false, false, false);
InterfaceType IT23 = typeFactory.createFcItfType("///interceptor-controller", "org.objectweb.fractal.koch.control.interceptor.InterceptorController", false, false, false);
InterfaceType IT24 = typeFactory.createFcItfType("///membrane-controller", "org.objectweb.fractal.koch.control.membrane.MembraneController", false, false, false);
ComponentType CT7 = typeFactory.createFcType(new InterfaceType [] { IT19, IT20, IT21, IT22, IT23, IT24 });
      // --------------------------------------------------
      return CT7;
    }
    catch( Exception e ) {
      e.printStackTrace();
      throw new RuntimeException(e.getMessage());
    }
  }

  private org.objectweb.fractal.api.type.TypeFactory typeFactory;
  private org.objectweb.fractal.api.factory.GenericFactory genericFactory;

  public String[] listFc() {
    return new String[]{"type-factory","generic-factory"};
  }

  public void bindFc( String clientItfName, Object serverItf ) {
    if( clientItfName.equals("type-factory") ) {
      typeFactory = (org.objectweb.fractal.api.type.TypeFactory) serverItf;
    }
    if( clientItfName.equals("generic-factory") ) {
      genericFactory = (org.objectweb.fractal.api.factory.GenericFactory) serverItf;
    }
  }

  public Object lookupFc( String clientItfName ) {
    if( clientItfName.equals("type-factory") ) {
      return typeFactory;
    }
    if( clientItfName.equals("generic-factory") ) {
      return genericFactory;
    }
    return null;
  }

  public void unbindFc( String clientItfName ) {
    if( clientItfName.equals("type-factory") ) {
      typeFactory = null;
    }
    if( clientItfName.equals("generic-factory") ) {
      genericFactory = null;
    }
  }
}
