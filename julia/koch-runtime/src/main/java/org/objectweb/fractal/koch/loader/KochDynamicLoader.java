/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Lionel Seinturier
 *
 * Contributor: Philippe Merle
 */

package org.objectweb.fractal.koch.loader;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.julia.loader.DynamicLoader;
import org.objectweb.fractal.julia.loader.Tree;

/**
 * {@link org.objectweb.fractal.julia.loader.Loader} implementation used by
 * Koch.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public class KochDynamicLoader extends DynamicLoader implements KochDynamicLoaderAttributes {
    
    public KochDynamicLoader() {
        super();
    }

    // ---------------------------------------------------------------------
    // Loader interface implementation
    // ---------------------------------------------------------------------

    /**
     * @see {@link org.objectweb.fractal.julia.loader.Loader#loadClass(String,Object)}
     */
    public Class loadClass (final String className, Object loader)
      throws ClassNotFoundException
    {
	    if(loader == null) {
            // To retrieve content classes and interfaces specified by applications
            // it is more appropriate to use the current thread class loader instead of
            // the class loader which has loaded the Julia Loader class.
            loader = Thread.currentThread().getContextClassLoader();
    	}
        return super.loadClass(className, loader);
    }

    /**
     * @see {@link org.objectweb.fractal.julia.loader.Loader#loadClass(Tree,Object)}
     */
    public Class loadClass (final Tree classDescriptor, Object loader)
      throws ClassNotFoundException
      {
        if(loader == null) {
    	    // To retrieve content classes and interfaces specified by applications
            // it is more appropriate to use the current thread class loader instead of
            // the class loader which has loaded the Julia Loader class.
    	    loader = Thread.currentThread().getContextClassLoader();
    	}
    	return super.loadClass(classDescriptor, loader);
    }

    // ---------------------------------------------------------------------
    // KochDynamicLoaderAttributes interface implementation
    // ---------------------------------------------------------------------

    public void setConfigFileName(String configFileName) {
        if( this.configFileName.length() != 0 ) {
            this.configFileName += ",";
        }
        this.configFileName += configFileName;
        Map context = new HashMap();
        context.put("julia.config",configFileName);
        try {
            super.init(context);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public String getConfigFileName() {
        return configFileName;
    }
    
    private String configFileName = "";
}
