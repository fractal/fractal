/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.InterceptorInterface;
import org.objectweb.fractal.julia.asm.CodeGenerator;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.koch.loader.TreeParser;
import org.objectweb.fractal.koch.loader.TreeParserException;


/**
 * This class generates {@link org.objectweb.fractal.julia.Interceptor}
 * implementations.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public class InterceptorClassGenerator implements ClassGenerator {
    
    /**
     * The descriptor for this generator.
     * 
     * The string representation of this {@link Tree} structure follows a LISP
     * like syntax such as "(foo (bar foo))".
     * 
     * An example of such a descriptor for lifecycle interceptors follows:
     * "(org.objectweb.fractal.julia.asm.InterceptorClassGenerator
     *    org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator
     * )"
     * 
     * Another, slightly more complex one, taken from the statController
     * example:
     * (org.objectweb.fractal.julia.asm.InterceptorClassGenerator
     *    org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator
     *    stat.StatCodeGenerator
     *    stat.StatClassTransformer # transforms field accesses in content class
     * )
     */
    private Tree tdesc;
    
    private Loader loader;
    
    private final static HashMap map = new HashMap();
    private final static Tree treeIntercept = new Tree(InterceptorInterface.class.getName());
    private final static Tree treeObject = new Tree(Object.class.getName());
    private final static Tree treeIn = new Tree("in");
    private final static Tree treeOut = new Tree("out");
    
    public InterceptorClassGenerator( Loader loader, String desc ) throws TreeParserException {
        this.loader = loader;
        tdesc = (Tree) map.get(desc);
        if (tdesc == null) {
        	TreeParser tp = new TreeParser(desc);
        	tdesc = tp.parseTree();
        	map.put(desc, tdesc);
        }
    }
    
    /**
     * Return true if the mode (IN, OUT or IN_OUT) of at least one of the class
     * generators matchs the mode (client or server) of the given interface
     * type.
     */
    public boolean match( InterfaceType it ) {
        Tree[] trees = tdesc.getSubTrees();
        // Skip the 1st sub tree (InterceptorClassGenerator)
        for (int i = 1; i < trees.length; i++) {
            String cgclassname = trees[i].toString();
            try {
                Class cl = loader.loadClass(cgclassname,null);
                CodeGenerator cg = (CodeGenerator) cl.newInstance();
                int mode = cg.init(null);
                if( mode==CodeGenerator.IN && ! it.isFcClientItf() ) {
                    return true;
                }
                if( mode==CodeGenerator.IN_OUT ) {
                    return true;
                }
                if( mode==CodeGenerator.OUT && it.isFcClientItf() ) {
                    return true;
                }
            }
            catch (ClassNotFoundException e) {
                final String msg = e.getClass()+": "+e.getMessage();
                throw new RuntimeException(msg);
            }
            catch (InstantiationException e) {
                final String msg = e.getClass()+": "+e.getMessage();
                throw new RuntimeException(msg);
            }
            catch (IllegalAccessException e) {
                final String msg = e.getClass()+": "+e.getMessage();
                throw new RuntimeException(msg);
            }
        }
        
        return false;
    }

    /**
     * Generate a class implementing the given Fractal interface type.
     * 
     * @param it  the corresponding interface type
     * @param ic  the context for initializing the generated class
     * @return    a class implementing the given Fractal interface type
     */
    public Class generate( InterfaceType it, InitializationContext ic )
    throws ClassNotFoundException {
                
        /*
         * Retrieve controller classes.
         * They are needed by the Julia bytecode generation framework.
         * The rationale is that the lifecycle interceptor invokes not only the
         * public interface of the lifecycle controller, but also modifies some
         * public fields (e.g. fcInvocationCounter). We can then not rely on
         * control interfaces, and we need implementation classes.
         */
        List ctrlclasses = new ArrayList();
        Collection values = ic.interfaces.values();
        for (Iterator iter = values.iterator(); iter.hasNext();) {
            Object  o = iter.next();
            Class cl = o.getClass();
            ctrlclasses.add(cl);
        }
        Tree[] ctrls = new Tree[ctrlclasses.size()];
        int i = 0;
        for (Iterator iter = ctrlclasses.iterator(); iter.hasNext();) {
            Class ctrlclass = (Class) iter.next();
            ctrls[i] = new Tree(ctrlclass.getName());
            i++;
        }
        
        /*
         * Use the Julia bytecode generation framework for interceptors.
         * 
         * t typically looks like:
         *     (org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator) or
         *     (org.objectweb.fractal.julia.asm.LifeCycleCodeGenerator
         *          stat.StatCodeGenerator
         *          stat.StatClassTransformer
         *     )
         */
        Tree[] t = new Tree[tdesc.getSize()-1];
        System.arraycopy(tdesc.getSubTrees(), 1, t, 0, t.length);
        
        /*
         * Build the {@link Tree} structure needed as input by the Julia
         * bytecode generation framework.
         */
        String signature = it.getFcItfSignature();
        Tree args =
            new Tree(
                new Tree[]{
                    tdesc,
                    it.isFcClientItf() ? treeIntercept : treeObject,
                    new Tree( new Tree[]{new Tree(signature)} ),
                    new Tree(ctrls),
                    it.isFcClientItf() ? treeOut : treeIn
                    
                }
            );
        
        Class cl = loader.loadClass(args, null);
        return cl;
    }
}
