/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.factory;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.InitializationContext;

/**
 * Root class for {@link InterfaceType}-based class generators.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public interface ClassGenerator {
    
    /**
     * Return true if the mode (IN, OUT or IN_OUT) of the class generator matchs
     * the mode (client or server) of the given interface type.
     */
    public boolean match( InterfaceType it );

    /**
     * Generate a class implementing the given Fractal interface type.
     * 
     * @param it  the interface type
     * @param ic  the context for initializing the generated class
     * @return    a class implementing the given Fractal interface type
     */
    public Class generate( InterfaceType it, InitializationContext ic )
    throws ClassNotFoundException;
}
