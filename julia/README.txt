Julia 2.5
---------

Julia is the INRIA and France Telecom implementation of the Fractal
specifications. Julia is a free software distributed under the terms of the
GNU Lesser General Public license. Julia is written in Java.

Authors:
- Eric Bruneton <Eric.Bruneton@rd.francetelecom.com>
- Lionel Seinturier <Lionel.Seinturier@lifl.fr>

Eric Bruneton is the original and main developer of Julia. Lionel Seinturier
contributed the component-based versions of control membranes.


Table of content
----------------
  0. What's new with Julia 2.5
  1. Introduction
  2. Requirements
  3. Compiling Julia
  4. Running the examples distributed with Julia
  5. Libraries included with Julia


0. What's new with Julia 2.5
----------------------------
This section enumerates the features which are new in version 2.5. This section
is mainly intended for advanced Julia users. Newcomers may skip it and proceed
with Section 1.

The main new feature of Julia 2.5 is the possibility to develop applications
with component-based control membranes. This feature which was originally
developed for AOKell <http://fractal.objectweb.org/aokell> has been ported to
Julia. 

Some other new features follow:
- the introspection of a control membrane,
- the gluing of a control membrane to a content,
- the dynamic management of interceptors,
- the joint use of object-oriented and component-based control membrane.

These features are presented in Section 9 of the Java API documentation.
See <http://fractal.objectweb.org/current/doc/javadoc/julia/overview-summary.html#9>.


1. Introduction
---------------
Julia is a Java framework for running Fractal component-based applications.
The main entry point in the framework is the Fractal provider class. A Fractal
provider class is a factory for retrieving a bootstrap component which provides
a generic factory and a type factory.

Julia provides two Fractal provider classes:

- org.objectweb.fractal.julia.Julia
  This is the original Fractal provider class.

- org.objectweb.fractal.koch.Koch
  This is an alternate Fractal provider class which is available since Julia
  2.5. Components created by this provider are associated with a component-based
  control membrane.

These two Fractal provider classes can be used independently or jointly. When
used jointly in the same JVM, applications can use either components controlled
with component-based membranes, or components controlled with object-based
membranes.


2. Requirements
---------------
The following software is required to compile, run and use Julia:
	- Java SE >= 1.3
	- Maven >= 2.0.6


3. Compiling Julia
------------------
To compile Julia, from the root directory where Julia has been installed,
type:
	mvn install


4. Running the examples distributed with Julia
----------------------------------------------
Several examples are distributed with Julia. The source code of these examples
is located under the julia-examples/src/ directory. The list of available examples
follows.

	- helloworld: Fractal Hello World
	- collection: A simple example with auto-binding components
	- interceptor: A simple example to illustrate the use of interceptors
	- j2me: The Hello World example for Java ME CLDC
	- protoactive:
	    Asynchronous component operation calls with futures
	    as in ProActive <http://proactive.objectweb.org>

This example illustrates the features of Julia for the component-based
engineering of control membranes.

The four existing subpackages illustrates:
    - intropect: the introspection of a control membrane,
    - glue: the gluing of a control membrane to a content,
    - interceptor: the dynamic management of interceptors,
    - mix: the joint use of object-oriented and component-based control membrane.

To run one of the examples, type (xxx is the name of the example):
    cd julia-examples/xxx (or cd koch-examples/xxx)
	mvn -Prun.julia (or mvn -Prun.java)


5. Libraries included with Julia
--------------------------------
Julia includes the following software:
	- ASM 3.0           <http://asm.objectweb.org>
	- Fractal API 2     <http://fractal.objectweb.org>
	- Monolog 1.8       <http://monolog.objectweb.org>
	

For any question concerning Julia, please contact the Julia development team at
fractal@objectweb.org
