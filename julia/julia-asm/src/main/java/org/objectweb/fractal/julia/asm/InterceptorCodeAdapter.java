/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Label;

import java.lang.reflect.Method;

/**
 * An abstract code adapter to ease the implementation of {@link CodeGenerator}.
 * This code adapter can replace the return instructions it
 * visits with goto or jsr instructions, in order to add a post code block after
 * the original code of a method. This code adapter can also insert new local
 * variables before the original local variables (which are therefore shifted),
 * but after the actual parameters. This can be useful to use private local
 * variables in the interception code.
 */

public abstract class InterceptorCodeAdapter
  extends MethodAdapter
  implements Opcodes
{

  /**
   * Describes an empty post block.
   */

  public final static int EMPTY = 0;

  /**
   * Describes a normal post code block. A normal post code block is not
   * executed if the original code throws an exception.
   */

  public final static int NORMAL = 1;

  /**
   * Describes a finally post block. A finally post code block is always
   * executed, even if the original code throws an exception.
   */

  public final static int FINALLY = 2;

  /**
   * The method for which this code adapter is used.
   */

  protected Method m;

  /**
   * Size of the formal parameters of the method for which this code adapter is
   * used. This size is also the index of the first inserted local variable.
   */

  protected int nbFormals;

  /**
   * The inserted local variable used to store the result value.
   */

  protected int returnLocal;

  /**
   * Total size of the inserted local variables. This total includes the local
   * variable used to store the result value.
   */

  private int newLocals;

  /**
   * Type of the post code block that will be added after the original code.
   */

  private int postBlockType;

  /**
   * Beginning of the post code block added to the original code.
   */

  protected Label postBlockLabel;

  // -------------------------------------------------------------------------
  // Constructor
  // -------------------------------------------------------------------------

  /**
   * Constructs a new {@link InterceptorCodeAdapter}.
   *
   * @param cv the code visitor to which this adapter must delegate calls.
   * @param m the method for which this code adapter is used.
   * @param newLocals the number of new local variables that must be inserted
   *      in the original method code. This number does not include the local
   *      variable used to store the result value, which is automatically
   *      inserted. The inserted local variables are those between the {@link
   *      #nbFormals nbFormals} index, inclusive, and the {@link #nbFormals
   *      nbFormals} + <tt>newLocals</tt> index, exclusive.
   * @param postBlockType the type of the post code block that will be added
   *      after the original code, i.e., either {@link #EMPTY EMPTY}, {@link
   *      #NORMAL NORMAL} or {@link #FINALLY FINALLY}.
   */

  public InterceptorCodeAdapter (
    final MethodVisitor cv,
    final Method m,
    final int newLocals,
    final int postBlockType)
  {
    super(cv);
    // computes the size of the formal parameters
    int parameterSize = 1;
    Class[] formals = m.getParameterTypes();
    for (int i = 0; i < formals.length; ++i) {
      if (formals[i] == Long.TYPE || formals[i] == Double.TYPE) {
        parameterSize += 2;
      } else {
        parameterSize += 1;
      }
    }
    // computes the size of the return type
    int returnSize;
    Class result = m.getReturnType();
    if (result == Long.TYPE || result == Double.TYPE) {
      returnSize = 2;
    } else if (result != Void.TYPE) {
      returnSize = 1;
    } else {
      returnSize = 0;
    }
    // initializes fields
    this.m = m;
    this.nbFormals = parameterSize;
    this.returnLocal = parameterSize + newLocals;
    this.newLocals = newLocals + returnSize;
    this.postBlockType = postBlockType;
    this.postBlockLabel = new Label();
  }

  // -------------------------------------------------------------------------
  // Overriden CodeAdapter methods
  // -------------------------------------------------------------------------

  /**
   * Replaces returns with instructions to jump to the beginning of the post
   * code block. More precisely, if the post code bock is empty, the returns
   * are not replaced. If the post code block is normal they are replaced by
   * sequences of the following form:
   * <pre>
   * xSTORE returnLocal
   * GOTO postCodeLabel
   * </pre>
   * If the post code block is a finally block, they are replaced by sequences
   * of the following form:
   * <pre>
   * xSTORE returnLocal
   * JSR postCodeLabel
   * xLOAD returnLocal
   * xRETURN
   * </pre>
   *
   * @param opcode the opcode of the visited instruction.
   */

  public void visitInsn (final int opcode) {
    if (postBlockType == EMPTY) {
      mv.visitInsn(opcode);
      return;
    }
    switch (opcode) {
      case IRETURN:
      case LRETURN:
      case FRETURN:
      case DRETURN:
      case ARETURN:
        int opcOffset = opcode - IRETURN;
        mv.visitVarInsn(ISTORE + opcOffset, returnLocal);
        if (postBlockType == NORMAL) {
          mv.visitJumpInsn(GOTO, postBlockLabel);
        } else {
          mv.visitJumpInsn(JSR, postBlockLabel);
          mv.visitVarInsn(ILOAD + opcOffset, returnLocal);
          mv.visitInsn(opcode);
        }
        break;
      case RETURN:
        if (postBlockType == NORMAL) {
          mv.visitJumpInsn(GOTO, postBlockLabel);
        } else {
          mv.visitJumpInsn(JSR, postBlockLabel);
          mv.visitInsn(RETURN);
        }
        break;
      default:
        mv.visitInsn(opcode);
    }
  }

  /**
   * Shifts the original local variables to make room for the inserted ones.
   *
   * @param opcode the opcode of the visited instruction.
   * @param var a local variable index.
   */

  public void visitVarInsn (final int opcode, final int var) {
    mv.visitVarInsn(opcode, var >= nbFormals ? var + newLocals : var);
  }

  /**
   * Shifts the original local variables to make room for the inserted ones.
   *
   * @param var a local variable index.
   * @param increment an increment value.
   */

  public void visitIincInsn (final int var, final int increment) {
    mv.visitIincInsn(var >= nbFormals ? var + newLocals : var, increment);
  }

  // -------------------------------------------------------------------------
  // Utility methods
  // -------------------------------------------------------------------------

  /**
   * Generates the code to return the result value stored in the {@link
   * #returnLocal returnLocal} local variable. More precisely, generates
   * a sequence of the following form:
   * <pre>
   * xLOAD returnLocal
   * xRETURN
   * </pre>
   */

  protected void generateReturnCode () {
    Class c = m.getReturnType();
    if (c.isPrimitive()) {
      if (c == Void.TYPE) {
        mv.visitInsn(RETURN);
      } else if (c == Long.TYPE) {
        mv.visitVarInsn(LLOAD, returnLocal);
        mv.visitInsn(LRETURN);
      } else if (c == Float.TYPE) {
        mv.visitVarInsn(FLOAD, returnLocal);
        mv.visitInsn(FRETURN);
      } else if (c == Double.TYPE) {
        mv.visitVarInsn(DLOAD, returnLocal);
        mv.visitInsn(DRETURN);
      } else {
        mv.visitVarInsn(ILOAD, returnLocal);
        mv.visitInsn(IRETURN);
      }
    } else {
      mv.visitVarInsn(ALOAD, returnLocal);
      mv.visitInsn(ARETURN);
    }
  }
}
