/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;

import org.objectweb.fractal.julia.control.binding.ContentBindingController;
import org.objectweb.fractal.julia.control.lifecycle.ContentLifeCycleController;
import org.objectweb.fractal.julia.loader.Generated;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.julia.loader.Initializable;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.Attribute;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class generator to merge several classes into a single one. This class
 * generator copies all the non static fields and all the non static methods of
 * the given classes (and of their super class, super super class, and so on)
 * into a new class, with the following exceptions:
 * <ul>
 * <li>fields whose name begins with "weaveable" are replaced by <tt>this</tt>.
 * One of the merged classes must implement the corresponding interface,
 * otherwise an exception is thrown.</li>
 * <li>fields whose name begins with "weaveableOpt" are replaced by
 * <tt>this</tt> if the corresponding interface is implemented by one of the
 * merged classes, or by <tt>null</tt> otherwise.</li>
 * <li>the method of the {@link org.objectweb.fractal.julia.Controller}
 * interface are merged into a single method.</li>
 * </ul>
 * <b>Notes</b>:
 * <ul>
 * <li>since static fields and methods are not copied, and since the generated
 * class will generally not be in the same package than the original classes,
 * all the static fields and methods used by the non static methods must be
 * public (but those that are used only by static methods may be private).</li>
 * <li>the constructors are not copied (this limitation will perhaps be removed
 * in future versions).</li>
 * </ul>
 * Except for the "weaveable" fields, and for the {@link
 * org.objectweb.fractal.julia.Controller} method, the fields and methods of the
 * classes to be merged should all be distinct: otherwise the generated class
 * will not be valid, because of duplicated field or method declarations.
 * <p>
 * As an example, the merging of the B and C classes below:
 * <pre>
 * public class A {
 *   public int a;
 *   public void m () { ... }
 * }
 *
 * public class B extends A implements I {
 *   private int b;
 *   private J weaveableJ;
 *   private K weaveableOptK;
 *   public void m () { super.m(); ... }
 *   public void n () { weaveableJ.o(weaveableOptK); }
 * }
 *
 * public class C implement J {
 *   public void o (K k) { p(); }
 *   public static p () { q(); }
 *   private static q () { ... }
 * }
 * </pre>
 * is the following class:
 * <pre>
 * public class <i>XYZ</i> implements I, J, Generated {
 *   public int a;                  // copied from B's super class
 *   private int b;                 // copied from B
 *   // weaveableJ                  // NOT copied
 *   // weaveableOptK               // NOT copied
 *   private void m$0 () { ... }           // copied from B's super class
 *   public void m () { m$0(); ... }       // copied from B
 *   public void n () { <b>o(null);</b> }  // copied from B
 *   public void o (K k) { <b>C.</b>p(); } // copied from C
 *   // p                           // NOT copied (static)
 *   // q                           // NOT copied (static)
 *   public String getFcGeneratorParameters () {
 *     return "(...MergeClassGenerator java.lang.Object B C)";
 *   }
 * }
 * </pre>
 */

public class MergeClassGenerator implements ClassGenerator, Opcodes {

  /**
   * The loader to be used to load or generate auxiliary classes, if needed.
   */

  public Loader loader;

  /**
   * The class loader to be used to load auxiliary classes, if needed.
   */
  
  public ClassLoader classLoader;
  
  /**
   * The parameters used to generate the merged class.
   */

  public String parameters;

  /**
   * Internal name of the merged class generated by this generator.
   */

  String mergedClassName;

  /**
   * The interfaces implemented by the classes to be merged. This list is a
   * list of Class object.
   */

  List interfaces;

  /**
   * Index of the class that is currently being merged into the merged class.
   */

  int currentClassIndex;

  /**
   * Internal name of the class that is currently being merged into the merged
   * class.
   */

  String currentClassName;

  /**
   * The class that is currently being merged into the merged class.
   */

  Class currentClass;

  /**
   * The super classes of the class that is currently being merged into the
   * merged class.
   */

  List inheritedClasses;

  /**
   * A map associating a counter to each method name and descriptor. These
   * Integer counters are used to rename overriden methods with "$x" suffixes.
   * Each counter corresponds to the number of time the corresponding method
   * is overriden by the sub classes of its class, minus one.
   */

  Map counters;

  /**
   * The code visitor used to generate the merged "initialize" method.
   */

  MethodVisitor inicv;

  /**
   * The code visitor used to generate the merged "initFcController" method.
   */

  MethodVisitor icv;

  /**
   * Generates a class by merging the given classes.
   *
   * @param name the name of the class to be generated.
   * @param args a tree of the form "(objectDescriptor superClassName
   *      className1 ... classNameN)", where className1 ... classNameN are the
   *      names of the classes to be merged, and superClassName is the name of
   *      the super class to be used for the merged class.
   * @param loader the loader to be used to load or generate auxiliary classes,
   *      if needed.
   * @param classLoader the class loader to be used to load auxiliary classes,
   *      if needed.
   * @return a class constructed by merging the given classes.
   * @throws ClassGenerationException if a problem occurs during the generation
   *      of the class.
   */

  public byte[] generateClass (
    final String name,
    final Tree args,
    final Loader loader,
    final ClassLoader classLoader) throws ClassGenerationException
  {
    this.loader = loader;
    this.classLoader = classLoader;
    String superClassName = args.getSubTree(1).toString().replace('.', '/');
    mergedClassName = name.replace('.', '/');

    Class[] classes = new Class[args.getSize() - 2];
    interfaces = new ArrayList();
    interfaces.add(Generated.class);
    interfaces.add(Initializable.class);
    for (int i = 0; i < classes.length; ++i) {
      try {
        classes[i] = loader.loadClass(args.getSubTree(i + 2), classLoader);
      } catch (ClassNotFoundException e) {
        throw new ClassGenerationException(
          e,
          args.toString(),
          "Cannot load the '" + args.getSubTree(i + 2) + "' class");
      }
      Class c = classes[i];
      while (c != Object.class) {
        Class[] citfs = c.getInterfaces();
        for (int j = 0; j < citfs.length; ++j) {
          if (!interfaces.contains(citfs[j])) {
            interfaces.add(citfs[j]);
          }
        }
        c = c.getSuperclass();
      }
    }
    // adds the ContentLifeCycleController interface if the super class
    // implements LifeCycleController (see ContentLifeCycleController);
    // likewise for ContentBindingController
    Class superClass;
    try {
      superClass = loader.loadClass(args.getSubTree(1), classLoader);
    } catch (ClassNotFoundException e) {
      throw new ClassGenerationException(
        e,
        args.toString(),
        "Cannot load the '" + args.getSubTree(1) + "' class");
    }
    if (LifeCycleController.class.isAssignableFrom(superClass)) {
      interfaces.add(ContentLifeCycleController.class);
    }
    if (BindingController.class.isAssignableFrom(superClass)) {
      interfaces.add(ContentBindingController.class);
    }

    // creates an empty class
    ClassWriter cw = new ClassWriter(0);
    String[] itfs = new String[interfaces.size()];
    for (int i = 0; i < itfs.length; ++i) {
      itfs[i] = Type.getInternalName((Class)interfaces.get(i));
    }
    cw.visit(ClassVersionHelper.getClassVersion(), ACC_PUBLIC, mergedClassName, 
        null,  superClassName, itfs );
    cw.visitSource("MERGED", null); 

    // generates the default constructor
    MethodVisitor mw = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
    mw.visitCode();
    mw.visitVarInsn(ALOAD, 0);
    mw.visitMethodInsn(INVOKESPECIAL, superClassName, "<init>", "()V");
    mw.visitInsn(RETURN);
    mw.visitMaxs(1, 1);
    mw.visitEnd();
    // generates the 'getFcGeneratorParameters' method
    parameters = args.toString();
    String mName = "getFcGeneratorParameters";
    String mDesc = "()Ljava/lang/String;";
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, mName, mDesc, null, null);
    mv.visitCode();
    mv.visitLdcInsn(parameters);
    mv.visitInsn(ARETURN);
    mv.visitMaxs(1, 1);
    mv.visitEnd();
    // generates the 'initialize' method
    inicv = cw.visitMethod(
      ACC_PUBLIC,
      "initialize",
      "(Lorg/objectweb/fractal/julia/loader/Tree;)V",
      null,
      null);
    inicv.visitCode();
    // generates the 'initFcController' method
    icv = cw.visitMethod(
      ACC_PUBLIC,
      "initFcController",
      "(Lorg/objectweb/fractal/julia/InitializationContext;)V",
      null,
      null);
    icv.visitCode();
    // merges all the classes
    for (int i = 0; i < classes.length; ++i) {
      inheritedClasses = new ArrayList();
      counters = new HashMap();

      Class c = classes[i];
      while (c != Object.class) {
        inheritedClasses.add(Type.getInternalName(c));
        c = c.getSuperclass();
      }

      c = classes[i];
      while (c != Object.class) {
        currentClass = c;
        currentClassIndex = i;
        currentClassName = Type.getInternalName(c);
        MergeClassAdapter mca = new MergeClassAdapter(cw);
        try {
          getClassReader(c).accept(mca, 0);
        } catch (IOException e) {
          throw new ClassGenerationException(
            e, args.toString(), "Cannot read the '" + c.getName() + "' class");
        } catch (VisitException e) {
          throw e.getException();
        }
        updateCounters(c);
        c = c.getSuperclass();
      }
    }

    inicv.visitInsn(RETURN);
    inicv.visitMaxs(3, 2);
    inicv.visitEnd();
    icv.visitInsn(RETURN);
    icv.visitMaxs(2, 2);
    icv.visitEnd();
    // generates the ContentLifeCycleController interface if the super class
    // implements LifeCycleController (see ContentLifeCycleController)
    if (LifeCycleController.class.isAssignableFrom(superClass)) {
      // generates the 'startFcContent' method
      mName = "startFcContent";
      mv = cw.visitMethod(ACC_PUBLIC, mName, "()V", null, null);
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitMethodInsn(INVOKESPECIAL, superClassName, "startFc", "()V");
      mv.visitInsn(RETURN);
      mv.visitMaxs(1, 1);
      // generates the 'stopFcContent' method
      mName = "stopFcContent";
      mv = cw.visitMethod(ACC_PUBLIC, mName, "()V", null, null);
      mv.visitVarInsn(ALOAD, 0);
      mv.visitMethodInsn(INVOKESPECIAL, superClassName, "stopFc", "()V");
      mv.visitInsn(RETURN);
      mv.visitMaxs(1, 1);
      mv.visitCode();
    }

    // generates the ContentBindingController interface if the super class
    // implements BindingController (see ContentBindingController)
    if (BindingController.class.isAssignableFrom(superClass)) {
      // generates the 'listFcContent' method
      mName = "listFcContent";
      mv = cw.visitMethod(
        ACC_PUBLIC, mName, "()[Ljava/lang/String;", null, null);
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitMethodInsn(
        INVOKESPECIAL, superClassName, "listFc", "()[Ljava/lang/String;");
      mv.visitInsn(ARETURN);
      mv.visitMaxs(1, 1);
      mv.visitEnd();
      // generates the 'lookupFcContent' method
      mName = "lookupFcContent";
      mv = cw.visitMethod(
        ACC_PUBLIC, mName, "(Ljava/lang/String;)Ljava/lang/Object;", null, null);
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, 1);
      mv.visitMethodInsn(
        INVOKESPECIAL,
        superClassName,
        "lookupFc",
        "(Ljava/lang/String;)Ljava/lang/Object;");
      mv.visitInsn(ARETURN);
      mv.visitMaxs(2, 2);
      mv.visitEnd();
      // generates the 'bindFcContent' method
      mName = "bindFcContent";
      mv = cw.visitMethod(
        ACC_PUBLIC, mName, "(Ljava/lang/String;Ljava/lang/Object;)V", null, null);
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, 1);
      mv.visitVarInsn(ALOAD, 2);
      mv.visitMethodInsn(
        INVOKESPECIAL,
        superClassName,
        "bindFc",
        "(Ljava/lang/String;Ljava/lang/Object;)V");
      mv.visitInsn(RETURN);
      mv.visitMaxs(3, 3);
      mv.visitEnd();
      // generates the 'unbindFcContent' method
      mName = "unbindFcContent";
      mv = cw.visitMethod(
        ACC_PUBLIC, mName, "(Ljava/lang/String;)V", null, null);
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, 1);
      mv.visitMethodInsn(
        INVOKESPECIAL, superClassName, "unbindFc", "(Ljava/lang/String;)V");
      mv.visitInsn(RETURN);
      mv.visitMaxs(2, 2);
      mv.visitCode();
    }

    // returns the merged class
    return cw.toByteArray();
  }

  /**
   * Returns a {@link ClassReader} to analyze the given class.
   *
   * @param c a class.
   * @return a {@link ClassReader} to analyze the given class.
   * @throws IOException if the bytecode of the class cannot be found.
   */

  ClassReader getClassReader (final Class c) throws IOException {
    try {
      return new ClassReader(c.getName());
    } catch (IOException e) {
      // an exception can occur if c is a dynamically generated class;
      // indeed, in this case, it can not be found by the system class loader.
      String s = Type.getInternalName(c) + ".class";
      return new ClassReader(c.getClassLoader().getResourceAsStream(s));
    }
  }

  /**
   * Returns the counter associated to the given method.
   *
   * @param name the method's name.
   * @param desc the method's descriptor.
   * @return the counter associated to the given method.
   */

  int getCounter (final String name, final String desc) {
    Integer value = (Integer)counters.get(name + desc);
    return value == null ? -1 : value.intValue();
  }

  /**
   * Updates the {@link #counters counters} map with the given class. This
   * method increments the counter associated to each non static method of the
   * given class.
   *
   * @param c a class.
   */

  void updateCounters (final Class c) {
    Method[] meths = c.getDeclaredMethods();
    for (int i = 0; i < meths.length; ++i) {
      Method meth = meths[i];
      if (!Modifier.isStatic(meth.getModifiers())) {
        String key = meth.getName() + Type.getMethodDescriptor(meth);
        Integer value = (Integer)counters.get(key);
        int count = value == null ? 0 : value.intValue() + 1;
        counters.put(key, new Integer(count));
      }
    }
  }

  /**
   * Tests if the given method is declared in the given class.
   *
   * @param m the name and descriptor of a method.
   * @param c a class.
   * @return <tt>true</tt> if the <tt>c</tt> declares a method with same name,
   *      the same formal parameter types, and the same return type than
   *      <tt>m</tt>.
   */

  static boolean declares (final String m, final Class c) {
    Method[] meths = c.getDeclaredMethods();
    for (int i = 0; i < meths.length; ++i) {
      if (m.equals(meths[i].getName() + Type.getMethodDescriptor(meths[i]))) {
        return true;
      }
    }
    return false;
  }

  /**
   * A class adapter to remove some fields and methods in a class to be merged.
   */

 class MergeClassAdapter extends ClassAdapter implements Opcodes {

    /**
     * Constructs a new {@link MergeClassGenerator.MergeClassAdapter} object.
     *
     * @param cv the class vistor to which this adapter must delegate calls.
     */

    public MergeClassAdapter (final ClassVisitor cv) {
      super(cv);
    }

    public void visit (
      final int version,
      final int access,
      final String name,
      final String signature,
      final String superName,
      final String[] interfaces)
    {
      // does nothing
    }

    public FieldVisitor visitField (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final Object value)
    {
	    FieldVisitor fv = null;
      if ((access & ACC_STATIC) == 0) {
        if (!name.startsWith("weaveable")) {
          fv = super.visitField(access, name, desc, signature, value);    
        }
      }
      return fv;
    }

    public MethodVisitor visitMethod (
      final int access,
      final String name,
      final String desc,
      final String signature,
      final String[] exceptions)
    {
      if ((access & ACC_STATIC) != 0 ||
          (access & ACC_ABSTRACT) != 0 ||
          name.equals("<init>") ||
          name.startsWith("getFcGeneratorParameters"))
      {
        return null;
      }
      MethodVisitor mv;
      int count = getCounter(name, desc);
      if (count == -1) {
        if (name.equals("initFcController")) {
          icv.visitVarInsn(ALOAD, 0);
          icv.visitVarInsn(ALOAD, 1);
          icv.visitMethodInsn(
            INVOKESPECIAL,
            mergedClassName,
            "$" + currentClassIndex + "$" + name,
            "(Lorg/objectweb/fractal/julia/InitializationContext;)V");
        }
        if (name.equals("initialize")
            && desc.equals("(Lorg/objectweb/fractal/julia/loader/Tree;)V"))
        {
          inicv.visitVarInsn(ALOAD, 0);
          inicv.visitVarInsn(ALOAD, 1);
          inicv.visitIntInsn(SIPUSH, currentClassIndex);
          inicv.visitMethodInsn(
            INVOKEVIRTUAL,
            "org/objectweb/fractal/julia/loader/Tree",
            "getSubTree",
            "(I)Lorg/objectweb/fractal/julia/loader/Tree;");
          inicv.visitMethodInsn(
            INVOKESPECIAL,
            mergedClassName,
            "$" + currentClassIndex + "$" + name,
            desc);
        }

        if (name.startsWith("initFcController")
            || (name.startsWith("initialize")
            && desc.equals("(Lorg/objectweb/fractal/julia/loader/Tree;)V")))
        {
          mv = cv.visitMethod(
            ACC_PRIVATE,
            "$" + currentClassIndex + "$" + name,
            desc,
            signature, 
            exceptions);
        } else {
          mv = cv.visitMethod(access, name, desc, signature, exceptions);
        }
      } else {
        int newAccess = access & ~(ACC_PUBLIC | ACC_PROTECTED) | ACC_PRIVATE;
        String newName = name + "$" + count;
        if (name.startsWith("initFcController")
            || (name.startsWith("initialize")
            && desc.equals("(Lorg/objectweb/fractal/julia/loader/Tree;)V")))
        {
          mv = cv.visitMethod(
            ACC_PRIVATE,
            "$" + currentClassIndex + "$" + newName,
            desc,
            signature, 
            exceptions);
        } else {
          mv = cv.visitMethod(newAccess, newName, desc, signature, exceptions);
        }
      }
      return new MergeMethodAdapter(mv);
    }
  }

  /**
   * A code adapter to replace access to removed fields in a class to be merged.
   */

  class MergeMethodAdapter extends MethodAdapter implements Opcodes {

    /**
     * Constructs a new {@link MergeClassGenerator.MergeCodeAdapter} object.
     *
     * @param cv the code vistor to which this adapter must delegate calls.
     */

	  public MergeMethodAdapter (final MethodVisitor mv) {
      super(mv);
    }

    public void visitFieldInsn (
      final int opcode,
      final String owner,
      final String name,
      final String desc)
    {
      if (opcode == GETFIELD) {
        if (inheritedClasses.contains(owner)) {
          boolean remove = false;
          boolean replaceNull = false;
          if (name.startsWith("weaveable")) {
            remove = true;
            String s = desc.substring(1, desc.length() - 1).replace('/', '.');
            Class c;
            try {
              c = loader.loadClass(s, classLoader);
            } catch (ClassNotFoundException e) {
              throw new VisitException(new ClassGenerationException(
                e,
                parameters,
                "Cannot load the '" + s +
                "' interface required by one the classes to be merged"));
            }
            replaceNull = true;
            for (int i = 0; i < interfaces.size(); ++i) {
              if (c.isAssignableFrom((Class)interfaces.get(i))) {
                replaceNull = false;
                break;
              }
            }
            if (!name.startsWith("weaveableOpt") && replaceNull) {
              throw new VisitException(new ClassGenerationException(
                null,
                parameters,
                "The required interface '" + name +
                "' is not provided by any of the classes to be merged"));
            }
          }
          if (remove) {
            if (replaceNull) {
              visitInsn(POP);
              visitInsn(ACONST_NULL);
            }
          } else {
            super.visitFieldInsn(opcode, mergedClassName, name, desc);
          }
        } else {
          super.visitFieldInsn(opcode, owner, name, desc);
        }
      } else if (opcode == PUTFIELD) {
        if (inheritedClasses.contains(owner)) {
          if (name.startsWith("weaveable")) {
            visitInsn(POP2);
          } else {
            super.visitFieldInsn(opcode, mergedClassName, name, desc);
          }
        } else {
          super.visitFieldInsn(opcode, owner, name, desc);
        }
      } else {
        super.visitFieldInsn(opcode, owner, name, desc);
      }
    }

    public void visitMethodInsn (
      final int opcode,
      final String owner,
      final String name,
      final String desc)
    {
      if (opcode != INVOKESTATIC && inheritedClasses.contains(owner)) {
        // call to a method of the current class or of one of its super classes
        if (opcode == INVOKESPECIAL && !owner.equals(currentClassName)) {
          // call to an overriden method
          String superName;
          if (declares(name + desc, currentClass)) {
            superName = name + "$" + (getCounter(name, desc) + 1);
          } else if (getCounter(name, desc) >= 0) {
            superName = name + "$" + getCounter(name, desc);
          } else {
            superName = name;
          }
          super.visitMethodInsn(opcode, mergedClassName, superName, desc);
        } else {
          if (name.startsWith("initFcController")
              || (name.startsWith("initialize")
              && desc.equals("(Lorg/objectweb/fractal/julia/loader/Tree;)V")))
          {
            super.visitMethodInsn(
              opcode,
              mergedClassName,
              "$" + currentClassIndex + "$" + name,
              desc);
          } else {
            // call to a private method
            super.visitMethodInsn(opcode, mergedClassName, name, desc);
          }
        }
      } else {
        super.visitMethodInsn(opcode, owner, name, desc);
      }
    }
  }
}
