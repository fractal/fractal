/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.julia.loader.Generated;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An abstract class generator based on the {@link org.objectweb.asm} package.
 * This class generator takes as parameter the name of a super class and a list
 * of interface names, and generates a sub class of the given super class that
 * implements all the given interfaces.
 * <p>
 * <b>Note</b>: since this class stores information about the class that is
 * currently being generated in its fields, it should not be used by multiple
 * threads simultaneously.
 */

public abstract class AbstractClassGenerator implements
  ClassGenerator,
  Opcodes
{

  /**
   * The class that is being generated.
   */

  public ClassWriter cw;

  /**
   * The internal name of the class that is being generated.
   */

  public String name;

  /**
   * The internal name of the super class of the class that is being generated.
   */

  public String superClass;

  /**
   * The interfaces to be implemented by the class that is being generated.
   * More precisely, this list of String is the list of the internal names of
   * these interfaces.
   */

  public List interfaces;

  /**
   * The parameters used to generate the class that is being generated.
   */

  public String parameters;

  /**
   * The loader to be used to load or generate auxiliary classes, if needed.
   */

  public Loader loader;

  /**
   * The class loader to be used to load auxiliary classes, if needed.
   */
  
  public ClassLoader classLoader;
  
  /**
   * Generates a sub class of the given class that implements the given
   * interfaces. This method initializes the fields of this class, and then
   * calls {@link #parseArgs parseArgs}, {@link #generateHeader generateHeader},
   * {@link #generateConstructor generateConstructor}, {@link
   * #generateDefaultMethods generateDefaultMethods} and {@link
   * #generateInterfaceMethods generateInterfaceMethods}, in this order, in
   * order to generate the specified class.
   * 
   * @param name the name of the class that must be generated.
   * @param args the parameters that described how the class must be generated.
   * @param loader the loader to be used to load auxiliary classes.
   * @param classLoader the class loader to be used to load auxiliary classes.
   * @return the generated class, as a byte array.
   * @throws ClassGenerationException if the class cannot be generated. 
   */

  public byte[] generateClass (
    final String name,
    final Tree args,
    final Loader loader,
    final ClassLoader classLoader) throws ClassGenerationException
  {
    this.name = name.replace('.', '/');
    this.parameters = args.toString();
    this.loader = loader;
    this.classLoader = classLoader;
    this.cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
    parseArgs(args);
    generateHeader();
    generateConstructor();
    generateDefaultMethods();
    generateInterfaceMethods();
    return cw.toByteArray();
  }

  /**
   * Initializes this class generator with the given arguments. The default
   * implementation of this method requires arguments of the form
   * "(objectDescriptor superClassName (itfName1 ... itfNameN) ...)", where
   * superClassName is the name of the super class of the class to be generated,
   * and itfName1 ... itfNameN the names of the interfaces it must implement.
   *
   * @param args the descriptor of the class to be generated.
   */

  protected void parseArgs (final Tree args) {
    superClass = args.getSubTree(1).toString().replace('.', '/');
    interfaces = new ArrayList(Arrays.asList(args.getSubTree(2).getSubTrees()));
    for (int i = 0; i < interfaces.size(); ++i) {
      interfaces.set(i, interfaces.get(i).toString().replace('.', '/'));
    }
  }

  /**
   * Generates the header of the class to be generated.
   *
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateHeader () throws ClassGenerationException {
    int access = ACC_PUBLIC;
    List itfList = getImplementedInterfaces();
    String[] itfs = (String[])itfList.toArray(new String[itfList.size()]);
    cw.visit(ClassVersionHelper.getClassVersion(), access, name, null, 
        superClass, itfs);
    cw.visitSource(getSource(), null);
  }

  /**
   * Returns the source of the class to be generated. The default implementation
   * of this method returns "GENERATED".
   *
   * @return the source of the class to be generated.
   */

  protected String getSource () {
    return "GENERATED";
  }

  /**
   * Returns all the interfaces implemented by the class to be generated. This
   * list contains the {@link #interfaces interfaces} interfaces, and some
   * default interfaces such as {@link
   * org.objectweb.fractal.julia.loader.Generated}. This method is used by
   * {@link #generateHeader generateHeader}.
   *
   * @return the names of all the interfaces implemented by the generated class.
   * @throws ClassGenerationException if a problem occurs.
   */

  protected List getImplementedInterfaces () throws ClassGenerationException {
    List itfs = new ArrayList(interfaces);
    itfs.add(Type.getInternalName(Generated.class));
    return itfs;
  }

  /**
   * Generates the constructor of the class to be generated. The default
   * implementation of this method generates an empty public constructor without
   * arguments. The super class should also have such a constructor.
   *
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateConstructor () throws ClassGenerationException {
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
    // generates the bytecode corresponding to 'super(...);'
    mv.visitCode();
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKESPECIAL, superClass, "<init>", "()V");
    mv.visitInsn(RETURN);
    mv.visitMaxs(1, 1);
    mv.visitEnd();
  }

  /**
   * Generates the default methods of the class to be generated. The default
   * implementation of this method generates the {@link
   * org.objectweb.fractal.julia.loader.Generated#getFcGeneratorParameters
   * getFcGeneratorParameters} method.
   *
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateDefaultMethods () throws ClassGenerationException {
    String name = "getFcGeneratorParameters";
    String desc = "()Ljava/lang/String;";
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, name, desc, null, null);
    mv.visitCode();
    mv.visitLdcInsn(parameters);
    mv.visitInsn(ARETURN);
    mv.visitMaxs(1, 1);
    mv.visitEnd();
  }

  /**
   * Generates the methods of the interfaces to be implemented by the class to
   * be generated. This method calls {@link #generateMethod generateMethod} for
   * each method of each interface to be implemented ({@link #generateMethod
   * generateMethod} is called only once per method, even for those that are
   * specified in several interfaces).
   *
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateInterfaceMethods () throws ClassGenerationException {
    List generated = new ArrayList();
    for (int i = 0; i < interfaces.size(); ++i) {
      String s = ((String)interfaces.get(i)).replace('/', '.');
      Class itf;
      try {
        itf = loader.loadClass(s, classLoader);
      } catch (ClassNotFoundException e) {
        throw new ClassGenerationException(
          e, parameters, "Cannot load the '" + s + "' interface");
      }
      Method[] meths = itf.getMethods();
      for (int j = 0; j < meths.length; ++j) {
        Method meth = meths[j];
        String desc = meth.getName() + Type.getMethodDescriptor(meth);
        if (!generated.contains(desc)) {
          generateMethod(meth);
          generated.add(desc);
        }
      }
    }
  }

  /**
   * Generates a method of an interface to be implemented by the class to be
   * generated.
   *
   * @param m the method to be generated.
   * @throws ClassGenerationException if a problem occurs.
   */

  protected abstract void generateMethod (Method m)
    throws ClassGenerationException;

  /**
   * Returns the offset which must be added to some opcodes to get an opcode of
   * the given type. More precisely, returns the offset which must be added to
   * an opc_iXXXX opcode to get the opc_YXXXX opcode corresponding to the given
   * type. For example, if the given type is double the result is 3, which
   * means that opc_dload, opc_dstore, opc_dreturn... opcodes are equal to
   * opc_iload+3, opc_istore+3, opc_ireturn+3...
   *
   * @param type a Java class representing a Java type (primitive or not).
   * @return the opcode offset of the corresponding to the given type.
   */

  public static int getOpcodeOffset (final Class type) {
    if (type == Double.TYPE) {
      return 3;
    } else if (type == Float.TYPE) {
      return 2;
    } else if (type == Long.TYPE) {
      return 1;
    } else if (type.isPrimitive()) {
      return 0;
    }
    return 4;
  }

  /**
   * Returns the size of the given type. This size is 2 for the double and long
   * types, and 1 for the other types.
   *
   * @param type a Java class representing a Java type (primitive or not).
   * @return the size of the given type.
   */

  public static int getSize (final Class type) {
    return (type == Double.TYPE || type == Long.TYPE ? 2 : 1);
  }
}
