/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

/**
 * Thrown if a problem occurs during the visit of a class or of a method. Since
 * the methods of the {@link org.objectweb.asm.ClassVisitor} and
 * {@link org.objectweb.asm.MethodVisitor} interfaces do not declare
 * exceptions, it is not possible to throw checked exceptions during a visit.
 * But it is possible to throw a {@link RuntimeException} that
 * wraps a checked exception. Hence this class.
 */

class VisitException extends RuntimeException {

  /**
   * The exception wrapped in this exception.
   */

  private final ClassGenerationException exception;

  /**
   * Constructs a new {@link VisitException} object.
   *
   * @param exception the exception to be wrapped in this exception.
   */

  public VisitException (final ClassGenerationException exception) {
    this.exception = exception;
  }

  /**
   * Returns the exception wrapped in this exception.
   *
   * @return the exception wrapped in this exception.
   */

  public ClassGenerationException getException () {
    return exception;
  }
}
