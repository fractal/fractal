/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link CodeGenerator} to generate pre and post code
 * to call an associated controller object. This abstract code generator can
 * be used to easily generate interceptors of the following form, without
 * knowing the Java bytecode instructions and ASM:
 *
 * <center>
 * <img SRC="../../../../../../figures/interceptor.gif" height=313 width=417><p>
 * <b>Figure 1: an interceptor and an associated controller object (see also
 * <a href="../../../../../overview-summary.html#4">here</a>)</b>
 * </center>
 *
 * <p>where the interceptor calls a <tt>void</tt> <i>prem</i> <tt>(String
 * s)</tt> method and a <tt>void</tt> <i>postm</i> <tt>(String s)</tt> method on
 * the controller object before and after each intercepted method. This abstract
 * code generator can be configured in many ways:
 * <ul>
 * <li>The name <i>N</i> (see the above figure) can be configured with the
 * {@link #getControllerInterfaceName getControllerInterfaceName} method.</li>
 * <li>The names of the <i>prem</i> and <i>postm</i> methods can be configured
 * with the {@link #getPreMethodName getPreMethodName} and {@link
 * #getPostMethodName getPostMethodName} methods. These methods must be defined
 * in the <i>C</i> class. They can also be declared in the <tt>T</tt>
 * interface, but this is not mandatory.</li>
 * <li>the <i>prem</i> and <i>postm</i> methods take as argument a String that
 * is computed from the description of the intercepted method by the {@link
 * #getMethodName getMethodName} method.</li>
 * <li>a context can be returned by the <i>prem</i> method and passed back to
 * the <i>postm</i> method (this can be useful to avoid mixin the start of a
 * method call with the end of another, concurrent, method call). The type of
 * this context can be configured with the {@link #getContextType
 * getContextType} method. It can be a primitive type, an object type, or void
 * if no context is required. If a context is used, then the <i>prem</i> and
 * <i>postm</i> methods defined in <i>C</i> must have the following
 * signatures (where <tt>I</tt> is the type of the context): <tt>I prem
 * (String s)</tt> and <tt>void postm (String s, I ctxt)</tt>.</li>
 * <li>the name of fractal interface to which the interceptor is associated can
 * be passed as parameter in the  <i>prem</i> and  <i>postm</i> methods. For
 * this you just need to override the {@link #needsInterfaceName
 * needsInterfaceName} method, so that it returns <tt>true</tt>. The <i>prem</i>
 * and  <i>postm</i> methods must then have an additional parameter of type
 * <tt>String</tt>: <tt>I prem (String itf, String s)</tt> and <tt>void postm
 * (String itf, String s, I ctxt)</tt>.</li>
 * <li>the target object of the intercepted method call can be passed as
 * parameter in the  <i>prem</i> and  <i>postm</i> methods. For
 * this you just need to override the {@link #needsTargetObject
 * needsTargetObject} method, so that it returns <tt>true</tt>. The <i>prem</i>
 * and  <i>postm</i> methods must then have an additional parameter of type
 * <tt>Object</tt>: <tt>I prem (Object target, String s)</tt> and <tt>void postm
 * (Object target, String s, I ctxt)</tt> (if the previous option is also
 * enabled, the parameters must be placed in the following order: <tt>I prem
 * (String itf, Object target, String s)</tt> and <tt>void postm (String itf,
 * Object target, String s, I ctxt)</tt>).</li>
 * <li>the {@link #getInterceptionType getInterceptionType} method can be
 * overriden to choose between three type of pre/post semantics. See {@link
 * AbstractCodeGenerator}.</li>
 * <li>the {@link #intercept intercept} method can be overriden to intercept
 * only some specific methods, while leaving the other unchanged.</li>
 * </ul>
 *
 * <p>The code adapters returned by the {@link #generateInterceptionCode
 * generateInterceptionCode} method (see {@link CodeGenerator})
 * transform the original methods into methods of the following form (assuming
 * a context is used):
 *
 * <p><pre>
 * <i>method-signature</i> {
 *   delegate.<i>prem</i>(<i>id</i>);
 *   // original method code
 * }
 * </pre>
 *
 * if {@link #getInterceptionType getInterceptionType} returns
 * {@link InterceptorCodeAdapter#EMPTY EMPTY},
 *
 * <p><pre>
 * <i>method-signature</i> {
 *   <i>return-type</i> result;
 *   <i>I</i> context = delegate.<i>prem</i>(<i>id</i>);
 *   // original method code, where returns are replaced with gotos
 *   delegate.<i>postm</i>(<i>id</i>, context);
 *   return result;
 * }
 * </pre>
 *
 * if {@link #getInterceptionType getInterceptionType} returns
 * {@link InterceptorCodeAdapter#NORMAL NORMAL},
 *
 * <p><pre>
 * <i>method signature</i> {
 *   <i>I</i> context = delegate.<i>prem</i>(<i>id</i>);
 *   try {
 *     // original method code
 *   } finally {
 *     delegate.<i>postm</i>(<i>id</i>, context);
 *   }
 * }
 * </pre>
 *
 * if {@link #getInterceptionType getInterceptionType} returns
 * {@link InterceptorCodeAdapter#FINALLY FINALLY}.
 *
 * <p>The {@link #generateInitCode generateInitCode} method adds a
 * <tt>delegate</tt> field to the interceptor class, and adds a code fragment of
 * the following form to the generated {@link #generateInitCode
 * generateInitCode} method (recall that <i>N</i> can be configured with the
 * {@link #getControllerInterfaceName getControllerInterfaceName} method):
 *
 * <p><pre>
 * delegate = (...)ic.getFcInterface(<i>N</i>);
 * </pre>
 *
 * Finally this code generator takes into account the Julia
 * <a href="../../../../../overview-summary.html#5.1">optimizations options</a>.
 * So, for example, if the interceptors and controller objects are merged, the
 * <tt>delegate</tt> field is not generated, and <tt>this</tt> is used instead.
 */

public abstract class SimpleCodeGenerator
  extends AbstractCodeGenerator
  implements Initializable
{

  /**
   * The arguments used to initialize this object. These arguments are the list
   * of interface names, corresponding to the list of interface types in
   * icg.interfaces. May be <tt>null</tt>.
   */

  private Tree args;

  /**
   * The interceptor class generator to which this code generator belongs.
   */

  private InterceptorClassGenerator icg;

  /**
   * The name of the 'delegate' field.
   */

  private String delegateFieldName;

  /**
   * The descriptor of the 'delegate' field.
   */

  private String delegateFieldDesc;

  /**
   * The name of the owner class of the 'delegate' field.
   */

  private String owner;

  // -------------------------------------------------------------------------
  // Implementation of the Init interface
  // -------------------------------------------------------------------------

  public void initialize (final Tree args) {
    this.args = args.getSubTree(0);
  }

  // -------------------------------------------------------------------------
  // Overriden methods
  // -------------------------------------------------------------------------

  public int init (final InterceptorClassGenerator icg) {
    this.icg = icg;
    return IN;
  }

  public void generateInitCode (final MethodVisitor cv)
    throws ClassGenerationException
  {
    // computes the internal name of the controller class
    // that contains the needed 'preMethod' method
    String owner = null;
    for (int i = 0; i < icg.controllerClasses.length; ++i) {
      Class c = icg.controllerClasses[i];
      List params = new ArrayList();
      if (needsInterfaceName()) {
        params.add(String.class);
      }
      if (needsTargetObject()) {
        params.add(Object.class);
      }
      params.add(String.class);
      Class[] paramClasses = (Class[])params.toArray(new Class[params.size()]);
      try {
        c.getMethod(getPreMethodName(), paramClasses);
        owner = c.getName();
        break;
      } catch (Exception e) {
      }
    }
    if (owner == null) {
      throw new ClassGenerationException(
        null,
        icg.args.toString(),
        "Cannot find a controller class providing a '" +
        getPreMethodName() + "' method with the good arguments");
    }
    owner = owner.replace('.', '/');

    int hashcode = getControllerInterfaceName().hashCode();
    delegateFieldName = "d" + Integer.toHexString(hashcode);
    delegateFieldDesc = "L" + owner + ";";

    FieldVisitor fv;
    fv = icg.cw.visitField(
      ACC_PRIVATE, delegateFieldName, delegateFieldDesc, null, null);
    if (fv != null) fv.visitEnd();

    cv.visitVarInsn(ALOAD, 0);
    cv.visitVarInsn(ALOAD, 1);
    cv.visitLdcInsn(getControllerInterfaceName());
    cv.visitMethodInsn(
      INVOKEVIRTUAL,
      Type.getInternalName(InitializationContext.class),
      "getInterface",
      "(" + Type.getDescriptor(String.class) + ")" +
        Type.getDescriptor(Object.class));
    cv.visitTypeInsn(CHECKCAST, owner);
    cv.visitFieldInsn(PUTFIELD, icg.name, delegateFieldName, delegateFieldDesc);
  }

  protected int getInterceptionCodeFormals (final Method m) {
    // local variable to store the context, if a context is used
    return AbstractClassGenerator.getSize(getContextType());
  }

  public void generateCloneCode (final MethodVisitor cv) {
    cv.visitVarInsn(ALOAD, 1);
    cv.visitVarInsn(ALOAD, 0);
    cv.visitFieldInsn(GETFIELD, icg.name, delegateFieldName, delegateFieldDesc);
    cv.visitFieldInsn(PUTFIELD, icg.name, delegateFieldName, delegateFieldDesc);
  }

  // -------------------------------------------------------------------------
  // Implementation of inherited abstract methods
  // -------------------------------------------------------------------------

  protected void generateInterceptionCodeBlock (
    final Method m,
    final boolean pre,
    final MethodVisitor cv,
    final int formals)
  {
    Class c = getContextType();
    String desc = Type.getDescriptor(c);

    // pushes the reference of the controller object associated to the
    // interceptor. This reference is stored in the 'delegate' field, unless
    // the controller and interceptor classes are merged (in this case the
    // reference of the controller object is the reference of the interceptor,
    // i.e., 'this')
    cv.visitVarInsn(ALOAD, 0);
    // if controller and interceptor classes are merged, the generateInitCode
    // method is not called, and the delegateFieldName is therefore null. In
    // this case we do not load the value of the 'delegate' field, which does
    // not exist.
    if (delegateFieldName != null) {
      cv.visitFieldInsn(
        GETFIELD, icg.name, delegateFieldName, delegateFieldDesc);
    }

    if (needsInterfaceName()) {
      String name = getInterfaceName(m);
      if (name == null) {
        cv.visitInsn(ACONST_NULL);
      } else {
        cv.visitLdcInsn(name);
      }
    }

    if (needsTargetObject()) {
      // generates code to push the reference of the target object
      cv.visitVarInsn(ALOAD, 0);
      cv.visitFieldInsn(
        GETFIELD, icg.name, icg.implFieldName, icg.implFieldDesc);
    }

    // generates code to push the name of the intercepted method
    cv.visitLdcInsn(getMethodName(m));

    // if we are generating the post code block, and if a context is passed from
    // the pre method to the post method, then we push this context, which has
    // been stored in the 'formals' local variable in the pre code block.
    if (!pre && c != Void.TYPE) {
      cv.visitVarInsn(
        ILOAD + AbstractClassGenerator.getOpcodeOffset(c), formals);
    }

    // we then generate the code to call the pre or post method

    // we first compute the name of the owner class of this method.
    if (owner == null) {
      if (delegateFieldDesc == null) {
        owner = icg.name;
      } else {
        owner = delegateFieldDesc.substring(1, delegateFieldDesc.length() - 1);
      }
    }

    // we then compute the name of the pre or post method itself
    String name = pre ? getPreMethodName() : getPostMethodName();

    // and we compute the signature of this method
    if (pre) {
      desc = "Ljava/lang/String;)" + desc;
    } else if (c != Void.TYPE) {
      desc = "Ljava/lang/String;" + desc + ")V";
    } else {
      desc = "Ljava/lang/String;)V";
    }
    if (needsTargetObject()) {
      desc = "Ljava/lang/Object;" + desc;
    }
    if (needsInterfaceName()) {
      desc = "Ljava/lang/String;" + desc;
    }

    // we can finally generate the code to invoke the pre or post method
    cv.visitMethodInsn(INVOKEVIRTUAL, owner, name, "(" + desc);

    // if we are generating the pre code block, and if a context is passed from
    // the pre method to the post method, then we store this context, which has
    // in the 'formals' local variable.
    if (pre && c != Void.TYPE) {
      cv.visitVarInsn(
        ISTORE + AbstractClassGenerator.getOpcodeOffset(c), formals);
    }
  }

  // -------------------------------------------------------------------------
  // Utility methods
  // -------------------------------------------------------------------------

  /**
   * Returns the name of the interface provided by the controller object to be
   * associated to the interceptor.
   *
   * @return the name of the interface provided by the controller object to be
   *      associated to the interceptor (called <i>N</i> in the above figure).
   */

  protected abstract String getControllerInterfaceName ();

  /**
   * Returns the name of the method to be called on the associated controller
   * object, before each intercepted call.
   *
   * @return the name of the method to be called on the associated controller
   *      object, before each intercepted call (called <i>prem</i> above).
   */

  protected abstract String getPreMethodName ();

  /**
   * Returns the name of the method to be called on the associated controller
   * object, after each intercepted call.
   *
   * @return the name of the method to be called on the associated controller
   *      object, after each intercepted call (called <i>postm</i> above).
   */

  protected abstract String getPostMethodName ();

  /**
   * Returns the type of the context to be passed from the "pre method" to the
   * "post method".
   *
   * @return the type of the context to be passed from the "pre method" to the
   *      "post method" (called <i>I</i> above).
   */

  protected abstract Class getContextType ();

  /**
   * Returns the String to be passed as argument to the pre and post methods,
   * for the given method.
   *
   * @param m a method object.
   * @return the String to be passed as argument to the pre and post methods,
   *     for the given method.
   */

  protected abstract String getMethodName (Method m);

  /**
   * Returns <tt>true</tt> if the target interface name must be passed as
   * argument to the pre and post methods. The default implementation of this
   * method returns <tt>false</tt>.
   *
   * @return <tt>true</tt> if the name of the fractal interface associated to
   *      the generated interceptor must be passed as argument in the pre and
   *      post methods.
   */

  protected boolean needsInterfaceName () {
    return false;
  }

  /**
   * Returns the name of the fractal interface in which the given method is
   * defined.
   *
   * @param m a method.
   * @return the name of the fractal interface in which the given method is
   *      defined.
   */

  private String getInterfaceName (Method m) {
    if (args != null) {
      String desc = m.getName()+Type.getMethodDescriptor(m);
      List itfs = icg.interfaces;
      for (int i = 0; i < itfs.size(); ++i) {
        String itf = ((String)itfs.get(i)).replace('/', '.');
        Class c;
        try {
          c = icg.loader.loadClass(itf, icg.loader);
        } catch (ClassNotFoundException e) {
          continue;
        }
        Method[] meths = c.getMethods();
        for (int j = 0; j < meths.length; ++j) {
          Method meth = meths[j];
          if (desc.equals(meth.getName()+Type.getMethodDescriptor(meth))) {
            return args.getSubTree(i).toString();
          }
        }
      }
    }
    return null;
  }

  /**
   * Returns <tt>true</tt> if the target object must be passed as argument
   * to the pre and post methods. The default implementation of this method
   * returns <tt>false</tt>.
   *
   * @return <tt>true</tt> if the target object of intercepted method calls must
   *      be passed as argument in the pre and post methods.
   */

  protected boolean needsTargetObject () {
    return false;
  }
}
