/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.api.control.AttributeController;

import org.objectweb.fractal.julia.control.attribute.CloneableAttributeController;
import org.objectweb.fractal.julia.loader.Tree;


import org.objectweb.asm.MethodVisitor; 
import org.objectweb.asm.FieldVisitor; 
import org.objectweb.asm.Label;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A class generator to generate {@link AttributeController}
 * classes. This generator produces classes that implement a given set of
 * attribute controller interfaces, i.e., interfaces with only <tt>getXXX</tt>
 * and <tt>setXXX</tt> methods. The methods that are generated to implement
 * these interfaces store and retrieve values in private fields of the generated
 * class, two for each method pair.
 * <p>
 * More precisely, the code generated for the IAttributes interface below is the
 * following:
 * <pre>
 * public interface IAttributes extends AttributeController {
 *   int getI ();
 *   void setI (int i);
 * }
 *
 * public class <i>XYZ</i>
 *   implements IAttributes, CloneableAttributeController, Generated
 * {
 *
 *   private int I;
 *   private boolean _I; // true if I has been set
 *
 *   public int getI () {
 *     return I;
 *   }
 *
 *   public void setI (int i) {
 *     I = i;
 *     _I = true;
 *   }
 *
 *   public void cloneFcAttributes (AttributeController ac) {
 *     if (_I) {
 *       ((IAttributes)ac).setI(I);
 *     }
 *   }
 *
 *   public String getFcGeneratorParameters () {
 *     return "(<i>...</i> ... (IAttributes))";
 *   }
 * }
 * </pre>
 */

public class AttributeControllerClassGenerator extends AbstractClassGenerator {

  /**
   * The set of already generated fields.
   */

  private Set fields = new HashSet();

  /**
   * The {@link MethodVisitor} used to generate the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   */

  private MethodVisitor imv;

  /**
   * A label that designates the end of the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   */

  private Label endLabel;

  /**
   * Initializes this class generator with the given arguments. This method
   * requires arguments of the form "(objectDescriptor (itfName1 ...
   * itfNameN))", where itfName1 ... itfNameN are the names of the interfaces it
   * must implement.
   *
   * @param args the descriptor of the class to be generated.
   */

  protected void parseArgs (final Tree args) {
    superClass = "java/lang/Object";
    interfaces = new ArrayList(Arrays.asList(args.getSubTree(1).getSubTrees()));
    for (int i = 0; i < interfaces.size(); ++i) {
      interfaces.set(i, interfaces.get(i).toString().replace('.', '/'));
    }
  }

  /**
   * Adds {@link CloneableAttributeController} to the list returned by the
   * overriden method.
   * 
   * @return the names of all the interfaces implemented by the generated class.
   * @throws ClassGenerationException if a problem occurs.
   */

  protected List getImplementedInterfaces () throws ClassGenerationException {
    List itfs = super.getImplementedInterfaces();
    itfs.add(Type.getInternalName(CloneableAttributeController.class));
    return itfs;
  }

  /**
   * Calls the overriden method and generates the header of the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   * 
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateDefaultMethods () throws ClassGenerationException {
    super.generateDefaultMethods();
    imv = cw.visitMethod(
      ACC_PUBLIC,
      "cloneFcAttributes",
      "(" + Type.getDescriptor(AttributeController.class) + ")V",
      null,
      null);
  }

  /**
   * Calls the overriden method and finishes the generation of the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   * 
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateInterfaceMethods () throws ClassGenerationException {
    super.generateInterfaceMethods();
    if (endLabel != null) {
      imv.visitLabel(endLabel);
    }
    imv.visitInsn(RETURN);
    imv.visitMaxs(3, 3);
  }

  /**
   * Generates a getter or setter method, and the corresponding fields if they
   * are not already generated. If the given method is a setter method, this
   * method also adds code corresponding to this attribute to the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   * 
   * @param m the method to be generated.
   * @throws ClassGenerationException if a problem occurs.
   */

  protected void generateMethod (final Method m) {
    // generates the header of the method
    String mName = m.getName();
    String mDesc = Type.getMethodDescriptor(m);        
    MethodVisitor mv = cw.visitMethod(ACC_PUBLIC, mName, mDesc, null, null); 
    mv.visitCode();
    // computes the name and type of the corresponding field
    String fieldName = mName.substring(3);
    String iFieldName = "_" + fieldName;
    Class fieldType;
    if (mName.startsWith("get")) {
      fieldType = m.getReturnType();
    } else {
      fieldType = m.getParameterTypes()[0];
    }
    String fieldDesc = Type.getDescriptor(fieldType);
    if (!fields.contains(fieldName)) {
      FieldVisitor fv;  
      fv = cw.visitField(ACC_PRIVATE, fieldName, fieldDesc, null, null);
      if (fv != null) fv.visitEnd();
      fv = cw.visitField(ACC_PRIVATE, iFieldName, "Z", null, null);
      if (fv != null) fv.visitEnd();
      fields.add(fieldName);
    }
    if (mName.startsWith("get")) {
      // generates the bytecode corresponding to 'return xxx;'
      mv.visitVarInsn(ALOAD, 0);
      mv.visitFieldInsn(GETFIELD, name, fieldName, fieldDesc);
      mv.visitInsn(IRETURN + getOpcodeOffset(fieldType));
    } else {
      // generates the bytecode corresponding to 'this.xxx = xxx;'
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ILOAD + getOpcodeOffset(fieldType), 1);
      mv.visitFieldInsn(PUTFIELD, name, fieldName, fieldDesc);
      // generates the bytecode corresponding to 'this._xxx = true;'
      mv.visitVarInsn(ALOAD, 0);
      mv.visitInsn(ICONST_1);
      mv.visitFieldInsn(PUTFIELD, name, iFieldName, "Z");
      mv.visitInsn(RETURN);
      // adds code corresponding to 'xxx' in 'copyFcAttributes'
      generateInitMethod(fieldName, fieldDesc, iFieldName, m);
    }
    mv.visitMaxs(Math.max(2, 1 + getSize(fieldType)), 1 + getSize(fieldType));
    mv.visitEnd();
  }

  /**
   * Adds the code corresponding to the given attribute to the {@link
   * CloneableAttributeController#cloneFcAttributes cloneFcAttributes} method.
   *
   * @param fieldName the name of the 'xxx' field corresponding to the
   *      attribute.
   * @param fieldDesc the descriptor of the 'xxx' field corresponding to an
   *      attribute.
   * @param iFieldName the name of the '_xxx' field corresponding to the
   *      attribute.
   * @param m the 'setXXX' setter method corresponding to the attribute.
   */

  private void generateInitMethod (
    final String fieldName,
    final String fieldDesc,
    final String iFieldName,
    final Method m)
  {
    if (endLabel != null) {
      imv.visitLabel(endLabel);
    }
    endLabel = new Label();
    String owner = Type.getInternalName(m.getDeclaringClass());
    String desc = Type.getMethodDescriptor(m);
    // generates the bytecode corresponding to 'if (_xxx) {'
    imv.visitVarInsn(ALOAD, 0);
    imv.visitFieldInsn(GETFIELD, name, iFieldName, "Z");
    imv.visitJumpInsn(IFEQ, endLabel);
    // generates the bytecode corresponding to '(XYZ)ac'
    imv.visitVarInsn(ALOAD, 1);
    imv.visitTypeInsn(CHECKCAST, owner);
    // generates the bytecode corresponding to 'setXXX(xxx)'
    imv.visitVarInsn(ALOAD, 0);
    imv.visitFieldInsn(GETFIELD, name, fieldName, fieldDesc);
    imv.visitMethodInsn(INVOKEINTERFACE, owner, m.getName(), desc);
  }
}
