/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Thrown when a problem occurs during the generation of a class. See {@link
 * ClassGenerator#generateClass generateClass}.
 */

public class ClassGenerationException extends ClassNotFoundException {

  /**
   * The exception that caused this exception. May be <tt>null</tt>.
   */

  private Throwable exception;

  /**
   * The class descriptor of the class that cannot be generated.
   */

  private String classDescriptor;

  /**
   * Constructs a new {@link ClassGenerationException} object.
   *
   * @param exception the cause of this exception. May be <tt>null</tt>.
   * @param classDescriptor the class descriptor of the class that cannot be
   *      generated.
   * @param message the name of the interface that cannot be found.
   */

  public ClassGenerationException (
    final Throwable exception,
    final String classDescriptor,
    final String message)
  {
    super("Cannot generate the " + classDescriptor + " class. " + message);
    this.exception = exception;
    this.classDescriptor = classDescriptor;
  }

  public Throwable getException () {
    return exception;
  }

  /**
   * Returns the class descriptor of the class that cannot be generated.
   *
   * @return the class descriptor of the class that cannot be generated.
   */

  public String getClassDescriptor () {
    return classDescriptor;
  }

  /**
   * Prints the stack backtrace.
   */

  public void printStackTrace () {
    if (exception != null) {
      System.err.println(this);
      exception.printStackTrace();
    } else {
      super.printStackTrace();
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print stream.
   *
   * @param s <tt>PrintStream</tt> to use for output.
   */

  public void printStackTrace (final PrintStream s) {
    if (exception != null) {
      s.println(this);
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print writer.
   *
   * @param s <tt>PrintWriter</tt> to use for output.
   */

  public void printStackTrace (final PrintWriter s) {
    if (exception != null) {
      s.write(this + "\n");
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }
}
