/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;

/**
 * The interface that all Julia's class generators must implement.
 */

public interface ClassGenerator {

  /**
   * Generates the class whose descriptor is given, with the given name. The
   * generated class must implement the {@link
   * org.objectweb.fractal.julia.loader.Generated} interface, and its {@link
   * org.objectweb.fractal.julia.loader.Generated#getFcGeneratorParameters
   * getFcGeneratorParameters} method must return <tt>args.toString()</tt>.
   *
   * @param name the name of the class to be generated.
   * @param args the descriptor of the class to be generated. This
   *      descriptor <i>must</i> be of the form "(objectDescriptor arg1 ...
   *      argN)" (see {@link
   *      Loader#loadClass(org.objectweb.fractal.julia.loader.Tree,java.lang.Object) loadClass}).
   * @param loader the loader to be used to load or generate auxiliary classes,
   *      if needed.
   * @param classLoader the class loader to be used to load auxiliary classes,
   *      if needed.
   * @return a class named <tt>name</tt> and whose content is described by the
   *      given class descriptor.
   * @throws ClassGenerationException if a problem occurs during the generation
   *      of the class.
   */

  byte[] generateClass (String name, Tree args, Loader loader, ClassLoader classLoader)
    throws ClassGenerationException;
}
