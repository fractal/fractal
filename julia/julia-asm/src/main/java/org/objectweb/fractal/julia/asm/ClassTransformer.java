package org.objectweb.fractal.julia.asm;

import org.objectweb.asm.ClassVisitor;

/**
 * A {@link ClassVisitor} that transforms the classes it visits.
 */
public interface ClassTransformer extends ClassVisitor {

  /**
   * Sets the class visitor that must be used by this transformer to generate
   * the transformed classes.
   *
   * @param cv a class visitor.
   */
  
  void setClassVisitor (ClassVisitor cv);
}
