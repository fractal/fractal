/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.asm;

import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;

import org.objectweb.asm.MethodVisitor;

import java.lang.reflect.Method;

/**
 * A {@link CodeGenerator} to generate pre and post code to trace
 * method calls. More precisely the code adapters returned by the {@link
 * #generateInterceptionCode generateInterceptionCode} method (see {@link
 * CodeGenerator}) transform the original methods into methods of
 * the following form:
 * <pre>
 * &lt;method signature&gt; {
 *   &lt;return type&gt; result;
 *   System.err.println("Entering &lt;method signature&gt;");
 *   // original method code, where returns are replaced with gotos
 *   System.err.println("Leaving &lt;method signature&gt;");
 *   return result;
 * }
 * </pre>
 */

public class TraceCodeGenerator extends AbstractCodeGenerator
  implements Initializable
{

  /**
   * The type of this code generator.
   */

  private int type = IN;

  // -------------------------------------------------------------------------
  // Implementation of the Init interface
  // -------------------------------------------------------------------------

  /**
   * Initializes this object with the given arguments.
   *
   * @param args the type of this code generator. This tree must be of the form
   *      "(in|out|inout)".
   */

  public void initialize (final Tree args) {
    String type = args.getSubTree(0).toString();
    if (type.equals("in")) {
      this.type = IN;
    } else if (type.equals("out")) {
      this.type = OUT;
    } else if (type.equals("inout")) {
      this.type = IN_OUT;
    }
  }

  // -------------------------------------------------------------------------
  // Overriden methods
  // -------------------------------------------------------------------------

  public int init (final InterceptorClassGenerator icg) {
    return type;
  }

  // -------------------------------------------------------------------------
  // Implementation of inherited abstract methods
  // -------------------------------------------------------------------------

  protected void generateInterceptionCodeBlock (
    final Method m,
    final boolean pre,
    final MethodVisitor cv,
    final int formals)
  {
    // pushes the System.err static field
    cv.visitFieldInsn(
      GETSTATIC, "java/lang/System", "err", "Ljava/io/PrintStream;");
    if (pre) {
      // pushes the "Entering ..." String constant
      cv.visitLdcInsn("Entering " + m);
    } else {
      // pushes the "Leaving ..." String constant
      cv.visitLdcInsn("Leaving " + m);
    }
    // invokes the 'println' method (defined in the PrintStream class)
    cv.visitMethodInsn(
      INVOKEVIRTUAL,
      "java/io/PrintStream",
      "println",
      "(Ljava/lang/String;)V");
  }
}
