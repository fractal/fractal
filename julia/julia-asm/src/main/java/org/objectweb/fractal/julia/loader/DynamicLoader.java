/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.loader;

import org.objectweb.fractal.julia.asm.ClassGenerationException;
import org.objectweb.fractal.julia.asm.ClassGenerator;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides an implementation of the {@link Loader} interface that can
 * generate classes on the fly.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the Java platform must provide the Java reflection API and the
 * ClassLoader class.</li>
 * </ul>
 */

public class DynamicLoader extends BasicLoader {

  // -------------------------------------------------------------------------
  // PUBLIC constructor (needed for bootstrap)
  // -------------------------------------------------------------------------

  public DynamicLoader () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  private String genLog;
  
  private String genDir;
  
  public void init (final Map context) throws Exception {
    super.init(context);
    
    if (context.get("julia.loader.gen.dir") != null) {
      genDir = (String)context.get("julia.loader.gen.dir");
    } else if (System.getProperty("julia.loader.gen.dir") != null) {
      genDir = System.getProperty("julia.loader.gen.dir");
    }

    if (context.get("julia.loader.gen.log") != null) {
      genLog = (String)context.get("julia.loader.gen.log");
    } else if (System.getProperty("julia.loader.gen.log") != null) {
      genLog = System.getProperty("julia.loader.gen.log");
    }
  }
  
  public Class loadClass (final String name, final Object loader) 
    throws ClassNotFoundException 
  {
    return PublicClassLoader.getInstance(getClassLoader(loader)).loadClass(name);
  }

  protected Class generateClass (
    final String name,
    final Tree classDescriptor,
    final Object loader) throws ClassNotFoundException
  {
    ClassLoader cl = getClassLoader(loader);
    ClassGenerator classGen = null;
    try {
      classGen = (ClassGenerator)newObject(classDescriptor.getSubTree(0), loader);
    } catch (ClassCastException e) {
      throw new ClassGenerationException(
        null,
        classDescriptor.toString(),
        "The class generator is not an instance of ClassGenerator");
    } catch (Exception e) {
      throw new ClassGenerationException(
        e, classDescriptor.toString(), "Cannot create the class generator");
    }
    byte[] b = classGen.generateClass(name, classDescriptor, this, cl);
    if (genLog != null) {
      String line = classDescriptor.toString();
      if (genLog.equals("out")) {
        System.out.println(line);
      } else if (genLog.equals("err")) {
        System.err.println(line);
      } else {
        try {
          FileWriter fw = new FileWriter(genLog, true);
          fw.write(line);
          fw.write('\n');
          fw.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    if (genDir != null) {
      try {
        String pkg;
        String cls;
        int dot = name.lastIndexOf('.');
        if (dot > 0) {
          pkg = name.substring(0, dot).replace('.', File.separatorChar);
          cls = name.substring(dot + 1);
        } else {
          pkg = "";
          cls = name;
        }
        File dirFile = new File(new File(genDir), pkg);
        if (!dirFile.exists()) {
          dirFile.mkdirs();
        }
        File classFile = new File(dirFile, cls + ".class");
        BufferedOutputStream bos =
          new BufferedOutputStream(new FileOutputStream(classFile));
        bos.write(b);
        bos.close();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return PublicClassLoader.getInstance(cl).defineClass(name, b);
  }

  private ClassLoader getClassLoader (final Object loader) {
    if (loader instanceof ClassLoader) {
      return (ClassLoader)loader;
    } else if (loader != null) {
      return loader.getClass().getClassLoader();
    } else {
      return getClass().getClassLoader();
    }
  }
  
  /**
   * Provides a {@link ClassLoader} with a public {@link ClassLoader#defineClass
   * defineClass} method.
   */

  static class PublicClassLoader extends ClassLoader {

    /**
     * A map associating loaders to their parent class loader.
     */

    private final static Map INSTANCES = new HashMap();

    /**
     * Byte code of the dynamically generated classes.
     */

    private Map bytecodes = new HashMap();

    /**
     * Constructs a {@link PublicClassLoader} with the given parent class
     * loader.
     *
     * @param parent the parent class loader.
     */

    private PublicClassLoader (final ClassLoader parent) {
      super(parent);
    }

    /**
     * Returns the PublicClassLoader whose parent class loader is equal to the
     * given loader.
     *
     * @param parent the parent class loader.
     * @return the PublicClassLoader whose parent class loader is equal to the
     *      given loader.
     */

    public static PublicClassLoader getInstance (final ClassLoader parent) {
      synchronized (INSTANCES) {
        PublicClassLoader instance = (PublicClassLoader)INSTANCES.get(parent);
        if (instance == null) {
          instance = new PublicClassLoader(parent);
          INSTANCES.put(parent, instance);
        }
        return instance;
      }
    }

    /**
     * Converts an array of bytes into an instance of class {@link Class}.
     *
     * @param name the name of the class.
     * @param b the class bytes.
     * @return the {@link Class} object created from the data.
     */

    public Class defineClass (final String name, final byte[] b) {
      bytecodes.put(name.replace('.', '/') + ".class", b);
      return defineClass(name, b, 0, b.length);
    }

    public InputStream getResourceAsStream (final String name) {
      InputStream is = super.getResourceAsStream(name);
      if (is == null) {
        byte[] b = (byte[])bytecodes.get(name);
        if (b != null) {
          is = new ByteArrayInputStream(b);
        }
      }
      return is;
    }
  }
}
