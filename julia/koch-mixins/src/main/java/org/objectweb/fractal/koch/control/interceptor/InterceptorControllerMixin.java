/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.koch.control.interceptor.InterceptorDefAttributes;

/**
 * This class implements an interceptor controller for managing the
 * interceptors associated to the client and server interfaces of a component.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public abstract class InterceptorControllerMixin
    implements Controller, InterceptorController, InterceptorDefAttributes {

    // -----------------------------------------------------------------
    // Implementation of the Controller interface
    // -----------------------------------------------------------------
    
    public void initFcController(InitializationContext ic)
    throws InstantiationException {        
        _super_initFcController(ic);
    }
    
    // -----------------------------------------------------------------
    // Implementation of the InterceptorController interface
    // -----------------------------------------------------------------
    
    /**
     * Appends the specified interceptor at the end of the list of already
     * existing interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     */
    public void addFcInterceptor( ComponentInterface itf, Interceptor interceptor ) {
        
        /*
         * Fetch the last interceptor in the list of interceptors
         * already associated to the specified interface.
         */
        Object last = null;
        Object next = itf.getFcItfImpl();
        while( next instanceof Interceptor ) {
            Interceptor i = (Interceptor) next;
            last = next;
            next = i.getFcItfDelegate();
        }
        
        if( last == null ) {
            // The list of existing interceptors is empty
            Object content = itf.getFcItfImpl();
            interceptor.setFcItfDelegate(content);
            itf.setFcItfImpl(interceptor);
        }
        else {
            Interceptor i = (Interceptor) last;
            Object content = i.getFcItfDelegate();
            interceptor.setFcItfDelegate(content);
            i.setFcItfDelegate(interceptor);
        }
        
        itf.updateFcState();                                
    }

    /**
     * Returns the array of interceptors associated to the specified Fractal
     * interface.
     * 
     * @param itf  the Fractal interface
     * @return     the array of interceptors
     */
    public Interceptor[] getFcInterceptors( ComponentInterface itf ) {

        Object next = itf.getFcItfImpl();
        int size = 0;
        while( next instanceof Interceptor ) {
            Interceptor i = (Interceptor) next;
            next = i.getFcItfDelegate();
            size++;
        }
        
        Interceptor[] result = new Interceptor[size];
        next = itf.getFcItfImpl();
        size = 0;
        while( next instanceof Interceptor ) {
            Interceptor i = (Interceptor) next;
            result[size] = i;
            next = i.getFcItfDelegate();
            size++;
        }
        
        return result;
    }
    
    /**
     * Removes the specified interceptor from the list of already existing
     * interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     * @return             true if the list contained the specified interceptor
     */
    public boolean removeFcInterceptor( ComponentInterface itf, Interceptor interceptor ) {

        Object previous = null;
        Object next = itf.getFcItfImpl();
        while( next instanceof Interceptor ) {
            Interceptor i = (Interceptor) next;
            if( next == interceptor ) {
                // The interceptor has been found
                Object nextnext = i.getFcItfDelegate();
                if( previous == null ) {
                    // The interceptor is the first one in the list
                    itf.setFcItfImpl(nextnext);
                }
                else {
                    Interceptor p = (Interceptor) previous;
                    p.setFcItfDelegate(nextnext);
                }
                itf.updateFcState();                                
                return true;
            }
            
            previous = next;
            next = i.getFcItfDelegate();
        }
        
        // The interceptor hasn't been found
        return false;
    }
    
    
    // -----------------------------------------------------------------
    // Implementation of the InterceptorDefAttributes interface
    // -----------------------------------------------------------------
    
    /**
     * @param interceptors
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    public void setInterceptors( String interceptors ) {    
        this.interceptors = interceptors;
    }
    
    /**
     * @return
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    public String getInterceptors() {
        return interceptors;
    }

    private String interceptors = "";
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    abstract public void _super_initFcController( InitializationContext ic )
    throws InstantiationException;
}
