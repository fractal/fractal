/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.julia.Controller;

/**
 * Mixin class for dynamically managing (add and remove) interceptors to the
 * model instance of a collection interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public abstract class CollectionItfContentControllerMixin
    implements Controller, ContentController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private CollectionItfContentControllerMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  public Object getFcInternalInterface (final String interfaceName)
    throws NoSuchInterfaceException
  {
      if( interfaceName.startsWith("/koch/collection/") ) {
          return _this_fcInternalInterfaces.get(interfaceName);
      }
      
      return _super_getFcInternalInterface(interfaceName);
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>fcInterfaces</tt> field required by this mixin. This field is
   * supposed to store the interfaces of the component.
   */
  public Map _this_fcInternalInterfaces;
  
  public abstract Object _super_getFcInternalInterface(final String interfaceName)
  throws NoSuchInterfaceException;

}
