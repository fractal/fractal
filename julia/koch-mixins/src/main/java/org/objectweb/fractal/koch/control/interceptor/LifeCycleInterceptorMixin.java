/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.julia.control.lifecycle.ChainedIllegalLifeCycleException;
import org.objectweb.fractal.koch.control.interceptor.IllegalInterceptorException;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;

/**
 * Provides lifecycle related checks to a {@link InterceptorController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public abstract class LifeCycleInterceptorMixin implements InterceptorController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private LifeCycleInterceptorMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * Checks that the component is stopped and then calls the overriden method.
   *
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public void addFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException {

      if (_this_weaveableOptLC != null) {
      String state = _this_weaveableOptLC.getFcState();
      if (!LifeCycleController.STOPPED.equals(state)) {
        throw new ChainedIllegalLifeCycleException(
          null, _this_weaveableOptC, "The component is not stopped");
      }
    }
    _super_addFcInterceptor(itf, interceptor);
  }

  /**
   * Checks that the component is stopped and then calls the overriden method.
   * 
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * @return             true if the list contained the specified interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public boolean removeFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException {
      
      if (_this_weaveableOptLC != null) {
        String state = _this_weaveableOptLC.getFcState();
        if (!LifeCycleController.STOPPED.equals(state)) {
          throw new ChainedIllegalLifeCycleException(
            null, _this_weaveableOptC, "The component is not stopped");
        }
      }
      return _super_removeFcInterceptor(itf, interceptor);
    }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */
  public Component _this_weaveableOptC;

  /**
   * The <tt>weaveableOptLC</tt> field required by this mixin. This field is
   * supposed to reference the {@link LifeCycleController} interface of the
   * component to which this controller object belongs.
   */
  public LifeCycleController _this_weaveableOptLC;

  /**
   * The {@link InterceptorController#addFcInterceptor addFcInterceptor} method
   * overriden by this mixin.
   *
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public abstract void _super_addFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException;

  /**
   * The {@link InterceptorController#removeFcInterceptor removeFcInterceptor}
   * method overriden by this mixin.
   * 
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * @return             true if the list contained the specified interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public abstract boolean _super_removeFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException;
}
