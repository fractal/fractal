/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.membrane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.koch.control.membrane.MembraneController;
import org.objectweb.fractal.util.SuperControllerHelper;


/**
 * Provides as a mixin layer, a basic implementation of the {@link
 * MembraneController} interface.
 * 
 * Membrane controllers provide an access to the current control membrane.
 * This access allows introspecting the control membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5
 */
public abstract class BasicMembraneControllerMixin implements MembraneController {

    private BasicMembraneControllerMixin() {}
    
    public Component getFcMembrane() {
        return SuperControllerHelper.getTopMostComponent(compctrl);
    }

    private Component compctrl;

    public void initFcController(InitializationContext ic) throws InstantiationException {
        compctrl = (Component) ic.getInterface("//component");
        _super_initFcController(ic);
    }
    
    abstract void _super_initFcController(InitializationContext ic)
    throws InstantiationException;
}
