/***
 * Julia
 * Copyright (C) 2005-2007 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.koch.control.interceptor.ChainedIllegalInterceptorException;
import org.objectweb.fractal.koch.control.interceptor.IllegalInterceptorException;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;

/**
 * Provides binding related operations to a {@link InterceptorController}.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 * @since 2.5.1
 */
public abstract class BindingInterceptorMixin implements Controller, InterceptorController {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private BindingInterceptorMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  private Object fcContent;
  
  public void initFcController(InitializationContext ic) throws InstantiationException {
      fcContent = ic.content;
      _super_initFcController(ic);
  }
  
  /**
   * Re-inject the dependency towards the target of a binding when an
   * interceptor is added.
   *
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   */
  public void addFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException {
            
    _super_addFcInterceptor(itf, interceptor);
    
    /*
     * For client interfaces owned by a component with a content (e.g. primitive
     * ones), re-inject the dependency towards the target of a binding.
     */
    InterfaceType it = (InterfaceType) itf.getFcItfType();
    try {
        if( it.isFcClientItf() && fcContent != null ) {
            
            String clientItfName = itf.getFcItfName();
            BindingController bc = (BindingController) fcContent;
            Object target = bc.lookupFc(clientItfName);
            
            /*
             * When an interceptor has already been added, the target references
             * this interceptor. The current interceptor is added at the end
             * of the chain of interceptors. In this case, there is no need to
             * modify the binding.
             */
            if( target!=null && !(target instanceof Interceptor) ) {
                bc.bindFc(clientItfName, interceptor);
            }
        }
    }
    catch (NoSuchInterfaceException e) {
        String msg = "NoSuchInterfaceException: "+e.getMessage();
        throw new ChainedIllegalInterceptorException(e,itf,interceptor,msg);
    }
    catch (IllegalBindingException e) {
        String msg = "IllegalBindingException: "+e.getMessage();
        throw new ChainedIllegalInterceptorException(e,itf,interceptor,msg);
    }
  }

  /**
   * Re-inject the dependency towards the target of a binding when an
   * interceptor is removed.
   * 
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * @return             true if the list contained the specified interceptor
   */
  public boolean removeFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalInterceptorException, IllegalLifeCycleException {
      
      boolean ret = _super_removeFcInterceptor(itf, interceptor);
      
      /*
       * For client interfaces owned by a component with a content (e.g. primitive
       * ones), re-inject the dependency towards the target of a binding if the
       * interceptor has effectively been removed.
       */
      InterfaceType it = (InterfaceType) itf.getFcItfType();
      try {
        if( ret && it.isFcClientItf() && fcContent != null ) {
            
            String clientItfName = itf.getFcItfName();
            BindingController bc = (BindingController) fcContent;
            Object delegate = interceptor.getFcItfDelegate();
            bc.bindFc(clientItfName, delegate);
        }
    }
    catch (NoSuchInterfaceException e) {
        String msg = "NoSuchInterfaceException: "+e.getMessage();
        throw new ChainedIllegalInterceptorException(e,itf,interceptor,msg);
    }
    catch (IllegalBindingException e) {
        String msg = "IllegalBindingException: "+e.getMessage();
        throw new ChainedIllegalInterceptorException(e,itf,interceptor,msg);
    }
      
      return ret;
    }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableOptC</tt> field required by this mixin. This field is
   * supposed to reference the {@link Component} interface of the component to
   * which this controller object belongs.
   */
  public Component _this_weaveableOptC;

//  /**
//   * The <tt>weaveableBC</tt> field required by this mixin. This field is
//   * supposed to reference the {@link BindingController} interface of the
//   * component to which this controller object belongs.
//   */
//  public BindingController _this_weaveableBC;

  /**
   * The {@link InterceptorController#addFcInterceptor addFcInterceptor} method
   * overriden by this mixin.
   *
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public abstract void _super_addFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalLifeCycleException;

  /**
   * The {@link InterceptorController#removeFcInterceptor removeFcInterceptor}
   * method overriden by this mixin.
   * 
   * @param itf          the Fractal interface
   * @param interceptor  the interceptor
   * @return             true if the list contained the specified interceptor
   * 
   * @throws IllegalLifeCycleException if this component has a {@link
   *      LifeCycleController} interface, but it is not in an appropriate
   *      state to perform this operation.
   */
  public abstract boolean _super_removeFcInterceptor( ComponentInterface itf, Interceptor interceptor )
  throws IllegalLifeCycleException;

  /**
   * The {@link Controller#initFcController initFcController} method overriden
   * by this mixin.
   *
   * @param ic information about the component to which this controller object
   *      belongs.
   * @throws InstantiationException if the initialization fails.
   */
  public abstract void _super_initFcController (InitializationContext ic)
  throws InstantiationException;
}
