This module is used to launch Julia performance tests. Performance classes are defined in julia-tests-perf module, and
here are used via profiles. The pom.xml defines a number of execution profiles: each one can be activated using
standard command-line arguments. So:

mvn -Pperf1
mvn -Pperf1ogf
mvn -Pperf2
mvn -Pperf2ogf
mvn -Pperf3
mvn -Pperf3ogf
mvn -Pperf4
mvn -Pperf4ogf
mvn -Pperf5
mvn -Pperf5ogf
mvn -Pperf6
mvn -Pperf6ogf
mvn -Pperf7
mvn -Pperf7ogf
