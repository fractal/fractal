/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.loader;

import java.util.Map;

/**
 * An interface to load classes or to generate them on the fly.
 */

public interface Loader {

  /**
   * Initializes this loader.
   * 
   * @param context the initialization arguments.
   * @throws Exception if the initialization fails. 
   */
  
  void init (Map context) throws Exception;
  
  /**
   * Loads the tree whose name is given.
   *
   * @param name a tree name.
   * @return the tree whose name is given.
   * @throws Exception if the tree is not found or cannot be loaded.
   */
  
  Tree loadTree (String name) throws Exception;

  /**
   * Evaluates the given tree in the given context. Each leaf node of the given
   * tree whose name begins with ' is replaced by the evaluation of the tree
   * associated to this name in the given context (except if the result of this
   * evaluation is "QUOTE"). For example the evaluation of "('x d)" in a context
   * where x is associated to "(a 'y)", y is associated to "(b 'z)", and z is
   * associated to "QUOTE" gives "((a (b 'z)) d)".
   *
   * @param tree the tree to be evaluated.
   * @param context a map associating names to trees.
   * @return the evaluated tree.
   * @throws Exception if a name is not found in the given context. The
   *      exception's detail is equal to this name.
   */

  Tree evalTree (Tree tree, Map context) throws Exception;

  /**
   * Creates and returns the object whose description is given.
   *
   * @param objectDescriptor the descriptor of the object to be created. This
   *      descriptor must be either of the form "className", or of the form
   *      "(classDescriptor arg1 ... argN)". In the first case, "className"
   *      must be the fully qualified name of a class. In the second case,
   *      "classDescriptor" must be a valid class descriptor (see {@link
   *      #loadClass(Tree,Object) loadClass}). "arg1" ... "argN" can be arbitrary
   *      trees.
   * @param loader an optional class loader to load auxiliary classes.
   * @return an object created according to the given description. If this
   *      description is of the form "className", this method returns a new
   *      instance of the "className" class (which must have a default public
   *      constructor). If the description is of the form "(classDescriptor
   *      arg1 ... argN)", the class described by "classDescriptor" is loaded or
   *      generated with the {@link #loadClass loadClass} method, and a new
   *      instance of this class is created (this class must have a default
   *      public constructor). Then, if the arguments "arg1 ... argN" are not
   *      empty, the {@link Initializable#initialize initialize} method is used
   *      to initialize the previous instance (in this case, the
   *      "classDescriptor" class must implement the {@link Initializable}
   *      interface; the {@link Initializable#initialize initialize} method is
   *      called with <tt>(arg1 ... argN)</tt> as parameter). Finally the
   *      previously created object is returned.
   * @throws Exception if the specified object cannot be created.
   */

  Object newObject (Tree objectDescriptor, Object loader) throws Exception;

  /**
   * Loads the class whose name is given.
   *
   * @param className the fully qualified name of the class to be returned.
   * @param loader an optional class loader to load auxiliary classes.
   * @return the class whose name is given.
   * @throws ClassNotFoundException if the specified class is not found.
   */

  Class loadClass (String className, Object loader) throws ClassNotFoundException;

  /**
   * Loads or generates the class whose descriptor is given.
   *
   * @param classDescriptor the descriptor of the class to be loaded or
   *      dynamically generated. This descriptor must be either of the form
   *      "className", or of the form "(objectDescriptor arg1 ... argN)". In the
   *      first case, "className" must be the fully qualified name of a class.
   *      In the second case, "objectDescriptor" must be a valid object
   *      descriptor (see {@link #newObject newObject}), describing an object
   *      that implements the {@link
   *      org.objectweb.fractal.julia.asm.ClassGenerator} interface.
   *      "arg1" ... "argN" can be arbitrary trees.
   * @param loader an optional class loader to load auxiliary classes.
   * @return the class whose description is given. If this description is of the
   *      form "className", this method returns the "className" class. If the
   *      description is of the form "(objectDescriptor arg1 ... argN)", the
   *      class generator described by "objectDescriptor" is instantiated with
   *      the {@link #newObject newObject} method, and its {@link
   *      org.objectweb.fractal.julia.asm.ClassGenerator#generateClass
   *      generateClass} method is used to generate a class (this method is
   *      called with <tt>classDescriptor</tt> as second argument), which is
   *      finally returned.
   * @throws ClassNotFoundException is the specified class cannot be loaded or
   *      dynamically generated.
   */

  Class loadClass (Tree classDescriptor, Object loader) throws ClassNotFoundException;
}
