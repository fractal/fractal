/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * A component interface to control the bindings of the content of container
 * components. This interface is useful when a container and its encapsulated
 * "user" component are merged into a single class. Indeed, in this case, the
 * container class is generated as a sub class of the user component class, and
 * may therefore override the binding management methods that may be provided by
 * the user component (because both classes may implement the same {@link
 * org.objectweb.fractal.api.control.BindingController} interface). In order to
 * still be able to call the user component binding management methods from
 * outside the component, four new methods are generated in the container class,
 * that just calls the corresponding methods in the user component class (i.e.,
 * in the super class) with the <tt>super</tt> keyword.
 */

public interface ContentBindingController {

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.BindingController#listFc listFc}
   * method on the component encapsulated in this container.
   *
   * @return the names of the client interfaces of the component to which this
   *      interface belongs.
   */

  String[] listFcContent ();

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.BindingController#lookupFc lookupFc}
   * method on the component encapsulated in this container.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @return the server interface to which the given interface is bound, or <tt>
   *      null</tt> if it is not bound.
   * @throws NoSuchInterfaceException if the component to which this interface
   *      belongs does not have a client interface whose name is equal to the
   *      given name.
   */

  Object lookupFcContent (String clientItfName) throws NoSuchInterfaceException;

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.BindingController#bindFc bindFc} method
   * on the component encapsulated in this container.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @param serverItf a server interface.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be created.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void bindFcContent (String clientItfName, Object serverItf) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.BindingController#unbindFc unbindFc}
   * method on the component encapsulated in this container.
   *
   * @param clientItfName the name of a client interface of the component to
   *      which this interface belongs.
   * @throws NoSuchInterfaceException if there is no such client interface.
   * @throws IllegalBindingException if the binding cannot be removed.
   * @throws IllegalLifeCycleException if this component has a {@link
   *      org.objectweb.fractal.api.control.LifeCycleController} interface, but it is not in an appropriate state
   *      to perform this operation.
   */

  void unbindFcContent (String clientItfName) throws
    NoSuchInterfaceException,
    IllegalBindingException,
    IllegalLifeCycleException;
}
