/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.Type;

/**
 * Provides a basic implementation of the {@link ComponentInterface} interface.
 */

public abstract class BasicComponentInterface implements ComponentInterface {

  /**
   * The component to which this component interface belongs.
   */

  protected Component owner;

  /**
   * The name of this component interface.
   */

  protected String name;

  /**
   * The type of this component interface.
   */

  protected Type type;

  /**
   * The flags of this component interfaces. These flags indicates if this
   * component interface is an internal interface or not, and if it has an
   * permanently associated interceptor or not.
   */

  protected int flags;

  // -------------------------------------------------------------------------
  // Constructors
  // -------------------------------------------------------------------------

  /**
   * Constructs an uninitialized {@link BasicComponentInterface}. This default
   * public constructor is needed for the implementation of the {@link #clone
   * clone} method.
   */

  public BasicComponentInterface () {
  }

  /**
   * Constructs a new {@link BasicComponentInterface}.
   *
   * @param owner the component to which the interface belongs. If this
   *      parameter is <tt>null</tt> the owner is set to the interface itself,
   *      which must therefore be a {@link Component} interface.
   * @param name the name of the interface.
   * @param type the type of the interface.
   * @param isInternal <tt>true</tt> if the interface is an internal interface.
   * @param impl the object that implements the interface.
   */

  public BasicComponentInterface (
    final Component owner,
    final String name,
    final Type type,
    final boolean isInternal,
    final Object impl)
  {
    boolean hasInterceptor = impl instanceof Interceptor;
    this.owner = owner == null ? (Component)this : owner;
    this.name = name;
    this.type = type;
    this.flags = (isInternal ? 0x01 : 0x00) | (hasInterceptor ? 0x02 : 0x00);
    this.setFcItfImpl(impl);
  }

  // -------------------------------------------------------------------------
  // Implementation of the Interface interface
  // -------------------------------------------------------------------------

  public Component getFcItfOwner () {
    return owner;
  }

  public String getFcItfName () {
    return name;
  }

  public Type getFcItfType () {
    return type;
  }

  public boolean isFcInternalItf () {
    return (flags & 0x01) != 0;
  }

  // -------------------------------------------------------------------------
  // Implementation of the ComponentInterface interface
  // -------------------------------------------------------------------------

  public void setFcItfName (final String name) {
    this.name = name;
  }

  public boolean hasFcInterceptor () {
    return (flags & 0x02) != 0;
  }

  /**
   * Update the internal state of this interface.
   * Needed to be able to dynamically add or remove an interceptor.
   * 
   * The original code only sets flags in the constructor. When an interceptor
   * is dynamically added, the fact that hasInterceptor() returns false causes
   * an incorrect behavior of the BindingController which ignores the added
   * interceptor.
   * The updateFcState() method is meant to be called whenever an interceptor is
   * added or removed to update the value of flags. This solution has been
   * preferred to the more intruisive one which would have consisted in changing
   * the implementation of hasFcInterceptor() to:
   * (getFcImpl() instanceof Interceptor).
   * 
   * @since 2.5
   */
  public void updateFcState() {
      boolean hasInterceptor = getFcItfImpl() instanceof Interceptor;
      this.flags = (flags & 0x01) | (hasInterceptor ? 0x02 : 0x00);
  }
  
  // -------------------------------------------------------------------------
  // Overriden Object methods
  // -------------------------------------------------------------------------

  /**
   * Returns the hash code of this component interface.
   *
   * @return the hash code of this component interface.
   */

  public int hashCode () {
    return System.identityHashCode(getFcItfOwner()) * getFcItfName().hashCode();
  }

  /**
   * Tests if the given object is equal to this component interface.
   *
   * @param o the object to be compared to this component interface.
   * @return <tt>true</tt> if o is a component interface with the same name and
   *      owner as this component interface, and if both are internal (resp.
   *      external) component interfaces.
   */

  public boolean equals (final Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Interface) {
      Interface itf = (Interface)o;
      return
        getFcItfOwner() == itf.getFcItfOwner() && // TODO ou equals?
        getFcItfName().equals(itf.getFcItfName()) &&
        isFcInternalItf() == itf.isFcInternalItf();
    }
    return false;
  }

  public Object clone () {
    // cannot use super.clone(), because the clone method is not defined in all
    // Java platforms (such as J2ME, CLDC profile)
    BasicComponentInterface clone;
    try {
      clone = (BasicComponentInterface)getClass().newInstance();
    } catch (Exception e) {
      throw new Error("Internal error"); // should not happen
    }
    clone.owner = this.owner;
    clone.name = name;
    clone.type = type;
    clone.flags = flags;
    Object impl = getFcItfImpl();
    if (hasFcInterceptor()) {
      impl = ((Interceptor)impl).clone();
    }
    clone.setFcItfImpl(impl);
    return clone;
  }
}
