/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.Util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * A sub class of the {@link InstantiationException} class.
 */

public class ChainedInstantiationException extends InstantiationException {

  /**
   * The exception that caused this exception. May be <tt>null</tt>.
   */

  private final Throwable exception;

  /**
   * The factory component that cannot be instantiated. May be <tt>null</tt>.
   */

  private transient Component factory;

  /**
   * Constructs a new {@link ChainedInstantiationException} exception.
   *
   * @param exception the cause of this exception. May be <tt>null</tt>.
   * @param factory the factory component that cannot be instantiated. May be
   *      <tt>null</tt>.
   * @param message a detailed error message.
   */

  public ChainedInstantiationException (
    final Throwable exception,
    final Component factory,
    final String message)
  {
    super(message);
    this.exception = exception;
    this.factory = factory;
  }

  /**
   * Returns the exception that caused in this exception.
   *
   * @return the exception that caused this exception. May be <tt>null</tt>.
   */

  public Throwable getException () {
    return exception;
  }

  /**
   * Returns the factory component that cannot be instantiated.
   *
   * @return the factory component that cannot be instantiated. May be
   *      <tt>null</tt>.
   */

  public Component getFactory () {
    if (factory != null && !(factory instanceof Interface)) {
      try {
        return (Component)factory.getFcInterface("component");
      } catch (NoSuchInterfaceException ignored) {
      }
    }
    return factory;
  }

  // -------------------------------------------------------------------------
  // Overriden Exception methods
  // -------------------------------------------------------------------------

  /**
   * Returns a String representation of this exception.
   *
   * @return a String representation of this exception.
   */

  public String toString () {
    StringBuffer buf = new StringBuffer();
    buf.append("InstantiationException: ");
    buf.append(getMessage());
    if (getFactory() != null) {
      buf.append(" (template = ");
      Util.toString(getFactory(), buf);
      buf.append(')');
    }
    return buf.toString();
  }

  /**
   * Prints the stack backtrace.
   */

  public void printStackTrace () {
    if (exception != null) {
      System.err.println(this);
      exception.printStackTrace();
    } else {
      super.printStackTrace();
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print stream.
   *
   * @param s <tt>PrintStream</tt> to use for output.
   */

  public void printStackTrace (final PrintStream s) {
    if (exception != null) {
      s.println(this);
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print writer.
   *
   * @param s <tt>PrintWriter</tt> to use for output.
   */

  public void printStackTrace (final PrintWriter s) {
    if (exception != null) {
      s.write(this + "\n");
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }
  
  private void writeObject (final ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    Component c = getFactory();
    out.writeObject(c instanceof Interface ? c : null);
  }

  private void readObject (final ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject();
    factory = (Component)in.readObject();
  }
}
