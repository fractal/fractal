/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;

import org.objectweb.fractal.julia.Util;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * A sub class of the {@link IllegalBindingException} class.
 */

public class ChainedIllegalBindingException extends IllegalBindingException {

  /**
   * The exception that caused this exception. May be <tt>null</tt>.
   */

  private final Throwable exception;

  /**
   * The client side component of the illegal binding.
   */

  private transient Component clientComp;

  /**
   * The server side component of the illegal binding. May be <tt>null</tt>.
   */

  private transient Component serverComp;

  /**
   * The name of the client interface of the illegal binding.
   */

  private final String clientItf;

  /**
   * The name of the server interface of the illegal binding. May be
   * <tt>null</tt>.
   */

  private final String serverItf;

  /**
   * Constructs a new {@link ChainedIllegalBindingException} exception.
   *
   * @param exception the cause of this exception. May be <tt>null</tt>.
   * @param clientComponent the client side component of the illegal binding.
   * @param serverComponent the server side component of the illegal binding.
   *      May be <tt>null</tt>.
   * @param clientItf the name of the client interface of the illegal binding.
   * @param serverItf the name of the server interface of the illegal binding.
   *      May be <tt>null</tt>.
   * @param message a detailed error message.
   */

  public ChainedIllegalBindingException (
    final Throwable exception,
    final Component clientComponent,
    final Component serverComponent,
    final String clientItf,
    final String serverItf,
    final String message)
  {
    super(message);
    this.exception = exception;
    this.clientComp = clientComponent;
    this.serverComp = serverComponent;
    this.clientItf = clientItf;
    this.serverItf = serverItf;
  }

  /**
   * Returns the exception that caused in this exception.
   *
   * @return the exception that caused this exception. May be <tt>null</tt>.
   */

  public Throwable getException () {
    return exception;
  }

  /**
   * Returns the client side component of the illegal binding.
   *
   * @return the client side component of the illegal binding.
   */

  public Component getClientComponent () {
    if (clientComp != null && !(clientComp instanceof Interface)) {
      try {
        return (Component)clientComp.getFcInterface("component");
      } catch (NoSuchInterfaceException ignored) {
      }
    }
    return clientComp;
  }

  /**
   * Returns the server side component of the illegal binding.
   *
   * @return the server side component of the illegal binding. May be
   *      <tt>null</tt>.
   */

  public Component getServerComponent () {
    if (serverComp != null && !(serverComp instanceof Interface)) {
      try {
        return (Component)serverComp.getFcInterface("component");
      } catch (NoSuchInterfaceException ignored) {
      }
    }
    return serverComp;
  }

  /**
   * Returns the name of the client interface of the illegal binding.
   *
   * @return the name of the client interface of the illegal binding.
   */

  public String getClientInterface () {
    return clientItf;
  }

  /**
   * Returns he name of the server interface of the illegal binding.
   *
   * @return the name of the server interface of the illegal binding. May be
   *      <tt>null</tt>.
   */

  public String getServerInterface () {
    return serverItf;
  }

  // -------------------------------------------------------------------------
  // Overriden Exception methods
  // -------------------------------------------------------------------------

  /**
   * Returns a String representation of this exception.
   *
   * @return a String representation of this exception.
   */

  public String toString () {
    StringBuffer buf = new StringBuffer();
    buf.append("IllegalBindingException: ");
    buf.append(getMessage());
    buf.append(" (client interface = ");
    Util.toString(getClientComponent(), buf);
    buf.append('.');
    buf.append(getClientInterface());
    if (getServerComponent() != null) {
      buf.append(", server interface = ");
      Util.toString(getServerComponent(), buf);
      buf.append('.');
      buf.append(getServerInterface());
    }
    buf.append(')');
    return buf.toString();
  }

  /**
   * Prints the stack backtrace.
   */

  public void printStackTrace () {
    if (exception != null) {
      System.err.println(this);
      exception.printStackTrace();
    } else {
      super.printStackTrace();
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print stream.
   *
   * @param s <tt>PrintStream</tt> to use for output.
   */

  public void printStackTrace (final PrintStream s) {
    if (exception != null) {
      s.println(this);
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }

  /**
   * Prints this exception and its backtrace to the specified print writer.
   *
   * @param s <tt>PrintWriter</tt> to use for output.
   */

  public void printStackTrace (final PrintWriter s) {
    if (exception != null) {
      s.write(this + "\n");
      exception.printStackTrace(s);
    } else {
      super.printStackTrace(s);
    }
  }
  
  private void writeObject (final ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    Component c = getClientComponent();
    out.writeObject(c instanceof Interface ? c : null);
    c = getServerComponent();
    out.writeObject(c instanceof Interface ? c : null);
  }

  private void readObject (final ObjectInputStream in)
    throws IOException, ClassNotFoundException
  {
    in.defaultReadObject();
    clientComp = (Component)in.readObject();
    serverComp = (Component)in.readObject();
  }
}

