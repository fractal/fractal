/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.type;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

/**
 * Provides a basic implementation of the {@link TypeFactory} interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>none.</li>
 * </ul>
 */

public class BasicTypeFactoryMixin implements TypeFactory {

  // -------------------------------------------------------------------------
  // PUBLIC constructor (unlike standard mixin classes ; needed for bootstrap)
  // -------------------------------------------------------------------------

  public BasicTypeFactoryMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  public InterfaceType createFcItfType (
    final String name,
    final String signature,
    final boolean isClient,
    final boolean isOptional,
    final boolean isCollection) throws InstantiationException
  {
    return new BasicInterfaceType(
      name, signature, isClient, isOptional, isCollection);
  }

  public ComponentType createFcType (final InterfaceType[] interfaceTypes)
    throws InstantiationException
  {
    return new BasicComponentType(interfaceTypes);
  }
}
