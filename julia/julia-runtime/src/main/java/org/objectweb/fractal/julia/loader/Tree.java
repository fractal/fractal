/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.loader;

/**
 * An immutable string tree.
 */

public class Tree {

  /**
   * The value of this leaf node, or <tt>null</tt> for a "normal" node.
   */

  private final String leaf;

  /**
   * The subtrees of this node, or <tt>null</tt> for a leaf node.
   */

  private final Tree[] subTrees;

  // -----------------------------------------------------------------------
  // Constructor
  // -----------------------------------------------------------------------

  /**
   * Constructs a new {@link Tree} instance.
   *
   * @param leaf the value of the node to be created.
   */

  public Tree (final String leaf) {
    this.leaf = leaf;
    this.subTrees = null;
  }
  /**
   * Constructs a new {@link Tree} instance.
   *
   * @param subTrees the subtrees of the node to be created, or <tt>null</tt>
   *      for a leaf node.
   */

  public Tree (final Tree[] subTrees) {
    this.leaf = null;
    this.subTrees = subTrees;
  }

  // -----------------------------------------------------------------------
  // Accessors
  // -----------------------------------------------------------------------

  /**
   * Returns the number of sub trees of this tree.
   *
   * @return the number of sub trees of this tree.
   */

  public int getSize () {
    return (subTrees == null ? 0 : subTrees.length);
  }

  /**
   * Returns the sub tree of this tree whose index is given.
   *
   * @param index the index of the sub tree to be returned.
   * @return the sub tree of this tree whose index is given.
   * @throws java.lang.ArrayIndexOutOfBoundsException if <tt>index</tt> is
   *      negative or strictly greater than {@link #getSize getSize}.
   */

  public Tree getSubTree (final int index) {
    if (subTrees == null) {
      throw new ArrayIndexOutOfBoundsException(index);
    } else {
      return subTrees[index];
    }
  }

  /**
   * Returns the sub trees of this tree.
   *
   * @return the sub trees of this tree.
   */

  public Tree[] getSubTrees () {
    if (subTrees == null) {
      return new Tree[0];
    } else {
      return subTrees;
    }
  }

  /**
   * Return the value associated to the given key. This method supposes that 
   * this tree is of the form "((key1 value1) ... (keyN valueN))".
   * 
   * @param key a key
   * @return the value returned to this key, or <tt>null</tt>.
   */
  
  public Tree getValue (final String key) {
    Tree[] t = getSubTrees();
    for (int i = 0; i < t.length; ++i) {
      if (t[i].getSize() == 2) {
        if (t[i].getSubTree(0).equals(key)) {
          return t[i].getSubTree(1);
        }
      }
    }
    return null;
  }
  
  // -----------------------------------------------------------------------
  // Overriden Object methods
  // -----------------------------------------------------------------------

  /**
   * Tests if this tree is equal to the given object.
   *
   * @param o the object to be compared to this tree.
   * @return <tt>true</tt> if <tt>o</tt> is a tree that is equal to this tree,
   *      or if <tt>o</tt> is a String that is equal to the String
   *      representation of this tree.
   */

  public boolean equals (final Object o) {
    if (o instanceof String) {
      String s = (String)o;
      // instead of converting this tree to a String, and then comparing the two
      // Strings, it is MUCH MORE EFFICIENT to compare the tree directly to the
      // given String, without doing any String manipulation:
      return equals(s, 0, s.length()) == 0;
    } else {
      throw new RuntimeException("Not yet implemented");
    }
  }

  /**
   * Returns the hashcode of this tree.
   *
   * @return the hashcode of this tree.
   */

  public int hashCode () {
    // instead of converting this tree to a String, and then computing the
    // hashcode of this String, it is MUCH MORE EFFICIENT to compute the
    // hashcode directly, without doing any String manipulation:
    return hashCode(0);
  }

  /**
   * Returns a string representation of this tree.
   *
   * @return a string representation of this tree, with a LISP like syntax, as
   *      in the following example: "(foo (bar foo))".
   */

  public String toString () {
    if (leaf != null) {
      return leaf;
    }
    if (subTrees.length == 0) {
      return "()";
    }
    StringBuffer buf = new StringBuffer();
    toString(buf);
    return buf.toString();
  }

  // -----------------------------------------------------------------------
  // Utility methods
  // -----------------------------------------------------------------------

  /**
   * Computes the hashcode of this tree based on an initial hashcode value.
   *
   * @param hc an initial hashcode value.
   * @return the hashcode of this tree based on the given initial value.
   */

  private int hashCode (int hc) {
    if (leaf != null) {
      hc = 17 * (hc + leaf.hashCode());
    } else {
      int l = subTrees.length;
      hc = 17 * (hc + '(');
      for (int i = 0; i < l; ++i) {
        if (i > 0) {
          hc = 17 * (hc + ' ');
        }
        hc = subTrees[i].hashCode(hc);
      }
      hc = 17 * (hc + ')');
    }
    return hc;
  }

  /**
   * Test if the String representation of this tree is equal to the given
   * String.
   *
   * @param str a String.
   * @param off the start index of the String to be compared to this tree in
   *      <tt>str</tt>;
   * @param len the length of the String to be compared to this tree.
   * @return the number of remaining characters in <tt>str</tt>, once the prefix
   *      corresponding to the String representation of this tree is removed, or
   *      -1 if the given String does not starts with the String representation
   *      of this tree.
   */

  public int equals (final String str, int off, int len) {
    if (leaf != null) {
      int l = leaf.length();
      if (len < l || !str.regionMatches(false, off, leaf, 0, l)) {
        return -1;
      }
      return len - l;
    } else {
      int l = subTrees.length;
      if (len == 0 || str.charAt(off) != '(') {
        return -1;
      }
      ++off;
      --len;
      for (int i = 0; i < l; ++i) {
        if (i > 0) {
          if (len == 0 || str.charAt(off) != ' ') {
            return -1;
          }
          ++off;
          --len;
        }
        int newLen = subTrees[i].equals(str, off, len);
        if (newLen == -1) {
          return -1;
        }
        off += len - newLen;
        len = newLen;
      }
      if (len == 0 || str.charAt(off) != ')') {
        return -1;
      }
      return len - 1;
    }
  }

  /**
   * Appends the string representation of this tree to the given string buffer.
   *
   * @param buf the string buffer into which the string description of this
   *      tree must be appened.
   */

  private void toString (final StringBuffer buf) {
    if (subTrees == null) {
      buf.append(leaf);
    } else {
      int l = subTrees.length;
      buf.append('(');
      for (int i = 0; i < l; ++i) {
        if (i > 0) {
          buf.append(' ');
        }
        subTrees[i].toString(buf);
      }
      buf.append(')');
    }
  }
}
