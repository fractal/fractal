/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * A component interface to control the life cycle of the content of container
 * components. This interface is useful when a container and its encapsulated
 * "user" component are merged into a single class. Indeed, in this case, the
 * container class is generated as a sub class of the user component class, and
 * may therefore override the life cycle management methods that may be provided
 * by the user component (because both classes may implement the same {@link
 * org.objectweb.fractal.api.control.LifeCycleController} interface). In order
 * to still be able to call the user component life cycle management methods
 * from outside the component, two new methods are generated in the container
 * class, that just calls the corresponding methods in the user component class
 * (i.e., in the super class) with the <tt>super</tt> keyword.
 */

public interface ContentLifeCycleController {

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.LifeCycleController#startFc startFc}
   * method on the component encapsulated in this container.
   * 
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  void startFcContent () throws IllegalLifeCycleException;

  /**
   * Calls the {@link
   * org.objectweb.fractal.api.control.LifeCycleController#stopFc stopFc}
   * method on the component encapsulated in this container.
   * 
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  void stopFcContent () throws IllegalLifeCycleException;
}
