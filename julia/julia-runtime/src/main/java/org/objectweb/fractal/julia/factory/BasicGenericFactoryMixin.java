/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id:BasicGenericFactoryMixin.java 2631 2007-05-30 03:13:46Z merle $
 */

package org.objectweb.fractal.julia.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;

import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.loader.Tree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provides a basic implementation of the {@link GenericFactory} interface.
 * <br>
 * <br>
 * <b>Requirements</b>
 * <ul>
 * <li>the types used in the {@link #newFcInstance newFcInstance} method must
 * be instances of {@link ComponentType}.</li>
 * <li>the controller descriptors used in the {@link #newFcInstance
 * newFcInstance} method must be strings referencing "true descriptors", which
 * are loaded with the {@link #_this_weaveableL} loader. Here "true descriptors"
 * means controller descriptors as specified in {@link
 * org.objectweb.fractal.julia.asm.ContextClassGenerator}.</li>
 * </ul>
 */

public class BasicGenericFactoryMixin implements GenericFactory {

  // -------------------------------------------------------------------------
  // PUBLIC constructor (needed for bootstrap)
  // -------------------------------------------------------------------------

  public BasicGenericFactoryMixin () {
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  public Component newFcInstance (
    final Type type,
    final Object controllerDesc,
    final Object contentDesc) throws InstantiationException
  {
    return newComponent(
    	     getInitializationContextClass(type, controllerDesc, contentDesc),
    		 contentDesc);
  }

  /**
   * Get the initialization context class associated to a given
   * {@link Type} instance, controller and content descriptors.
   *
   * @param type the given {@link Type} instance.
   * @param controllerDesc the given controller descriptor.
   * @param contentDesc the given content descriptor.
   * @return the initialization content class.
   * @throws InstantiationException when problems obscur.
   */

  public Class getInitializationContextClass (
    final Type type,
    final Object controllerDesc,
    final Object contentDesc) throws InstantiationException
  {
    Object loader;
    Object controller;

    if (controllerDesc instanceof Object[]) {
      loader = getFcLoader(((Object[])controllerDesc)[0]);
      controller = ((Object[])controllerDesc)[1];
    } else {
      loader = getFcLoader(null);
      controller = controllerDesc;
    }

    // generates the component descriptor
    Tree typeTree = getFcTypeDescriptor(type);
    Tree controllerTree = getFcControllerDescriptor(controller);
    Tree contentTree = getFcContentDescriptor(contentDesc);
    Tree controllerDescTree = new Tree((String)controller);
    Tree controllerWithDescTree = new Tree(new Tree[] {controllerTree,controllerDescTree});
    Tree componentTree = new Tree(new Tree[] {
      new Tree("org.objectweb.fractal.julia.asm.ContextClassGenerator"),
      typeTree,
      controllerWithDescTree, //controllerTree,
      contentTree
    });

    // creates an instance of the corresponding class
    // (a specific InitializationContext class for components of this kind)
    try {
      return _this_weaveableL.loadClass(componentTree, loader);
    } catch (Exception e) {
      throw new ChainedInstantiationException(
        e,
        null,
        "Cannot load the specific InitializationContext sub class " +
        "needed to create the component");
    }
  }

  /**
   * Create a new {@link Component} instance for a given
   * initialization context class and content descriptor.
   *
   * @param initializationContextClass the given initialization context class.
   * @param contentDesc the given content descriptor.
   * @return the new {@link Component} instance.
   * @throws InstantiationException when problems obscur.
   */

  public Component newComponent (
    final Class initializationContextClass,
    final Object contentDesc) throws InstantiationException
  {
    InitializationContext initializationContext;
    try {
      initializationContext =
        (InitializationContext)initializationContextClass.newInstance();
    } catch (Exception e) {
      throw new ChainedInstantiationException(
        e,
        null,
        "Cannot create the specific InitializationContext object " +
        "needed to create the component");
    }

    // initializes the initialization context itself
    if (!(contentDesc instanceof String)) {
      initializationContext.content = contentDesc;
    }
    initializationContext.hints = _this_weaveableTF;
    initializationContext.create();

    // initializes all the controller objects of the component
    List controllers = initializationContext.controllers;
    for (int i = 0; i < controllers.size(); ++i) {
      Object o = controllers.get(i);
      if (o instanceof Controller) {
        ((Controller)o).initFcController(initializationContext);
      }
    }

    // returns the Component interface of the component
    try {
      Component c = (Component)initializationContext.getInterface("component");
      return (Component)c.getFcInterface("component");
    } catch (NoSuchInterfaceException e) {
      throw new ChainedInstantiationException(
        e,
        null,
        "The created component does not provide the Component interface");
    }
  }

  public Object getFcLoader (final Object loader) {
    return loader;
  }
  
  /**
   * Returns a tree representing the given component type. This tree has the
   * format specified in {@link
   * org.objectweb.fractal.julia.asm.ContextClassGenerator}
   *
   * @param type a component type, must be instance of {@link ComponentType}.
   * @return a tree representing the given component type.
   * @throws InstantiationException if the given type is not an instance of
   *      {@link ComponentType}, or if it contains control interface types.
   */

  public Tree getFcTypeDescriptor (final Type type)
    throws InstantiationException
  {
    // checks the type system
    ComponentType compType;
    try {
      compType = (ComponentType)type;
    } catch (ClassCastException e) {
      throw new ChainedInstantiationException(
        e, null, "The component type must be an instance of ComponentType");
    }

    // checks the component type
    InterfaceType[] itfTypes = compType.getFcInterfaceTypes();
    Tree[] itfTrees = new Tree[itfTypes.length];
    for (int i = 0; i < itfTypes.length; i++) {
      InterfaceType itfType = itfTypes[i];
      String itfName = itfType.getFcItfName();
      if (!itfName.equals("attribute-controller")) {
        if (itfName.equals("component") || itfName.endsWith("-controller")) {
          throw new ChainedInstantiationException(
            null,
            null,
            "The component type must not contain control interface types");
        }
      }
      itfTrees[i] = new Tree(new Tree[] {
        new Tree(itfName),
        new Tree(itfType.getFcItfSignature()),
        new Tree(itfType.isFcClientItf() ? "true" : "false"),
        new Tree(itfType.isFcOptionalItf() ? "true" : "false"),
        new Tree(itfType.isFcCollectionItf() ? "true" : "false")
      });
    }

    // returns the type descriptor
    return new Tree(itfTrees);
  }

  /**
   * Returns the tree corresponding to the given controller descriptor. This
   * tree is found by using the {@link #_this_weaveableL} loader. It must have
   * the format specified in {@link
   * org.objectweb.fractal.julia.asm.ContextClassGenerator}
   *
   * @param controllerDesc a string referencing a true controller descriptor.
   * @return the tree corresponding to the given controller descriptor.
   * @throws InstantiationException if the tree cannot be loaded.
   */

  public Tree getFcControllerDescriptor (final Object controllerDesc)
    throws InstantiationException
  {
    Map definitions = new HashMap();
    definitions.put("attributeControllerInterface", new Tree("QUOTE"));
    definitions.put("interfaceName", new Tree("QUOTE"));
    try {
      return _this_weaveableL.evalTree(
        _this_weaveableL.loadTree((String)controllerDesc), definitions);
    } catch (Exception e) {
      throw new ChainedInstantiationException(
        e,
        null,
        "Cannot load the '" + controllerDesc + "' controller descriptor");
    }
  }

  /**
   * Returns the tree corresponding to the given content descriptor. This tree
   * has the format specified in {@link
   * org.objectweb.fractal.julia.asm.ContextClassGenerator}
   *
   * @param contentDesc a content descriptor.
   * @return the tree corresponding to the given content descriptor.
   */

  public Tree getFcContentDescriptor (final Object contentDesc) {
    if (contentDesc instanceof String) {
      return new Tree((String)contentDesc);
    } else if (contentDesc != null && !(contentDesc instanceof Object[])) {
      return new Tree(contentDesc.getClass().getName());
    } else {
      return new Tree("EMPTY");
    }
  }

  // -------------------------------------------------------------------------
  // Fields and methods required by the mixin class in the base class
  // -------------------------------------------------------------------------

  /**
   * The <tt>weaveableL</tt> field required by this mixin. This field is
   * supposed to reference the {@link Loader} interface of the component to
   * which this controller object belongs.
   */

  public Loader _this_weaveableL;

  /**
   * The <tt>weaveableTF</tt> field required by this mixin. This field is
   * supposed to reference the {@link TypeFactory} interface of the component to
   * which this controller object belongs.
   */

  public TypeFactory _this_weaveableTF;
}
