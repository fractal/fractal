/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;

import org.objectweb.fractal.julia.factory.ChainedInstantiationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A context used to initialize {@link Controller} objects.
 * @see Controller#initFcController initFcController
 */

public class InitializationContext {

  /**
   * The type of the component to which the controller object belongs.
   */

  public Type type;

  /**
   * The controllers objects of the component to which the controller object
   * belongs.
   */

  public List controllers;

  /**
   * The external interfaces of the component to which the controller object
   * belongs.
   */

  public Map interfaces;

  /**
   * The internal interfaces of the component to which the controller object
   * belongs.
   */

  public Map internalInterfaces;

  /**
   * The content of the component to which the controller object belongs.
   */

  public Object content;

  /**
   * Miscellaneous additional context information.
   */

  public Object hints;

  /**
   * Constructs an uninitialized {@link InitializationContext} object.
   */

  public InitializationContext () {
  }

  /**
   * Initializes and fills in the fields of this object. This method must create
   * the type, the controller objects and the interfaces of a component. The
   * default implementation of this method does nothing.
   *
   * @throws InstantiationException if the component type, a controller object
   *      or a component interface cannot be created.
   */

  public void create () throws InstantiationException {
    controllers = new ArrayList();
    interfaces = new HashMap();
    internalInterfaces = new HashMap();
  }

  /**
   * Returns the component interface of the {@link #interfaces interfaces} map
   * whose name is given.
   *
   * @param name the name of a component interface.
   * @return the interface whose name is given.
   * @throws InstantiationException if there is no such component interface.
   */

  public Object getInterface (final String name) throws InstantiationException {
    Object o = getOptionalInterface(name);
    if (o == null) {
      throw new ChainedInstantiationException(
        null,
        (Component)getOptionalInterface("component"),
        "Cannot find the required interface '" + name + '\'');
    }
    return o;
  }

  /**
   * Returns the component interface of the {@link #interfaces interfaces} map
   * whose name is given.
   *
   * @param name the name of a component interface.
   * @return the interface whose name is given, or <tt>null</tt> if there is no
   *      such component interface.
   */

  public Object getOptionalInterface (final String name) {
    Object o = interfaces.get(name);
    if (o instanceof ComponentInterface) {
      o = ((ComponentInterface)o).getFcItfImpl();
    }
    return o;
  }
}
