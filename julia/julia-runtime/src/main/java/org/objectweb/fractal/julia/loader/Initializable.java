package org.objectweb.fractal.julia.loader;

/**
 * The interface used by the {@link Loader#newObject newObject} method to
 * initialize some objects. See {@link Loader#newObject newObject}.
 */

public interface Initializable {

  /**
   * Initializes this object with the given arguments.
   *
   * @param args the arguments to be used to initialize this object. The format
   *      of these arguments depends on the class of this object.
   * @throws Exception if a problem occurs during the object initialization.
   */

  void initialize (Tree args) throws Exception;
}
