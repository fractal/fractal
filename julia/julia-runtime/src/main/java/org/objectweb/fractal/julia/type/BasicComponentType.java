/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 * Copyright (C) 2007 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@objectweb.org
 *
 * Author: Eric Bruneton
 *
 * Contributor: Philippe Merle
 *
 * $Id$
 */

package org.objectweb.fractal.julia.type;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.ChainedNoSuchInterfaceException;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;

import java.io.Serializable;

/**
 * Provides a basic implementation of the {@link ComponentType} interface.
 */

public class BasicComponentType implements ComponentType, Serializable {

  /**
   * The types of the interfaces of components of this type.
   */

  private final InterfaceType[] interfaceTypes;

  // -------------------------------------------------------------------------
  // Constructors
  // -------------------------------------------------------------------------

  /**
   * Constructs a {@link BasicComponentType} object.
   *
   * @param itfTypes the types of the interfaces of components of this type.
   * @throws InstantiationException if two interface types have the same name,
   *      or if the name of an interface type is a prefix of the name of a
   *      collection interface type.
   */

  public BasicComponentType (final InterfaceType[] itfTypes)
    throws InstantiationException
  {
    interfaceTypes = clone(itfTypes);
    // verifications
    for (int i = 0; i < interfaceTypes.length; ++i) {
      String p = interfaceTypes[i].getFcItfName();
      boolean collection = interfaceTypes[i].isFcCollectionItf();
      for (int j = i + 1; j < interfaceTypes.length; ++j) {
        String q = interfaceTypes[j].getFcItfName();
        if (p.equals(q)) {
          throw new ChainedInstantiationException(
            null, null, "Two interfaces have the same name '" + q + "'");
        }
        if (collection && q.startsWith(p)) {
          throw new ChainedInstantiationException(
            null,
            null,
            "The name of the interface '" + q + "' starts with '" +
            p + "', which is the name of a collection interface");
        }
        if (interfaceTypes[j].isFcCollectionItf() && p.startsWith(q)) {
          throw new ChainedInstantiationException(
            null,
            null,
            "The name of the interface '" + p + "' starts with '" +
            q + "', which is the name of a collection interface");
        }
      }
    }
  }

  // -------------------------------------------------------------------------
  // Implementation of the ComponentType interface
  // -------------------------------------------------------------------------

  public InterfaceType[] getFcInterfaceTypes () {
    return clone(interfaceTypes);
  }

  public InterfaceType getFcInterfaceType (final String name)
    throws NoSuchInterfaceException
  {
    for (int i = 0; i < interfaceTypes.length; ++i) {
      InterfaceType itfType = interfaceTypes[i];
      String itfName = itfType.getFcItfName();
      if (itfType.isFcCollectionItf()) {
        if (name.startsWith(itfName)) {
          return itfType;
        }
      } else if (name.equals(itfName)) {
        return itfType;
      }
    }
    throw new ChainedNoSuchInterfaceException(null, null, name);
  }

  public boolean isFcSubTypeOf (final Type type) {
    try {
      if (type instanceof ComponentType) {
        ComponentType t = (ComponentType)type;
        InterfaceType[] itfs = interfaceTypes;
        for (int i = 0; i < itfs.length; ++i) {
          InterfaceType i1 = itfs[i];
          if (i1.isFcClientItf()) {
            InterfaceType i2 = t.getFcInterfaceType(i1.getFcItfName());
            if (!i1.isFcSubTypeOf(i2)) {
              return false;
            }
          }
        }
        itfs = t.getFcInterfaceTypes();
        for (int i = 0; i < itfs.length; ++i) {
          InterfaceType i2 = itfs[i];
          if (!i2.isFcClientItf()) {
            InterfaceType i1 = getFcInterfaceType(i2.getFcItfName());
            if (!i1.isFcSubTypeOf(i2)) {
              return false;
            }
          }
        }
        return true;
      }
    } catch (NoSuchInterfaceException e) {
    }
    return false;
  }

  /**
   * Returns a copy of the given interface type array. This method is used to
   * return a copy of the field of this class, instead of the field itself, so
   * that its content can not be modified from outside.
   *
   * @param types the array to be cloned.
   * @return a clone of the given array, or an empty array if <tt>type</tt> is
   *      <tt>null</tt>.
   */

  private static InterfaceType[] clone (final InterfaceType[] types) {
    if (types == null) {
      return new InterfaceType[0];
    } else {
      InterfaceType[] clone = new InterfaceType[types.length];
      System.arraycopy(types, 0, clone, 0, types.length);
      return clone;
    }
  }

  /**
   * Return a string representation of this component type.
   */
  public String toString() {
      String s = "[";
      InterfaceType[] its = getFcInterfaceTypes();
      boolean first = true;
      for (int i = 0; i < its.length; i++) {
          if(first) {
              first = false;
          }
          else {
              s += ",";
          }
          s += its[i].toString();
      }
      s += "]";
      return s;
  }

  public boolean equals (final Object o) {
      if (o instanceof ComponentType) {
	      InterfaceType[] it = ((ComponentType)o).getFcInterfaceTypes();
	      if(interfaceTypes.length != it.length)
	         return false;
	      for(int i=0; i<it.length; i++) {
	        if(!interfaceTypes[i].equals(it[i])) {
	    	    return false;
	    	}
	      }
	      return true;
      }
      return false;
  }

  public int hashCode() {
	  if(interfaceTypes.length == 0)
		  return 1;
	  int result = interfaceTypes[0].hashCode();
	  for(int i=1; i<interfaceTypes.length; i++) {
		  result = result * interfaceTypes[i].hashCode();
	  }
	  return result;
  }
}
