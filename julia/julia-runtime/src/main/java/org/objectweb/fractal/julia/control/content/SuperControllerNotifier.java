/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.SuperController;

/**
 * An interface used to notify a component when it is added or removed from a
 * component.
 */

public interface SuperControllerNotifier extends SuperController {

  /**
   * Notifies this component that it has been added in the given component.
   *
   * @param parent the component into which this component has been added.
   */

  void addedToFc (Component parent);

  /**
   * Notifies this component that it has been removed from the given component.
   *
   * @param parent the component from which this component has been removed.
   */

  void removedFromFc (Component parent);
}
