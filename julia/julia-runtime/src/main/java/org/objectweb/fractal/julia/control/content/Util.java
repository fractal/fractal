package org.objectweb.fractal.julia.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;

/**
 * Provides static utility methods related to {@link ContentController}.
 */

public class Util {

  private Util () {
  }

  /**
   * Returns all the direct and indirect sub components of the given component.
   *
   * @param component a component.
   * @return all the direct and indirect sub components of the given component.
   */

  public static List getAllSubComponents (final Component component) {
		HashSet map = new HashSet();
    List allSubComponents = new ArrayList();
    List stack = new ArrayList();
    stack.add(component);
    while (stack.size() > 0) {
      int index = stack.size() - 1;
      Component c = (Component)stack.get(index);
      stack.remove(index);
      if (!map.contains(c)) {
        try {
          ContentController cc =
            (ContentController)c.getFcInterface("content-controller");
          Component[] subComponents = cc.getFcSubComponents();
          for (int i = subComponents.length - 1; i >= 0; --i) {
            stack.add(subComponents[i]);
          }
        } catch (NoSuchInterfaceException ignored) {
          // c is not a composite component: nothing to do
        }
        allSubComponents.add(c);
        map.add(c);
      }
    }
    return allSubComponents;
  }
}
