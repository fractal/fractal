/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal[at]objectweb.org
 *
 * Author: Eric Bruneton
 * Contributor: Romain Rouvoy
 */

package org.objectweb.fractal.julia;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.factory.BasicGenericFactoryMixin;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.type.BasicTypeFactoryMixin;

/**
 * Provides access to the Julia bootstrap component.
 */

public class Julia implements Factory, GenericFactory {

  /** Using the DynamicLoader class as Julia's default loader. */
  public static final String DEFAULT_LOADER = "org.objectweb.fractal.julia.loader.DynamicLoader";
  
  /**
   * The bootstrap component.
   */

  private static Component bootstrapComponent;

  // -------------------------------------------------------------------------
  // Implementation of the Factory interface
  // -------------------------------------------------------------------------

  /**
   * @return <tt>null</tt>.
   */

  public Type getFcInstanceType () {
    return null;
  }

  /**
   * @return <tt>null</tt>.
   */

  public Object getFcControllerDesc () {
    return null;
  }

  /**
   * @return <tt>null</tt>.
   */

  public Object getFcContentDesc () {
    return null;
  }

  /**
   * Returns the Julia bootstrap component. If this component does not exists
   * yet, it is created as follows:
   * <ul>
   * <li>a pre bootstrap component is created by assembling a {@link Loader}
   * object, a {@link BasicTypeFactoryMixin} object, and a {@link
   * BasicGenericFactoryMixin} object. The loader object is created by
   * instantiating the class specified in the "julia.loader" system
   * property.</li>
   * <li>the pre bootstrap component is used to create the real bootstrap
   * component, by calling the <tt>newFcInstance</tt> method of the
   * <tt>GenericFactory</tt> interface of the pre bootstrap component, with the
   * "bootstrap" string as controller descriptor.</li>
   * </ul>
   * 
   * @return the {@link Component} interface of the component instantiated from
   *      this factory.
   * @throws InstantiationException if the component cannot be created.
   */

  public Component newFcInstance () throws InstantiationException {
    return newFcInstance(new HashMap());
  }

  /**
   * Returns the Julia bootstrap component. If this component does not exists
   * yet, it is created as follows:
   * <ul>
   * <li>a pre bootstrap component is created by assembling a {@link Loader}
   * object, a {@link BasicTypeFactoryMixin} object, and a {@link
   * BasicGenericFactoryMixin} object. The loader object is created by
   * instantiating the class specified in the "julia.loader" system
   * property, or associated to the "julia.loader" key in the contentDesc 
   * Map.</li>
   * <li>the pre bootstrap component is used to create the real bootstrap
   * component, by calling the <tt>newFcInstance</tt> method of the
   * <tt>GenericFactory</tt> interface of the pre bootstrap component, with the
   * "bootstrap" string as controller descriptor.</li>
   * </ul>
   * 
   * @param type ignored.
   * @param controllerDesc ignored.
   * @param contentDesc an optional Map.
   * @return the {@link Component} interface of the component instantiated from
   *      this factory.
   * @throws InstantiationException if the component cannot be created.
   */

  public Component newFcInstance (
    final Type type, 
    final Object controllerDesc, 
    final Object contentDesc) throws InstantiationException 
  {
    Map context;
    if (contentDesc instanceof Map) {
      context = (Map)contentDesc;
    } else {
      context = new HashMap();
    }
    return newFcInstance(context);
  }

  private Component newFcInstance (final Map context) 
    throws InstantiationException 
  {
    if (bootstrapComponent == null) {
      String boot = (String)context.get("julia.loader");
      if (boot == null) {
        boot = System.getProperty("julia.loader");
      } 
      if (boot == null) {
        boot = DEFAULT_LOADER;
//      throw new InstantiationException(
//        "The julia.loader [system] property is not defined");
      }

      // creates the pre bootstrap controller components
      Loader loader;
      try {
        loader = (Loader)_forName(boot).newInstance();
        loader.init(context);
      } catch (Exception e) {
        throw new InstantiationException(
          "Cannot find or instantiate the '" + boot +
          "' class specified in the julia.loader [system] property");
      }
      BasicTypeFactoryMixin typeFactory = new BasicTypeFactoryMixin();
      BasicGenericFactoryMixin genericFactory = new BasicGenericFactoryMixin();
      genericFactory._this_weaveableL = loader;
      genericFactory._this_weaveableTF = typeFactory;

      // use the pre bootstrap component to create the real bootstrap component
      ComponentType t = typeFactory.createFcType(new InterfaceType[0]);
      try {
        bootstrapComponent = genericFactory.newFcInstance(t, "bootstrap", null);
        try {
          ((Loader)bootstrapComponent.getFcInterface("loader")).init(context);
        } catch (NoSuchInterfaceException ignored) {
        }
      } catch (Exception e) {
        throw new ChainedInstantiationException(
          e, null, "Cannot create the bootstrap component");
      }
    }
    return bootstrapComponent;
  }

  /*
   * Convenient method used for J2ME conversion
   * (ClassLoader not available in CDLC)
   */

  private Class _forName (final String name) throws ClassNotFoundException {
    return getClass().getClassLoader().loadClass(name);
  }
}
