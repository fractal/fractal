package org.objectweb.fractal.julia;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;

/**
 * Provides static utility methods related to {@link Component}.
 */

public class Util {

  private Util () {
  }

  /**
   * Computes a string representation of the given component.
   *
   * @param component a component.
   * @param buf the buffer to be used to store the result of this method.
   */

  public static void toString (
    final Component component,
    final StringBuffer buf)
  {
    try {
      NameController nc =
        (NameController)component.getFcInterface("name-controller");
      try {
        SuperController sc =
          (SuperController)component.getFcInterface("super-controller");
        Component[] superComponents = sc.getFcSuperComponents();
        if (superComponents.length > 0) {
          toString(superComponents[0], buf);
        }
      } catch (NoSuchInterfaceException ignored) {
      }
      buf.append('/');
      buf.append(nc.getFcName());
    } catch (NoSuchInterfaceException e) {
      buf.append(component.toString());
      return;
    }
  }
}
