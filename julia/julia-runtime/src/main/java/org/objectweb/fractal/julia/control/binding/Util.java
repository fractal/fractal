/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.binding;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.type.InterfaceType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Provides static utility methods related to {@link BindingController}.
 */

public class Util {

  // -------------------------------------------------------------------------
  // Private constructor
  // -------------------------------------------------------------------------

  private Util () {
  }

  // -------------------------------------------------------------------------
  // Static methods
  // -------------------------------------------------------------------------

  /**
   * Returns the client interfaces that are bound to the given server interface.
   *
   * @param serverItf a server interface.
   * @return the client interfaces that are bound to serverItf.
   * @throws Exception if a problem occurs.
   */

  public static Set getFcClientItfsBoundTo (final Interface serverItf)
    throws Exception
  {
    Set result = new HashSet();
    Object[] comps = getFcPotentialClientsOf(serverItf).toArray();
    for (int i = 0; i < comps.length; ++i) {
      Component comp = (Component)comps[i];
      List clientItfs = getFcClientItfsBoundTo(comp, serverItf);
      for (int j = 0; j < clientItfs.size(); ++j) {
        result.add(clientItfs.get(j));
      }
    }
    return result;
  }

  /**
   * Returns the potential client components of the given server interface.
   *
   * @param serverItf a server interface.
   * @return the potential client components of the given server interface.
   * @throws Exception if a problem occurs.
   */

  public static Set getFcPotentialClientsOf (final Interface serverItf)
    throws Exception
  {
    Set compSet = new HashSet();
    Component serverComp = serverItf.getFcItfOwner();
    if (serverItf.isFcInternalItf()) {
      ContentController cc;
      try {
        cc = (ContentController)serverComp.getFcInterface("content-controller");
      } catch (NoSuchInterfaceException e) {
        throw new Exception("Cannot create shortcuts");
      }
      Component[] comps = cc.getFcSubComponents();
      for (int i = 0; i < comps.length; ++i) {
        compSet.add(comps[i]);
      }
      compSet.add(serverComp);
    } else {
      SuperController sc;
      try {
        sc = (SuperController)serverComp.getFcInterface("super-controller");
      } catch (NoSuchInterfaceException e) {
        throw new Exception("Cannot create shortcuts");
      }
      Component[] superComps = sc.getFcSuperComponents();
      for (int i = 0; i < superComps.length; ++i) {
        compSet.add(superComps[i]);
        ContentController cc;
        try {
          cc = (ContentController)superComps[i].
            getFcInterface("content-controller");
        } catch (NoSuchInterfaceException e) {
          throw new Exception("Cannot create shortcuts");
        }
        Component[] subComps = cc.getFcSubComponents();
        for (int j = 0; j < subComps.length; ++j) {
          compSet.add(subComps[j]);
        }
      }
    }
    return compSet;
  }

  /**
   * Returns the client interfaces of the given component that are  bound to
   * the given server interface.
   *
   * @param component a component.
   * @param serverItf a server interface.
   * @return the client interfaces of <tt>component</tt> that are bound to
   *      <tt>serverItf</tt>.
   * @throws Exception if a problem occurs.
   */

  public static List getFcClientItfsBoundTo (
    final Component component,
    final Interface serverItf) throws Exception
  {
    List itfList = new ArrayList();
    BindingController bc;
    try {
      bc = (BindingController)component.getFcInterface("binding-controller");
    } catch (NoSuchInterfaceException e) {
      return itfList;
    }

    // search in external client interfaces
    Object[] itfs = component.getFcInterfaces();
    for (int i = 0; i < itfs.length; ++i) {
      Interface itf;
      InterfaceType itfType;
      try {
        itf = (Interface)itfs[i];
        itfType = (InterfaceType)itf.getFcItfType();
      } catch (ClassCastException e) {
        throw new Exception("Cannot create shortcuts");
      }
      if (!itfType.isFcClientItf()) {
        continue;
      }
      Object sItf;
      try {
        sItf = bc.lookupFc(itf.getFcItfName());
      } catch (NoSuchInterfaceException e) {
        continue;
      }
      if (serverItf.equals(sItf)) {
        itfList.add(itf);
      }
    }

    ContentController cc;
    try {
      cc = (ContentController)component.getFcInterface("content-controller");
    } catch (NoSuchInterfaceException e) {
      return itfList;
    }

    // search in internal client interfaces
    itfs = cc.getFcInternalInterfaces();
    for (int i = 0; i < itfs.length; ++i) {
      Interface itf;
      InterfaceType itfType;
      try {
        itf = (Interface)itfs[i];
        itfType = (InterfaceType)itf.getFcItfType();
      } catch (ClassCastException e) {
        throw new Exception("Cannot create shortcuts");
      }
      if (!itfType.isFcClientItf()) {
        continue;
      }
      Object sItf;
      try {
        sItf = bc.lookupFc(itf.getFcItfName());
      } catch (NoSuchInterfaceException e) {
        continue;
      }
      if (serverItf.equals(sItf)) {
        itfList.add(itf);
      }
    }
    return itfList;
  }
}
