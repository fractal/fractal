/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 *
 * Author: Eric Bruneton
 */

package org.objectweb.fractal.julia.control.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

/**
 * An extended {@link LifeCycleController} interface. This interface is
 * used to stop several components simultaneously, in order to avoid the
 * deadlocks that can occur when several components are stopped one by one
 * (for example, if a component C is stopped after a component S, and if a
 * thread T is executing a method m1 in C which calls a method m2 in S, then T
 * can be blocked in m2 just after S is stopped but before C is stopped, in
 * which case C can not be stopped, and a deadlock occurs).
 */

public interface LifeCycleCoordinator extends LifeCycleController {

  /**
   * Sets the state of this component, <i>but not of its sub components</i>, to
   * {@link #STARTED STARTED}. Because of component sharing, the {@link
   * #startFc startFc} method cannot be implemented by just calling
   * itself recursively on the sub components of this componets (otherwise some
   * sub components may be started several times). Hence this method.
   *
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STARTED STARTED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  boolean setFcStarted () throws IllegalLifeCycleException;

  /**
   * Sets the state of this component, <i>but not of its sub components</i>, to
   * "<tt>STOPPING</tt>". After this method has been called, and while the
   * {@link #setFcStopped setFcStopped} has not been called, this component must
   * notify the given coordinator when it becomes inactive, and when it is about
   * to become active, with the {@link #fcInactivated fcInactivated} and {@link
   * #fcActivated fcActivated} methods.
   * <br>
   * Note: the "<tt>STOPPING</tt>" state is an internal state that is not
   * visible from the outside. Indeed, in this state, the {@link #getFcState
   * getFcState} method should return {@link #STARTED STARTED}, as the component
   * is not yet stopped.
   *
   * @param coordinator the coordinator that must be notified when this
   *      component becomes inactive, and when it is about to become active.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  void setFcStopping (LifeCycleCoordinator coordinator)
    throws IllegalLifeCycleException;

  /**
   * Sets the state of this component, <i>but not of its sub components</i>, to
   * {@link #STOPPED STOPPED}. Because of component sharing, the {@link
   * #stopFc stopFc} method cannot be implemented by just calling
   * itself recursively on the sub components of this componets (otherwise some
   * sub components may be stopped several times). Hence this method.
   *
   * @return <tt>true</tt> if the execution state has changed, or <tt>false</tt>
   *      if it had already the {@link #STOPPED STOPPED} value.
   * @throws IllegalLifeCycleException if a problem occurs.
   */

  boolean setFcStopped () throws IllegalLifeCycleException;

  /**
   * Notifies this component that the given component is about to become active.
   * This method must be called by a component when the following conditions are
   * met:
   * <ul>
   * <li>the component is stopping, i.e., its {@link #setFcStopping
   * setFcStopping} method has been called.</li>
   * <li>the component is not yet stopped, i.e., its {@link #setFcStopped
   * setFcStopped} method has not been called yet.</li>
   * <li>the component is currently inactive, but a new activity is about to
   * start in this component.</li>
   * </ul>
   *
   * @param component the {@link LifeCycleCoordinator} interface of a component
   *      that is about to become active.
   * @return <tt>true</tt> if the given component can become active, or
   *      <tt>false</tt> if the new activity must be delayed until the component
   *      is restarted.
   */

  boolean fcActivated (LifeCycleCoordinator component);

  /**
   * Notifies this component that the given component has become inactive.
   * This method must be called by a component when the following conditions are
   * met:
   * <ul>
   * <li>the component is stopping, i.e., its {@link #setFcStopping
   * setFcStopping} method has been called.</li>
   * <li>the component is not yet stopped, i.e., its {@link #setFcStopped
   * setFcStopped} method has not been called yet.</li>
   * <li>the component has become inactive.</li>
   * </ul>
   *
   * @param component the {@link LifeCycleCoordinator} interface of a component
   *      that has become inactive.
   */

  void fcInactivated (LifeCycleCoordinator component);
}
