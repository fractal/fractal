This example illustrates the features of Julia for the component-based
engineering of control membranes.

The four existing subpackages illustrates:
- intropect: the introspection of a control membrane,
- glue: the gluing of a control membrane to a content,
- interceptor: the dynamic management of interceptors,
- mix: the joint use of object-oriented and component-based control membrane.

These features are presented in Section 9 of the Java API documentation.
See <http://fractal.objectweb.org/current/doc/javadoc/julia/overview-summary.html#9>.
