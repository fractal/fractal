/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact : dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Vivien Quema
 */

package membrane.glue;

/**
 * Controller interface to control
 * {@link org.objectweb.util.monolog.api.Logger loggers }associated with the
 * component this controller belong. Every loggers define in a component shared
 * the same base name and have a suffix (can be <code>null</code>).
 */
public interface LoggerController
{
  /**
   * Logger level for debug message. <br/>This constant can be used to change
   * logger level remotly. Indeed, monolog
   * {@link org.objectweb.util.monolog.api.BasicLevel }levels must be
   * initialized by monolog factories. So if this interface is used remotly, the
   * caller may not have initialized it.
   */
  int DEBUG   = 0;
  /** Logger level for info message. */
  int INFO    = 1;
  /** Logger level for warning message. */
  int WARN    = 2;
  /** Logger level for error message. */
  int ERROR   = 3;
  /** Logger level for fatal message. */
  int FATAL   = 4;
  /**
   * Logger level indicates that the level is inherited from logger's ancestors.
   */
  int INHERIT = 5;

  /**
   * Sets the loggers' base name.
   * 
   * @param name the base name
   */
  void setBaseName(String name);

  /**
   * Returns the loggers' base name.
   * 
   * @return the loggers' base name.
   */
  String getBaseName();

  /**
   * Returns the current logging level of a logger
   * 
   * @param loggerName the name of the logger
   * @return the current logging level of a logger. Returns <code>INHERIT</code>
   *         if given name is unknown.
   */
  int getLoggerLevel(String loggerName);

  /**
   * Sets the logging level of a logger.
   * 
   * @param loggerName the name of the logger
   * @param level a level
   */
  void setLoggerLevel(String loggerName, int level);

  /**
   * Returns the name of every registered loggers.
   * 
   * @return an array of registered loggers name.
   */
  String[] getLoggerNames();
}