/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): 
 */

package membrane.glue;

/**
 * Utility class for logger controller.
 */
public final class Util
{

  private static int nextUnnamedBaseName = 0;

  private Util()
  {
  }

  /**
   * Returns a unique logger base name for an unnamed component.
   * 
   * @return a unique logger base name for an unnamed component.
   */
  public static String getNextUnnamedBaseName()
  {
    return "unnamedComponent." + (nextUnnamedBaseName++);
  }
}