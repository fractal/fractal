/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): 
 */

package membrane.glue;

/**
 * Interface implemented by logger controller to allow other controllers and
 * content class to register as <i>logger controller client <i>.
 */
public interface LoggerControllerRegister
{

  /**
   * Registers a loggable as client of this logger controller for the specified
   * logger name.
   * 
   * @param loggerName a logger name
   * @param loggable a loggable object (controller or content class).
   */
  void register(String loggerName, Loggable loggable);

  /**
   * Un registers a loggable.
   * 
   * @param loggerName the registred logger name
   * @param loggable the registred loggable object (controller or content
   *          class).
   */
  void unregister(String loggerName, Loggable loggable);
}