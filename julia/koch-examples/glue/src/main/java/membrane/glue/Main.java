/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package membrane.glue;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.factory.Factory;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.koch.factory.MembraneFactory;
import org.objectweb.fractal.util.Fractal;

/**
 * This class illustrates the definition of new control membranes.
 * 
 * This example defines a control membrane which extends the control membrane of
 * primitive components with a log controller (as defined by the
 * <a href="http://dream.objectweb.org">Dream</a> framework).
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        
        Component boot = Fractal.getBootstrapComponent();
        TypeFactory tf = Fractal.getTypeFactory(boot);
        GenericFactory gf = Fractal.getGenericFactory(boot);
        MembraneFactory mf = (MembraneFactory) boot.getFcInterface("membrane-factory");
        
        /*
         * Create the component type.
         */
        ComponentType type =
            tf.createFcType(
                new InterfaceType[]{
                    tf.createFcItfType(
                        "r", Runnable.class.getName(), false, false, false)
                }
            );
        
        /*
         * Create a control membrane (a component) and glue it to the content.
         * The result is a application-level component equipped with a log
         * controller.
         */
        Factory lp = (Factory)
            Class.forName("membrane.glue.LoggablePrimitive").newInstance();
        ((BindingController)lp).bindFc("type-factory", tf);
        ((BindingController)lp).bindFc("generic-factory", gf);
        Component membrane = lp.newFcInstance();
        Component c1 = gf.newFcInstance(type, membrane, C.class.getName());
        run(c1);
        
        /*
         * Register a new membrane definition (loggablePrimitive).
         * Use this new control descriptor to create a component with a log
         * log.
         */
        String adl = "membrane.glue.LoggablePrimitive";
        mf.registerFcNewMembraneDef("loggablePrimitive", adl);
        Component c2 = gf.newFcInstance(type, "loggablePrimitive", C.class.getName());
        run(c2);
    }
    
    private static void run( Component c ) throws Exception {
        /*
         * Register the component with the logger controller.
         */
        LoggerControllerRegister lcr = (LoggerControllerRegister)
            c.getFcInterface("/logger-controller-register");
        Loggable content = (Loggable) c.getFcInterface("/content");
        lcr.register("myLogger", content);
        
        /*
         * Start and run the application.
         */
        Fractal.getLifeCycleController(c).startFc();
        ((Runnable)c.getFcInterface("r")).run();   
    }
}
