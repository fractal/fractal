/***
 * Fractal Hello World Example
 * Copyright (C) 2001-2002 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Eric.Bruneton@rd.francetelecom.com
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Eric Bruneton
 * Author: Lionel Seinturier
 */

package membrane.interceptor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;


public class ClientImpl implements Runnable, BindingController {

    public void run () {
        System.err.println( "Hello World from " + this );
      
        /*
         * TODO: modify bindings when an output interceptor is added.
         * Thus, invoking s.run() skips the added output interceptor.
         * To ensure that no output interceptor is skipped, invoke the service via
         * the client interface.
         */
        try {
            ((Runnable)component.getFcInterface("s")).run();
        }
        catch (NoSuchInterfaceException e) {
            System.err.println("<< Unexpected NoSuchInterfaceException: "+e.getMessage()+" >>");
        }
      
//        s.run();
    }

    private Component component;
    private Runnable s;
  
    public String[] listFc () { return new String[] { "s" }; }
    public Object lookupFc (String itfName) {
        if (itfName.equals("s")) { return s; }
        else return null;
    }
    public void bindFc (String itfName, Object itfValue) {
        if (itfName.equals("component")) { component = (Component)itfValue; }
        if (itfName.equals("s")) { s = (Runnable)itfValue; }
    }
    public void unbindFc (String itfName) {
        if (itfName.equals("s")) { s = null; }
    }
}
