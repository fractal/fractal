/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package membrane.interceptor;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.koch.control.interceptor.InterceptorController;
import org.objectweb.fractal.util.Fractal;

/**
 * This class illustrates the usage of the interceptor controller for
 * dynamically adding and removing an interceptor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        
        // ----------------------------------------------------------
        // Get the bootstrap component and factories
        // ----------------------------------------------------------
        Component boot = Fractal.getBootstrapComponent();
        TypeFactory tf = Fractal.getTypeFactory(boot);
        GenericFactory cf = Fractal.getGenericFactory(boot);
        
        // ----------------------------------------------------------
        // Create the component types
        // ----------------------------------------------------------
        ComponentType rType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType(
                   "m",
                   "java.lang.Runnable",
                   false,   // server interface
                   false,   // mandatory
                   false)   // singleton
              });
        
        ComponentType cType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType("m", "java.lang.Runnable", false, false, false),
                tf.createFcItfType("s", Runnable.class.getName(), true, false, false)
        });
        ComponentType sType = tf.createFcType(new InterfaceType[] {
                tf.createFcItfType("s", Runnable.class.getName(), false, false, false)
        });
        
        // ----------------------------------------------------------
        // Instantiate the components
        // ----------------------------------------------------------
        Component rComp = cf.newFcInstance(
                rType,
                "composite",
                null);
        
        Component cComp = cf.newFcInstance(cType, "primitive", ClientImpl.class.getName());
        Component sComp = cf.newFcInstance(sType, "parametricPrimitive", ServerImpl.class.getName());
        
        // ----------------------------------------------------------
        // Set component names
        // ----------------------------------------------------------
        Fractal.getNameController(rComp).setFcName("root");
        Fractal.getNameController(cComp).setFcName("client");
        Fractal.getNameController(sComp).setFcName("server");
        
        // ----------------------------------------------------------
        // Add the Client and Server components in the Root composite
        // ----------------------------------------------------------
        Fractal.getContentController(rComp).addFcSubComponent(cComp);
        Fractal.getContentController(rComp).addFcSubComponent(sComp);
        
        // ----------------------------------------------------------
        // Create bindings
        // ----------------------------------------------------------
        Fractal.getBindingController(rComp).bindFc("m", cComp.getFcInterface("m"));
        Fractal.getBindingController(cComp).bindFc("s", sComp.getFcInterface("s"));
        
        // ----------------------------------------------------------
        // Start the Root component
        // ----------------------------------------------------------
        Fractal.getLifeCycleController(rComp).startFc();
        
        // ----------------------------------------------------------
        // Call the entry point of the application
        // ----------------------------------------------------------
        System.err.println("First run, without interceptors");
        Runnable mainItf = (Runnable)rComp.getFcInterface("m"); 
        mainItf.run();
        System.err.println();

        // ----------------------------------------------------------
        // Add interceptors
        // ----------------------------------------------------------
        System.err.println("Adding interceptors...");
        InterceptorController ic = (InterceptorController) cComp.getFcInterface(InterceptorController.NAME);
        ComponentInterface mItf = (ComponentInterface) cComp.getFcInterface("m");
        ComponentInterface sItf = (ComponentInterface) cComp.getFcInterface("s");
        Interceptor inputInterceptor = new MyInputInterceptor();
        Interceptor outputInterceptor = new MyOutputInterceptor();
        Fractal.getLifeCycleController(cComp).stopFc();
        ic.addFcInterceptor(mItf, inputInterceptor);
        ic.addFcInterceptor(sItf, outputInterceptor);
        Fractal.getLifeCycleController(cComp).startFc();
        
        // ----------------------------------------------------------
        // Call the entry point of the application
        // ----------------------------------------------------------
        System.err.println("Second run, with interceptors");
        mainItf.run();
        System.err.println();

        // ----------------------------------------------------------
        // Remove interceptors
        // ----------------------------------------------------------
        System.err.println("Removing interceptors...");
        Fractal.getLifeCycleController(cComp).stopFc();
        ic.removeFcInterceptor(mItf, inputInterceptor);
        ic.removeFcInterceptor(sItf, outputInterceptor);
        Fractal.getLifeCycleController(cComp).startFc();
        
        // ----------------------------------------------------------
        // Call the entry point of the application
        // ----------------------------------------------------------
        System.err.println("Third run, without interceptors");
        mainItf.run();
    }
}

/**
 * A first interceptor class.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
class MyInputInterceptor implements Runnable, Interceptor {

    public void run() {
        System.err.println("   [[ In the input interceptor ]]");
        delegate.run();
    }

    public Object getFcItfDelegate() {
        return delegate;
    }

    public void setFcItfDelegate(Object delegate) {
        this.delegate = (Runnable) delegate;
    }
    
    protected Runnable delegate;

    public void initFcController(InitializationContext ic) throws InstantiationException {
    }
    
    public Object clone() {
        MyInputInterceptor clone = null;
        try {
            clone = (MyInputInterceptor) super.clone();
            clone.delegate = delegate;
        }
        catch( CloneNotSupportedException cnse ) {}
        return clone;
    }   
}

/**
 * A second interceptor class.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
class MyOutputInterceptor extends MyInputInterceptor {

    public void run() {
        System.err.println("   [[ In the output interceptor ]]");
        delegate.run();
    }
}
