/***
 * Julia
 * Copyright (C) 2005-2006 INRIA, France Telecom, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@lifl.fr
 *
 * Author: Lionel Seinturier
 */

package membrane.introspect;

import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.koch.control.membrane.MembraneController;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * This class illustrates the usage of the membrane controller to introspect
 * control membranes.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        
        Component boot = Fractal.getBootstrapComponent();
        TypeFactory tf = Fractal.getTypeFactory(boot);
        GenericFactory gf = Fractal.getGenericFactory(boot);
        
        ComponentType type =
            tf.createFcType(
                new InterfaceType[]{
                    tf.createFcItfType(
                        "r", Runnable.class.getName(), false, true, false)
                }
            );
        
        Component c = gf.newFcInstance(type, "composite", null);
        
        /*
         * Get the membrane control which provides the getFcMembrane() method
         * for retrieving the reference of the control membrane.
         */
        Object itf = c.getFcInterface("/membrane-controller");
        MembraneController mc = (MembraneController) itf;
        Component membrane = mc.getFcMembrane();
        
        /*
         * Display the content of the control membrane.
         */
        System.err.println("Membrane introspection for a composite component");
        System.err.println();
        display(membrane);
    }

    /**
     * Display a description of the given component: interfaces,
     * sub-components and bindings.
     * 
     * @param c  a component
     * @throws NoSuchInterfaceException 
     */
    private static void display( Component c ) throws NoSuchInterfaceException {
        
        System.err.println("  Interfaces");
        Object[] itfs = c.getFcInterfaces();
        for (int i = 0; i < itfs.length; i++) {
            Interface itf = (Interface) itfs[i];
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            System.err.println("  "+i+": "+it.toString());
        }
        System.err.println();
        
        System.err.println("  Sub-components (control components)");
        List subs = ContentControllerHelper.getAllSubComponents(c);
        // start at index 1 to skip c
        for (int i = 1; i < subs.size(); i++) {
            Component sub = (Component) subs.get(i);
            String name = Fractal.getNameController(sub).getFcName();
            System.err.println("  "+(i-1)+": "+name);
        }
    }
}
